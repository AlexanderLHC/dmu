package ex2ex3ex4student;

import java.time.LocalDate;
import java.time.Period;

public class Person {
	private String name;
	private int dayOfBirth; // 1..31
	private int monthOfBirth; // 1..12
	private int yearOfBirth; // 1900..2010
	private int dayToday;
	private int monthToday;
	private int yearToday;

	public Person(String name, int yearOfBirth, int monthOfBirth, int dayOfBirth) {
		this.name = name;
		this.dayOfBirth = dayOfBirth;
		this.monthOfBirth = monthOfBirth;
		this.yearOfBirth = yearOfBirth;
	}

	public void printPerson() {
		System.out.println(name + ", born " + yearOfBirth + "-" + monthOfBirth + "-" + dayOfBirth);
	}

	public void setBirthday(int yearOfBirth, int monthOfBirth, int dayOfBirth) {
		this.yearOfBirth = yearOfBirth;
		this.monthOfBirth = monthOfBirth;
		this.dayOfBirth = dayOfBirth;
	}

	public void calcAgeWithLib() {
		LocalDate currentTime = java.time.LocalDate.now();
		LocalDate bday = java.time.LocalDate.of(yearOfBirth, monthOfBirth, dayOfBirth);
		Period diff = Period.between(bday, currentTime);
		System.out.println("Alder beregnet med library: " + diff.getYears());
	}

	public void setDateToday() {
		dayToday = java.time.LocalDate.now().getDayOfMonth();
		monthToday = java.time.LocalDate.now().getMonthValue();
		yearToday = java.time.LocalDate.now().getYear();
	}

	public void calcAge() {
		setDateToday();
		int age = yearToday - yearOfBirth;
		if (monthOfBirth > monthToday || (monthOfBirth == monthToday && dayOfBirth > dayToday)) {
			age--;
		}
		System.out.println("Alder: " + age);
	}

	/*
	 * Prints whether int year is a leap year
	 */
	public void leapYeahParty(int year) {
		if (isLeapYear(year)) {
			System.out.println(year + " is a leap yeah!! 🥳");
		} else {
			System.out.println(year + " is not leap yeah... 😭");
		}
	}

	public int weekDayToInt(String weekday) {
		weekday = weekday.toLowerCase();
		if (weekday == "mandag") {
			return 1;
		} else if (weekday == "tirsdag") {
			return 2;
		} else if (weekday == "onsdag") {
			return 3;
		} else if (weekday == "torsdag") {
			return 4;
		} else if (weekday == "fredag") {
			return 5;
		} else if (weekday == "lørdag") {
			return 6;
		} else { // == søndag
			return 7;
		}
	}

	public String weekDayToStr(int weekday) {
		if (weekday == 1) {
			return "Mandag";
		} else if (weekday == 2) {
			return "Tirsdag";
		} else if (weekday == 3) {
			return "Onsdag";
		} else if (weekday == 4) {
			return "Torsdag";
		} else if (weekday == 5) {
			return "Fredag";
		} else if (weekday == 6) {
			return "Lørdag";
		} else { // == 7 = søndag
			return "Søndag";
		}
	}

	public boolean isLeapYear(int year) {
		if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
			return true;
		} else {
			return false;
		}
	}

	public String calcWeekday(String weekday, int year) {
		if (year > yearOfBirth) {
			int daysShift = 0;
			int i = 1;
			while (yearOfBirth + i <= year) {
				if (isLeapYear(yearOfBirth + i)) {
					daysShift++;
				}
				daysShift++;
				i++;
			}
			return weekDayToStr((weekDayToInt(weekday) + daysShift) % 7);
		} else {
			return "FEJL: Årstallet skal være større end fødselsåret.";
		}
	}
}
