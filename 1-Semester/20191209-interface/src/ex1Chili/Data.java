package ex1Chili;

public class Data {

	public static Measurable max(Measurable[] objects) { 
		Measurable max = objects[0];
		
		for (Measurable measurable : objects) {
			if (max.getMeasure() < measurable.getMeasure()) {
				max = measurable;
			}
		}
		return max;
	}
	
	public static double avg(Measurable[] objects) {
		double avg = 0;
		for (Measurable measurable : objects) {
			avg += measurable.getMeasure();
		}
		return avg / objects.length;
	}

	public static double avg(Measurable[] objects, Filter filter) {
		double avg = 0;
		for (Measurable m : objects) {
			if (filter.accept(m)) {
				avg += m.getMeasure();
			}
		}
		return avg / objects.length;
	}

}
