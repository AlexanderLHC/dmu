package ex1Chili;

public interface Filter {
	public boolean accept(Object x);
}
