package ex1Chili;

public class ChiliFilter implements Filter {
	private int measure;
	
	public ChiliFilter(int measure) {
		this.measure = measure;
	}

	@Override
	public boolean accept(Object x) {
		return ((Chili) x).getScoville() > measure;
	}

}
