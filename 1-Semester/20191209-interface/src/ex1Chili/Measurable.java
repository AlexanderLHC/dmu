package ex1Chili;

public interface Measurable {
	public double getMeasure();
}
