package ex2Customers;

public class Customer implements Comparable<Customer> {
	private String firstname;
	private String lastname;
	private int age;
	
	public Customer(String firstname, String lastname, int age) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.age = age;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public int compareTo(Customer c) {
		int compareValue = lastname.compareTo(c.lastname);
		if (compareValue == 0) { // if lastname equals
			compareValue = firstname.compareTo(c.firstname);
			if (compareValue == 0) { // if firstname equals
				compareValue = age - c.age;
			}
		}
		return compareValue;
	}
	
	@Override
	public String toString() {
		return firstname + " " + lastname + " (" + age + " år)";
	}
}	