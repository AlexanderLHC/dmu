package pp1;

public class PP1_1 {

	/*
	 * # P1.1
	 * 
	 * You want to decide whether you should drive your car to work or take the
	 * train. You know the one-way distance from your home to your place of work,
	 * and the fuel efficiency of your car (in miles per gallon). You also know the
	 * one-way price of a train ticket. You assume the cost of gas at $4 per gallon,
	 * and car maintenance at 5 cents per mile. Write an algorithm to decide which
	 * commute is cheaper.
	 */

	public static void main(String[] args) {
		double distance = 31; // miles (variable loop)
		double fuelEffeciency = 25; // miles per gallon
		double fuelPrice = 4; // dollars per gallon
		double carMaintenance = 0.05; // dollars per mile
		double priceCommute = 7; // dollars

		double priceCarTrip = (distance / fuelEffeciency * fuelPrice) + (distance * carMaintenance);

		System.out.printf("Bilturen koster: %.2f.\n" + "Togturen koster: %.2f.\n" + "Difference: %.2f", priceCarTrip,
				priceCommute, priceCarTrip - priceCommute);
	}

}
