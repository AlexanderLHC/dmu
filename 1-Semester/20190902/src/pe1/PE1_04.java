package pe1;

public class PE1_04 {

	public static void main(String[] args) {
		// Løsning 1
		System.out.println("Løsning 1:");
		double balance = 1000; // in $ dollars
		double firstYear = balance * 1.05;
		double secondYear = firstYear * 1.05;
		double thirdYear = secondYear * 1.05;
		System.out.println("Første år: " + firstYear + "\nAndet år: " + secondYear + "\nTredje år: " + thirdYear);
		// Løsning 2
		System.out.println("\nLøsning 2:");
		double balance2 = 1000; // in $ dollars
		// År 1
		balance2 = balance2 * 1.05;
		System.out.println("Første år: " + balance2);
		// År 2
		balance2 = balance2 * 1.05;
		System.out.println("Andet år: " + balance2);
		// År 3
		balance2 = balance2 * 1.05;
		System.out.println("Tredje år: " + balance2);
		// Løsning 3
		System.out.println("\nLøsning 3:");
		double factor = 1.05;
		double startBalance = 1000;
		System.out.println("År" + "    " + "balance");
		System.out.println(" 0" + "    " + "1000$");
		System.out.println(" " + 1 + "    " + startBalance * factor + "$");
		System.out.println(" " + 2 + "    " + startBalance * factor * factor + "$");
		System.out.println(" " + 3 + "    " + startBalance * factor * factor * factor + "$");
		// Løsning 4
		System.out.println("\nLøsning 4:");
		int year = 0;
		double interest = 1.05;
		System.out.println("År" + "    " + "balance");
		System.out.printf(" %d    %.2f$ \n", year, balance * Math.pow(interest, year));
		year++;
		System.out.printf(" %d    %.2f$ \n", year, balance * Math.pow(interest, year));
		year++;
		System.out.printf(" %d    %.2f$ \n", year, balance * Math.pow(interest, year));
		year++;
		System.out.printf(" %d    %.2f$ \n", year, balance * Math.pow(interest, year));
	}

}
