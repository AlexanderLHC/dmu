package ex1;

public class TwoDimensionalArrays {
	/*
	 * 1.  
	 	In a project in Eclipse create a class with a main() method.
	   2.
	   	Create in the main() method a two-dimensional array containing the following numbers:
		 0 4 3 9 6
		 1 3 5 2 2
		 3 3 1 0 1
		 0 0 9 7 1
		Use this two-dimensional array to test the methods made is this exercise. For every new 
		3.  Add a method that prints the array as a table:
		   public static void print(int[][] numbers)
		4.  Add these methods :
			 public static int getValueAt(int[][] numbers, int row, int col)
			 public static void setValueAt(int[][] numbers, int row, int col, int value)
			The methods gets and sets the value at (row, col) in the array named numbers.
		5.  Add these methods:
			public static int sumRow(int[][] numbers, int row)
			public static int sumColumn(int[][] numbers, int col)
			The first method returns the sum of the numbers in the given row,
		 	the second method sums 
		6.  Add a method that returns the sum of all numbers in the array: 
			public static int sum(int[][] numbers)
	 */
	public static void main(String[] args) {
		int[] row1 = { 0, 4, 3, 9, 6 };
		int[] row2 = { 1, 3, 5, 2, 2 };
		int[] row3 = { 3, 3, 1, 0, 1 };
		int[] row4 = { 0, 0, 9, 7, 1 };
		int table[][] = { row1, row2, row3, row4 };

		// 3)
		printTable(table);

		// 4)
		// - getter
		System.out.println("Row 0, col 2= " + getValueAt(table, 0, 2));
		System.out.println("Row 3, col 4= " + getValueAt(table, 3, 4));
		// - setter
		System.out.println("Changing row 0, col2 to 32.");
		setValueAt(table, 0, 2, 32);
		System.out.println("Row 0, col 2= " + getValueAt(table, 0, 2));
		System.out.println("Changing row 3, col4 to 9.");
		setValueAt(table, 3, 4, 9);
		System.out.println("Row 3, col 4= " + getValueAt(table, 3, 4));
		// 5)
		printTable(table);
		// - sumRow
		System.out.println("Sum of row2= " + sumRow(table, 2));
		// - sumColumn
		System.out.println("Sum of col2= " + sumColumn(table, 2));
		// 6)
		System.out.println("Sum of all= " + sumAll(table));

	}

	public static void printTable(int[][] table) {
		System.out.printf("       +---+---+---+---+---+%n");
		for (int row = 0; row < table.length; row++) {
			System.out.printf("Row %d: |", row);
			for (int col = 0; col < table[row].length; col++) {
				System.out.printf(" %d |", table[row][col]);
			}
			System.out.printf("%n       +---+---+---+---+---+%n");
		}
	}

	public static int getValueAt(int[][] table, int row, int col) {
		return table[row][col];
	}

	public static void setValueAt(int[][] table, int row, int col, int value) {
		table[row][col] = value;
	}

	public static int sumRow(int[][] table, int row) {
		int sumRow = 0;
		for (int col = 0; col < table[row].length; col++) {
			sumRow += table[row][col];
		}
		return sumRow;
	}

	public static int sumColumn(int[][] table, int col) {
		int sumCol = 0;
		for (int row = 0; row < table.length; row++) {
			sumCol += table[row][col];
		}
		return sumCol;
	}

	public static int sumAll(int[][] table) {
		int sum = 0;
		for (int i = 0; i < table.length; i++) {
			sum += sumRow(table, i);
		}
		return sum;
	}

}
