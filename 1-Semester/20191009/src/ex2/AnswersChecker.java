package ex2;

import java.util.Random;

public class AnswersChecker {

	/*
	 * Exercise 2
		Add a class with a main() method to a project in Eclipse.
		In the main() method create a two-dimensional array that is to contain answers from 8 
		students to a multiple choice test with 10 questions.  Each row shows the answers of one 
		student, and each column shows the answers to one question. Possible answers are the letters 
		'A', 'B', 'C' and 'D'.
		Randomly fill the two-dimensional array with answers.
		Also create a one-dimensional array that contains the correct answers to the 10 questions 
		(choose one answer for each question to be the correct answer.)
		Now add code to 
		- Create, fill and print an array with the number of correct answers for each student.
		- Create, fill and print an array with the number of correct answers to each question.
		You are welcome to add code to do more calculations, e.g finding the best student, finding the
	 */
	public static void main(String[] args) {
		int answerRange = 4; // number of possible answers
		int answersCount = 10;
		int studentsCount = 5;
		char studentsAnswers[][] = new char[studentsCount][answersCount];
		char correctAnswers[] = simulateStudentAnswer(answersCount, answerRange);

		for (int i = 0; i < studentsCount; i++) {
			char[] student = simulateStudentAnswer(answersCount, answerRange);
			studentsAnswers[i] = student;
		}

		printAnswerRow("Correct answers", correctAnswers, ".");
		printStudentsAnswers(studentsAnswers, correctAnswers);
	}

	public static char[] simulateStudentAnswer(int answersCount, int answerRange) {
		char student[] = new char[answersCount];
		Random rng = new Random();
		for (int i = 0; i < answersCount; i++) {
			char answer = (char) (rng.nextInt(answerRange) + 65); // A starts at 65
			student[i] = answer;
		}
		return student;
	}

	public static void printAnswerRow(String preText, char[] answerRow, String postText) {
		System.out.printf("%15s :", preText);
		for (char c : answerRow) {
			System.out.printf("%c, ", c);
		}
		System.out.printf(". %s.%n", postText);
	}

	public static void printStudentsAnswers(char[][] studentsAnswers, char[] correctAnswers) {
		for (int i = 0; i < studentsAnswers.length; i++) {
			char[] studentAnswer = studentsAnswers[i];
			int correctCounter = correctAnswers(correctAnswers, studentAnswer);
			printAnswerRow("Student " + i, studentAnswer, "Correct answers: " + correctCounter);

		}

	}

	public static int correctAnswers(char[] correctAnswers, char[] studentAnswer) {
		int correctCounter = 0;
		for (int i = 0; i < studentAnswer.length; i++) {
			if (studentAnswer[i] == correctAnswers[i]) {
				correctCounter++;
			}
		}
		return correctCounter;
	}

	// Redundant code with
	public static void printCorrectAnswersPerQuestion(char[] correctAnswers, char[][] studentsAnswers,
			int answersCount) {
		char[] answerStatistics = new char[answersCount];
		for (char[] student : studentsAnswers) {
			//TODO: here
		}

	}

}
