/**
 * 
 */
package ex;

import java.util.Scanner;

/**
 * @author alexander
 *
 */
public class PE6_2 {

	/**
	 * @param args
	 */

	/*  E6.2
		Write programs that read a sequence of integer inputs and print:
	
		a.The smallest and largest of the inputs.
		b.The number of even and odd inputs.
		c.Cumulative totals. For example, if the input is 1 7 2 9, the program should print 1 8 10 19.
		d.All adjacent duplicates. For example, if the input is 1 3 3 4 5 5 6 6 6 2, the program should print 3 5 6.	 
	*/
	public static void main(String[] args) {

		exA();
		exB();
		exC();
		exD();

	}

	public static void exA() {
		// === a ==== //
		// a.The smallest and largest of the inputs.
		Scanner scan = new Scanner(System.in);
		// alternativt brug do-while
		boolean canProceed = true;
		System.out.println("a) Indtast 2 tal: ");
		while (canProceed) {
			System.out.print("a=");
			int a = scan.nextInt();
			System.out.print("b=");
			int b = scan.nextInt();
			if (a == 0 || b == 0) {
				canProceed = false;
				System.out.println("Slut.");
			} else {
				if (a > b) {
					System.out.println(a + " > " + b);
				} else {
					System.out.println(a + " < " + b);
				}
			}
		}

	}

	public static void exB() {
		// === b ==== //
		// b.The number of even and odd inputs.
		Scanner scan = new Scanner(System.in);
		// alternativt brug do-while
		System.out.println("b) Indtast 1 tal: ");
		int input = scan.nextInt();
		int oddCounter = 0;
		int evenCounter = 0;

		while (input != 0) {
			if (input % 2 == 0) {
				evenCounter++;
			} else {
				oddCounter++;
			}
			System.out.println("Lige tal: " + evenCounter + ". Ulige tal: " + oddCounter);
			System.out.print("Næste tal: ");
			input = scan.nextInt();
		}
		System.out.println("Tak for denne gang.");

	}

	public static void exC() {
		// === c ==== //
		// c.Cumulative totals. For example, if the input is 1 7 2 9, the program should
		// print 1 8 10 19.
		Scanner scan = new Scanner(System.in);
		System.out.println("c) akkumulative sum. Skriv tal: ");
		int input = scan.nextInt();
		int sum = 0;
		while (input != 0) {
			sum += input;
			System.out.println("Forløbige sum = " + sum);
			System.out.println("Næste tal: ");
			input = scan.nextInt();
		}
		System.out.println("Pyha... Færdig for nu.");

	}

	public static void exD() {
		// === d ==== //
		// d.All adjacent duplicates. For example, if the input is 1 3 3 4 5 5 6 6 6 2,
		// the program should print 3 5 6.
		System.out.println("d) nærliggende dubletter. Skriv tal: ");
		Scanner scan = new Scanner(System.in);
		String duplicates = "";
		int previousInt = 0;
		int currentInt = scan.nextInt();

		while (currentInt != 0) {
			if (currentInt == previousInt) {
				duplicates += currentInt + " ";
			}
			previousInt = currentInt;
			System.out.println("Skriv nyt tal: ");
			currentInt = scan.nextInt();
		}
		System.out.println("Færdig. Fandt flg. dubletter: " + duplicates);

	}
}
