package exercises;

public class Ex7 {
	/*
	 * Exercise 7 Write a linear search method, named searchSum(), with two
	 * parameters: an int array, and a positive integer representing a total. The
	 * method should find a number of adjacent integers, whose sum is equal to the
	 * total parameter, and return the index of the first of these integers. Header
	 * of the method: public int searchSum(int[] t, int total)
	 * 
	 * If t1=[4, 3, 12, 5, 7, -4, 1, 8, 12], then searchSum(t1,9) should return 3,
	 * because t1[3]+ t1[4]+ t1[5]+ t1[6] = 5 + 7 + (-4) + 1 = 9.
	 * 
	 * You may get inspiration from “Linear search with more complicated matching”.
	 */

	public static void main(String[] args) {
		int[] l1 = { 4, 3, 12, 5, 7, -4, 1, 8, 12 };
		int sum = 9;
		System.out.printf("Searching for sum %s and returned: %s%n", sum, searchSum(l1, sum));
		sum = 7;
		System.out.printf("Searching for sum %s and returned: %s%n", sum, searchSum(l1, sum));
		sum = 21;
		System.out.printf("Searching for sum %s and returned: %s%n", sum, searchSum(l1, sum));

	}

	public static int searchSum(int[] list, int sum) {
		int index = -1;
		int i = 0;
		while (i < list.length - 1 && index == -1) {
			int sumFromIndex = list[i];
			int j = 1;
			while (i + j < list.length - 1) {
				if (sumFromIndex == sum) {
					index = i;
				}
				sumFromIndex += list[i + j];
				j++;
			}
			i++;
		}
		return index;
	}
}
