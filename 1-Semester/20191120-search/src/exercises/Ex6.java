package exercises;

public class Ex6 {
	/*
	 * Exercise 6 Write a search method, named repeatedChars(), with two parameters:
	 * a string s, and a positive int k. The method should return a boolean
	 * indicating whether the same character is found in k adjacent positions in the
	 * string. Examples: repeatedChars("vnhstxxxaby",3) returns true, because the
	 * character x is found in 3 adjacent places in the string.
	 * repeatedChars("vnhstxxxaby",4) returns false. You may get inspiration from
	 * “Linear search with more complicated matching”.
	 */

	public static void main(String[] args) {
		String s1 = "vnhstxxxaby";
		String s2 = "vnhstxxxxxxaby";
		int kAdjacent = 3;

		System.out.printf("In '%s' are there %s adjacent characters?: %s%n", s1, kAdjacent,
				repeatedChars(s1, kAdjacent));
		kAdjacent = 4;
		System.out.printf("In '%s' are there %s adjacent characters?: %s%n", s1, kAdjacent,
				repeatedChars(s1, kAdjacent));
		kAdjacent = 8;
		System.out.printf("In '%s' are there %s adjacent characters?: %s%n", s2, kAdjacent,
				repeatedChars(s2, kAdjacent));
		kAdjacent = 6;
		System.out.printf("In '%s' are there %s adjacent characters?: %s%n", s2, kAdjacent,
				repeatedChars(s2, kAdjacent));

	}

	public static boolean repeatedChars(String string, int kAdjacent) {
		boolean found = false;
		int i = 0;
		while (i < string.length() - 1 - kAdjacent && found == false) {
			String repeated = String.format("%0" + kAdjacent + "d", 0).replace("0", string.substring(i, i + 1));
			if (string.substring(i, i + kAdjacent).compareTo(repeated) == 0) {
				found = true;
			}
			i++;
		}
		return found;
	}

}
