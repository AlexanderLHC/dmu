package exercises;

public class Ex9 {

	/*
	 * Opgave 9 Skriv en metode, der (uden brug af operationen for kvadratrod:
	 * Math.sqrt) kan beregne heltalskvadratroden af et heltal n >= 0. Metoden skal
	 * altså returnere det største heltal r, der opfylder: r^2 <= n < (r+1)^2 . Her
	 * er nogle eksempler:
	 * 
	 * n 0 2 3 4 7 8 9 10 111 
	 * r 0 1 1 2 2 2 3 3 10
	 * 
	 * Skriv en metode der anvender lineær søgning Skriv en metode der anvender
	 * binær søgning Hint: Der skal hverken anvendes array eller ArrayList, der skal
	 * blot søges blandt tallene 0 til n.
	 */

	public static void main(String[] args) {
		highestSquaredIntLessOrEqualToN(111);
	}

	public static int highestSquaredIntLessOrEqualToN(int n) {
		int highestSquared = 0;
		int i = 0;

		while (i <= n) {
			int r = 0;
			while (r * r <= i) {
				highestSquared = r;
				r++;
			}
			i++;
		}

		return highestSquared;
	}
}
