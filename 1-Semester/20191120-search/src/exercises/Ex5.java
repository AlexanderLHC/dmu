package exercises;

import java.util.ArrayList;

import com.github.javafaker.Faker;

public class Ex5 {
	/*
	 * Exercise 5 Write a method that returns all indexes of a given String in a
	 * given ArrayList<String>. The header of the method: public static
	 * ArrayList<Integer> findAllIndices(ArrayList<String> list, String target)
	 */

	public static void main(String[] args) {
		Faker f = new Faker();
		ArrayList<String> strings = new ArrayList<String>();

		for (int i = 0; i < 100; i++) {
			strings.add(f.lorem().word());
		}

		String target = strings.get(20);
		System.out.printf("Looking for %s. Found at (indexes): %s%n", target, findAllIndices(strings, target));
		target = "asd";
		System.out.printf("Looking for %s. Found at (indexes): %s%n", target, findAllIndices(strings, target));
	}

	public static ArrayList<Integer> findAllIndices(ArrayList<String> list, String target) {
		ArrayList<Integer> foundIndexes = new ArrayList<Integer>();

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).compareTo(target) == 0) {
				foundIndexes.add(i);
			}
		}

		return foundIndexes;
	}
}
