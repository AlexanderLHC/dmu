package exercises;

import java.util.Arrays;
import java.util.Random;

public class Ex2 {
	/*
	 Exercise 2
	Write a linear search method that finds the first number belonging to the interval [10;15]. The
	method must return the number found in the interval, and take an array of integer numbers as
	parameter. If a number in the interval is not found, the method must return -1.
	Test the method.
	If the array is [7, 56, 34, 3, 7, 14, 13, 4], the method should return 14.
	 */

	public static void main(String[] args) {
		Random r = new Random();
		int[] l1 = {7, 56, 34, 3, 7, 14, 13, 4};
		int[] l2 = r.ints(5, 0, 100).toArray();
		int[] l3 = r.ints(10, 0, 50).toArray();
				
		int start = 10;
		int end = 15;
		// l1
		System.out.printf("List %s,%n   contains : %s. Between [%s, %s]%n%n----------%n",
				Arrays.toString(l1), linNumInInterval(l1, start, end),
				start, end);
		// l2
		System.out.printf("List %s,%n   contains : %s. Between [%s, %s]%n%n----------%n",
				Arrays.toString(l2), linNumInInterval(l2, start, end),
				start, end);
		// l3
		System.out.printf("List %s,%n   contains : %s. Between [%s, %s]%n%n----------%n",
				Arrays.toString(l3), linNumInInterval(l3, start, end),
				start, end);
	}
	
	public static int linNumInInterval(int[] list, int start, int end) {
		int found = -1;
		int i = 0;
		while (i < list.length && found == -1) {
			if (list[i] > start && list[i] < end) {
				found=list[i];
			}
			i++;
		}
		return found;
	}
}
