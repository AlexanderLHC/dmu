package exercises;

import java.util.Arrays;
import java.util.Random;

public class Ex3 {
	/*
	 Exercise 3
	Write a linear search method that returns true, if two adjacent numbers are the same. The
	method must take an array of integer numbers as parameter.

	Test the method.
	If the array is [7, 9, 13, 7, 9, 13], the method must return false.
	If the array is [7, 9, 13, 13, 9, 7], the method must returns true.

	Write another method that returns true, if the same number exists in n adjacent places. The
	method must take an array of integer numbers and the number n as parameters.
	Test the method.
	 */
	public static void main(String[] args) {
		Random r = new Random();
		int[] l1 = {7, 9, 13, 7, 9, 13};
		int[] l2 = { 7, 9, 13, 13, 9, 7 };
		int[] l3 = r.ints(10, 0, 50).toArray();
		int[] l4 = r.ints(10, 0, 50).toArray();
		int[] l5 = { 7, 9, 13, 13, 13, 9, 7 };

		int nAdjacent = 2;
		System.out.printf("List: %s%n %s has %s adjacent.%n----------%n", 
				Arrays.toString(l1), linFindNAdjacent(l1, nAdjacent), nAdjacent);
		System.out.printf("List: %s%n %s has %s adjacent.%n----------%n", 
				Arrays.toString(l2), linFindNAdjacent(l2, nAdjacent), nAdjacent);
		System.out.printf("List: %s%n %s has %s adjacent.%n----------%n", 
				Arrays.toString(l3), linFindNAdjacent(l3, nAdjacent), nAdjacent);
		System.out.printf("List: %s%n %s has %s adjacent.%n----------%n", 
				Arrays.toString(l4), linFindNAdjacent(l4, nAdjacent), nAdjacent);
		nAdjacent = 3;
		System.out.printf("List: %s%n %s has %s adjacent.%n----------%n", 
				Arrays.toString(l5), linFindNAdjacent(l5, nAdjacent), nAdjacent);

	}
	
	public static boolean linFindNAdjacent(int[] list, int nAdjacent) {
		boolean found = false;
		int i = 0;

		while (i < list.length -nAdjacent &&!found) {
			if (hasNAdjacentFromIndex(list, i, nAdjacent)) {
				found = true;
			}
			i++;
		}
		return found;
	}
	
	public static boolean hasNAdjacentFromIndex(int[] list, int index, int nAdjacent) {
		boolean adjacentUnequal = false;
		int i = 0;
		while (i < nAdjacent && !adjacentUnequal) {
			if (list[index] != list[index+i]) {
				adjacentUnequal = true;
			}
			i++;
		}
		return ((adjacentUnequal == true) ? false : true);
	}


}
