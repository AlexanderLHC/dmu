package exerices;

import java.util.ArrayList;
import java.util.Arrays;

public class ex3 {

	/*
	Exercise 3 
		Write two insertion sort methods. The first must take an array String[] as parameter, the 
		second must take an ArrayList<Customer> as parameter. Make your own Customer class that 
		has a compareTo(Customer): int method (you decide how customers are compared). 
		Test your methods. 
	 */

	public static void main(String[] args) {
		ArrayList<Student> students = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			students.add(new Student());
		}

		String[] names = new String[5];
		for (int i = 0; i < names.length; i++) {
			names[i] = students.get(i).getName();
		}

		// Tests
		System.out.println("3.1) Unsorted list: " + Arrays.toString(names));
		System.out.println("3.1) Sorted list: " + Arrays.toString(insertionSortNames(names)));
		System.out.println("3.1) Unsorted students: " + students.toString());
		System.out.println("3.2) Sorted students: " + insertionSortStudents(students).toString());

	}

	public static String[] insertionSortNames(String[] names) {
		for (int i = 1; i < names.length; i++) {
			String temp = names[i];
			int j = i - 1;
			while (j >= 0 && temp.compareTo(names[j]) < 0) {
				names[j + 1] = names[j];
				j--;
			}
			names[j + 1] = temp;
		}
		return names;
	}

	public static ArrayList<Student> insertionSortStudents(ArrayList<Student> students) {
		for (int i = 1; i < students.size(); i++) {
			Student temp = students.get(i);
			int j = i - 1;
			while (j >= 0 && temp.getName().compareTo(students.get(j).getName()) < 0) {
				students.set(j + 1, students.get(j));
				j--;
			}
			students.set(j + 1, temp);
		}
		return students;
	}
}