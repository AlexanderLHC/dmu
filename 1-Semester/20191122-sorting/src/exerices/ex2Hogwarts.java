package exerices;

import java.util.ArrayList;
import java.util.Arrays;

public class ex2Hogwarts {

	/*
	Exercise 2 
		Write two selection sort methods. The first must take an array String[] as parameter, the 
		second must take an ArrayList<Customer> as parameter. Make your own Customer class that 
		has a compareTo(Customer): int method (you decide how customers are compared). 
		Test your methods. 
	 */

	public static void main(String[] args) {
		ArrayList<Student> students = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			students.add(new Student());
		}

		String[] names = new String[5];
		for (int i = 0; i < names.length; i++) {
			names[i] = students.get(i).getName();
		}

		// Tests
		System.out.println("2.1) Unsorted list: " + Arrays.toString(names));
		System.out.println("2.1) Sorted list: " + Arrays.toString(selectionSort(names)));
		System.out.println("2.1) Unsorted students: " + students.toString());
		System.out.println("2.2) Sorted students: " + sortStudentsByHouse(students).toString());
	}

	public static String[] selectionSort(String[] liste) {
		for (int i = 0; i < liste.length - 1; i++) {
			// find the index of the smallest number in arr[i..arr.length-1]
			int indexOfMin = i;
			for (int j = i + 1; j < liste.length; j++) {
				if (liste[j].compareTo(liste[indexOfMin]) < 0) {
					indexOfMin = j;
				}
			}
			// swap arr[indexOfMin] and arr[i]
			if (indexOfMin != i) {
				String temp = liste[i];
				liste[i] = liste[indexOfMin];
				liste[indexOfMin] = temp;
			}
		}
		return liste;
	}

	public static ArrayList<Student> sortStudentsByHouse(ArrayList<Student> students) {
		for (int i = 0; i < students.size() - 1; i++) {
			int indexOfMin = i;
			for (int j = i + 1; j < students.size(); j++) {
				if (students.get(j).getHouse() > students.get(indexOfMin).getHouse()) {
					indexOfMin = j;
				}
			}
			if (indexOfMin != i) {
				Student tmp = students.get(i);
				students.set(i, students.get(indexOfMin));
				students.set(indexOfMin, tmp);
			}
		}
		return students;
	}
}
