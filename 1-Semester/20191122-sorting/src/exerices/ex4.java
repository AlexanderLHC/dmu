package exerices;

import java.util.ArrayList;
import java.util.Random;

public class ex4 {
	/*
	  
	Exercise 4 
		Measure the execution times of the 3 sorting algorithms (bubble, selection and insertion sort) 
		on an ArrayList<Integer>. Make a table with your results. 
		Use the method System.currentTimeMillis() (or the method System.nanoTime()) to find the 
		execution time of a method. 
		Use the method Random.ints() to generate random numbers, as in the following code that 
		makes an array with 1000 numbers between 1 and 10000: 
	
			Random r = new Random(); 
			 
			int[] ints = r.ints(1000, 1, 10000).toArray(); 
			 
			ArrayList<Integer> numbers = new ArrayList<>(); 
			for (int e : ints) { 
				numbers.add(e); 
			}
	 */

	public static void main(String[] args) {
		Random r = new Random();

		int[] ints = r.ints(1000, 1, 10000).toArray();

		ArrayList<Integer> numbers = new ArrayList<>();
		for (int e : ints) {
			numbers.add(e);
		}
		
		ArrayList<Integer> numsForBubble = new ArrayList<Integer>(numbers);
		ArrayList<Integer> numsForSelection = new ArrayList<Integer>(numbers);
		ArrayList<Integer> numsForInsertion = new ArrayList<Integer>(numbers);
		ArrayList<Integer> numsForComparable = new ArrayList<Integer>(numbers);

		// bubble
		long bubbleStart =  System.nanoTime();
		bubbleSort(numsForBubble);
		long bubbleDurationMs =	(System.nanoTime() -bubbleStart) / 1000000;
		// selection
		long selectionStart =  System.nanoTime();
		insertionSort(numsForSelection);
		long selectionDurationMs =	(System.nanoTime() -selectionStart) / 1000000;
		// insertion
		long insertionStart =  System.nanoTime();
		selectionSort(numsForInsertion);
		long insertionDurationMs =	(System.nanoTime() -insertionStart) / 1000000;
		// comparable
		long comparabelStart =  System.nanoTime();
		selectionSort(numsForComparable);
		long comparableDurationMs =	(System.nanoTime() -comparabelStart) / 1000000;

		
		System.out.printf("| %-20s| %-20s|%n", "Method", "Time (ms)");
		System.out.printf("|-------------------------------------------|%n");
		System.out.printf("| %-20s| %s%n", "bubble", bubbleDurationMs);
		System.out.printf("| %-20s| %s%n", "selection", selectionDurationMs);
		System.out.printf("| %-20s| %s%n", "insertion", insertionDurationMs);
		System.out.printf("| %-20s| %s%n", "comparable", comparableDurationMs);
		
	}

	/*
	 * Bubble sort
	 */
	public static long bubbleSort(ArrayList<Integer> arr) {
		long start = System.currentTimeMillis();
		for (int i = arr.size() - 1; i >= 2; i--) {
			for (int j = 1; j <= i; j++) {
				if (arr.get(j - 1) > arr.get(j)) {
					int temp = arr.get(j - 1);
					arr.set(j - 1, arr.get(j));
					arr.set(j, temp);
				}
			}
		}
		return System.currentTimeMillis() - start;
	}

	/*
	 * Selection sort
	 */
	public static ArrayList<Integer> selectionSort(ArrayList<Integer> arr) {
		for (int position = 0; position < arr.size() -1; position++) {
			int minimum = position;
			for (int scan = position+1; scan < arr.size(); scan++) {
				if (arr.get(scan) < arr.get(minimum)) {
					minimum = scan;
				}
			}
			int temp = arr.get(minimum);
			arr.set(minimum, arr.get(position));
			arr.set(position, temp);
		}
		return arr;
	}
	/*
	 * Insertion sort
	 */
	public static long insertionSort(ArrayList<Integer> arr) {
		long start = System.currentTimeMillis();
		for (int i = 1; i < arr.size(); i++) {
			int temp = arr.get(i);
			int j = i - 1;
			while (j >= 0 && temp < arr.get(j)) {
				arr.set(j + 1, arr.get(j));
				j--;
			}
			arr.set(j + 1, temp);
		}
		return System.currentTimeMillis() - start;
	}


}
