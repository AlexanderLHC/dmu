package exerices;

import java.util.Random;

import com.github.javafaker.Faker;

public class Student implements Comparable<Student> {
	private String name;
	private int age;
	private int house;
	private String home;

	public Student() {
		Faker f = new Faker();
		Random r = new Random();

		this.name = f.harryPotter().character();
		this.age = r.nextInt(70)+1;
		this.house = r.nextInt(5) + 1;
		this.home = f.harryPotter().location();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getHouse() {
		return house;
	}

	public void setHouse(int house) {
		this.house = house;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	@Override
	public String toString() {
		return this.name + " " + this.house;
	}

	@Override
	public int compareTo(Student s) {
		return this.name.compareTo(s.name);
	}
}
