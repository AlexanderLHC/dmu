package exerices;

import java.util.ArrayList;
import java.util.Collections;

public class ex5 {
	/*
	 Exercise 5 
		The sort() method in java.util.Collections is used to sort ArrayLists containing objects that 
		implements the Comparable interface. 
		Here is an example of the use of the method: 
	
		public class Test { 
			public static void main(String[] args) { 
				ArrayList<String> list = new ArrayList<String>(); 
				 // Add some strings to list here 
				 System.out.println(list); // print the list before sorting 
				 Collections.sort(list); 
				 System.out.println(list); // print the list after sorting 
			} 
		} 
		Run the program and look at the output. 
		Change the program to sort an ArrayList<Customer> containing customers from a Customer 
	 */

	public static void main(String[] args) {
		ArrayList<Student> students = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			students.add(new Student());
		}

		ArrayList<String> names = new ArrayList<>();
		for (int i = 0; i < 50; i++) {
			names.add(students.get(i).getName());
		}

		System.out.println(names);
		Collections.sort(names);
		System.out.println(names);
	}

}
