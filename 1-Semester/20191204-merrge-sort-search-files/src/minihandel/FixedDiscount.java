package minihandel;

public class FixedDiscount extends Discount {
	private int fixedDiscount;
	private int discountLimit;

	public FixedDiscount(int fixedDiscount, int discountLimit) {
		this.fixedDiscount = fixedDiscount;
		this.discountLimit = discountLimit;
	}

	@Override
	public Double getDiscount(Double price) {
		Double discount = 0.0;
		if (price >= discountLimit) {
			discount += fixedDiscount;
		}
		return discount;
	}
}
