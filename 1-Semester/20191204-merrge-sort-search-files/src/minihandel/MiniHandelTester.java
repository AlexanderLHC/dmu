package minihandel;

import java.time.LocalDate;

public class MiniHandelTester {

	public static void main(String[] args) {
		Product p1 = new Product(1, "Cola", 20);
		Product p2 = new Product(2, "Kaffe", 10);
		Product p3 = new Product(3, "Ostebolle", 10);
		Product p4 = new Product(4, "Vand", 8);
		Product p5 = new Product(5, "Croissant", 12);

		Customer c1 = new Customer("James Gosling", LocalDate.of(1955, 5, 19));
		Order o1 = new Order(1);
		o1.createOrderLine(1, p1);
		o1.createOrderLine(1, p3);
		c1.addOrder(o1);
		Order o2 = new Order(2);
		o2.createOrderLine(3, p5);
		o2.createOrderLine(1, p2);
		c1.addOrder(o2);
		Discount d1 = new PercentDiscount(15);
		c1.setDiscount(d1);

		Customer c2 = new Customer("Richard Stallman", LocalDate.of(1953, 3, 16));
		Order o3 = new Order(3);
		o3.createOrderLine(1, p4);
		o3.createOrderLine(1, p3);
		c2.addOrder(o3);
		Order o4 = new Order(4);
		o4.createOrderLine(2, p2);
		o4.createOrderLine(1, p5);
		c2.addOrder(o4);
		Order o5 = new Order(5);
		o5.createOrderLine(1, p4);
		o5.createOrderLine(1, p3);
		c2.addOrder(o5);
		Order o6 = new Order(6);
		o6.createOrderLine(1, p1);
		o6.createOrderLine(2, p5);
		c2.addOrder(o6);
		Discount d2 = new FixedDiscount(25, 100);
		c2.setDiscount(d2);

		System.out.printf("----------%s----------%n", c1.getName());
		System.out.printf("Orders: %s%n", c1.getOrders());
		System.out.printf("Total price: %s💵%n", c1.totalBuy());
		System.out.printf("Total price with discount: %s💵%n", c1.totalBuyWithDiscount());
		System.out.println("------------------");
		System.out.println();
		System.out.printf("----------%s----------%n", c2.getName());
		System.out.printf("Orders: %s%n", c2.getOrders());
		System.out.printf("Total price: %s💵%n", c2.totalBuy());
		System.out.printf("Total price with discount: %s💵%n", c2.totalBuyWithDiscount());
		System.out.println("------------------");

	}

}
