package exercisesPersoner;

import java.time.LocalDate;
import java.util.ArrayList;

public class PersonTester {
	/*
	 Opgave 1
	Lav tre klasser i en generaliseringsstruktur, der repræsenterer begreberne person, mekaniker og
	værkfører (på et mekanikerværksted).
	For personer registreres navn og adresse. Det skal være muligt både at opdatere og hente navn og
	adresse.
	For mekanikere registreres desuden år for svendeprøve samt en timelønssats. Igen skal det være
	muligt at hente og opdatere begge attributter.
	For værkførere (der også er mekanikere) registreres år for udnævnelse til værkfører samt
	størrelsen af det tillæg pr. uge, som værkføreren gives ud over den almindelige timeløn. Man skal
	kunne oprette forekomster af alle tre klasser.
	a) Lav et klasse-diagram indeholdende de tre klasser og deres indbyrdes sammenhæng
	b) Programmer klasserne i henhold til klasse-diagrammet
	
	Opgave 2
	Udvid klasserne fra opgave 1, så det nu bliver muligt at beregne ugeløn for mekanikere og værk-
	førere. Det kan antages at både mekanikere og værkfører har en arbejdsuge på 37 timer.
	Er det muligt at definere beregnLoen metoden en gang for alle i mekaniker-klassen, eller er det
	nødvendigt at redefinere metoden i værkførerklassen?
	Generaliseringsstrukturen skal gøre det muligt at skrive følgende operation i en anvendelses
	klasse:
	 */
	
	public static void main(String[] args) {
		Person p1 = new Person("Søren", "Rabalderstræde 12");
		Person p2 = new Mekaniker("Jacob", "Christiania 69", LocalDate.of(2019, 12, 04));
		Person p3 = new Værkfører("Victor", "Beardless Street 2nd", LocalDate.of(2002, 6, 21), LocalDate.of(2019, 12, 04));
		Person p4 = new Synsmand("Mads", "Aarhus Havn -1", LocalDate.of(2009, 6, 1));
		
		ArrayList<Person> personer = new ArrayList<Person>();
		personer.add(p1);
		personer.add(p2);
		personer.add(p3);
		personer.add(p4);
		
		System.out.println("Printer alle personer:");
		System.out.printf("| %-10s | %-25s | %-10s | %-5s | %-10s   | %-5s  | %-10s |%n", "navn", "adresse", "udlært", "timeløn", "ugeløn", "tillæg", "forfremmet");

		for (Person p : personer) {
			System.out.println(p);
		}
		ArrayList<Mekaniker> ansatte = new ArrayList<Mekaniker>();
		ansatte.add((Mekaniker) p2);
		ansatte.add((Mekaniker) p3);
		ansatte.add((Mekaniker) p4);
		System.out.println("\nDen samlede løn = " + samletLoen(ansatte) + "kr");
		
		System.out.println(p4);
		((Synsmand) p4).addSyn();
		System.out.println("Tilføjet et syn.\n " + p4);
	}
	
	/**
	* Beregner summen af ugelønnen for alle i listen
	**/
	public static Double samletLoen(ArrayList<Mekaniker> list){
		Double sum = 0.0;
		for (Mekaniker mekaniker : list) {
			if (mekaniker.getTimeløn() != null) {
				sum += mekaniker.ugeLøn();
			}
		}
		return sum;
	};

}
