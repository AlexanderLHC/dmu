package exercises;

import com.github.javafaker.Faker;

public class Customer implements Comparable<Customer> {
	private String firstname;
	private String lastname;
	private int age;

	public Customer() {
		Faker f = new Faker();
		firstname = f.name().firstName();
		lastname = f.name().lastName();
		age = f.random().nextInt(70);
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getName() {
		return firstname + " " + lastname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public int compareTo(Customer c) {
		// Sorts by full name
		if (lastname.compareTo(c.lastname) == 0) {
			return firstname.compareTo(c.firstname);
		} else {
			return lastname.compareTo(c.lastname);
		}
	}

}
