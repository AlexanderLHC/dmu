package exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Ex3 {
	/*
		Exercise 3
			In this exercise you are going to merge an ArrayList (containing customers) with an array
			(also containing customers).
			The ArrayList contains customers from a shop. The array contains customers that are known
			slow payers. The method goodCustomers() must return an ArrayList containing the shop's
			customers that are not slow payers.
			a) Program the following method in the class:
			
			* Returns a sorted ArrayList containing all customers
			* from l1 that are not in l2.
			* Requires: l1 and l2 are sorted and l2 has no empty
			* entries.
			
			public static ArrayList<Customer> goodCustomers(
			ArrayList<Customer> l1, Customer[] l2)
			b) Create an ArrayList containing Customer objects and sort the list.
			c) Create a Customer array containing customers and sort the array. Make sure some
			customers
			are both in the list and in the array (the list and the array must have customers with the same
			name).
			d) Use the method goodCustomers() and print the result.
	*/

	public static void main(String[] args) {
		// a and b)
		ArrayList<Customer> customers = new ArrayList<>();
		Customer[] badCustomers = new Customer[3];
		for (int i = 0; i < 10; i++) {
			customers.add(new Customer());
		}
		Collections.sort(customers);
		badCustomers[0] = customers.get(2);
		badCustomers[1] = customers.get(4);
		badCustomers[2] = customers.get(7);

		System.out.printf("List of all customers: %s%n", customers);
		System.out.printf("List of bad customers: %s%n", Arrays.toString(badCustomers));
		System.out.printf("List of good customers: %s%n", goodCustomers(customers, badCustomers));
	}

	public static ArrayList<Customer> goodCustomers(ArrayList<Customer> l1, Customer[] l2) {
		ArrayList<Customer> goodCustomers = new ArrayList<>(l1);

		for (int i = 0; i < l2.length; i++) {
			for (int j = 0; j < l1.size(); j++) {
				if (l2[i] == l1.get(j)) {
					goodCustomers.remove(j);
				}
			}
		}

		return goodCustomers;
	}
}
