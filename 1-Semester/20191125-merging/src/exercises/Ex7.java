package exercises;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Ex7 {
	/*
	 Opgave 2 (Søgning i filer)
		a) Opret en fil personer.txt med nedenstående opbygning: et antal linjer med et nummer,
		et fornavn og et efternavn:
		133 Jens Hansen
		150 Laila Jensen
		887 Karl Thomsen
		605 Bent Jensen
		100 Ove Andersen
		488 Hanne Hansen
		320 Egon Olsen
		122 Inge Jensen
		
		b) Skriv en metode der leder efter en person med et bestemt efternavn:
		public static String linFileSearchPerson(String fileName,
		String target) {...}
		Metoden skal returnere hele personens navn. Hvis personen ikke findes, skal den tomme
		streng returneres.
	 */
	public static void main(String[] args) throws FileNotFoundException {
		String lastName = "Hansen";
		System.out.printf("Looking for %s, result: %s.%n", lastName, linFileSearchPerson("personer.txt", lastName));
		lastName = "Vader";
		System.out.printf("Looking for %s, result: %s.%n", lastName, linFileSearchPerson("personer.txt", lastName));
		lastName = "Jensen";
		System.out.printf("Looking for %s, result: %s.%n", lastName, linFileSearchPerson("personer.txt", lastName));
	}

	public static String linFileSearchPerson(String fileName, String target) throws FileNotFoundException {
		String fullName = "";
		FileInputStream file = new FileInputStream(fileName);
		Scanner scan = new Scanner(file);
		while (scan.hasNextLine() && fullName.length() == 0) {
			String[] line = scan.nextLine().split(" ");

			if (line[2].compareTo(target) == 0) {
				fullName = line[1] + " " + line[2];
			}

		}
		scan.close();
		return fullName;
	}

}
