package exercises;

import java.util.Arrays;

public class Ex2 {
	/*
	 Exercise 2
		In this exercise you are going to merge two arrays containing integers.
		
		a) Program the following method in the method class:
		
		* Returns a sorted array containing all elements
		* shared by l1 and l2.
		* The returned array must have no empty entries.
		* Requires: l1 and l2 are sorted and have no empty entries.
		
		public static int[] sharedNumbers (int[] l1, int[] l2)
			
		b) Create two arrays in the main() method (perhaps {2,4,6,8,10,12,14} and
			{1,2,4,5,6,9,12,17}).
		c) Use the method sharedNumbers() with the two arrays and print the result. (With the two
		arrays above the result will be {2,4,6,12}).
	 */
	public static void main(String[] args) {
		int[] l1 = { 2, 4, 6, 8, 10, 12, 14 };
		int[] l2 = { 1, 2, 4, 5, 6, 9, 12, 17 };

		System.out.printf("List 1 %s%n", Arrays.toString(l1));
		System.out.printf("List 2 %s%n", Arrays.toString(l2));
		System.out.printf("Shared values: %s%n", Arrays.toString(sharedNumbers(l1, l2)));

	}

	public static int[] sharedNumbers(int[] l1, int[] l2) {
		int[] sharedNums = new int[l1.length + l2.length];
		int i1 = 0;
		int i2 = 0;
		int j = 0;

		while (i1 < l1.length && i2 < l2.length) {
			if (l1[i1] > l2[i2]) {
				i2++;
			} else if (l1[i1] < l2[i2]) {
				i1++;
			} else {
				sharedNums[j] = l1[i1];
				i1++;
				i2++;
				j++;
			}
		}
		return Arrays.copyOf(sharedNums, j);
	}
}
