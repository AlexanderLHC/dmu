package exercises;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Ex6 {

	/*
	 Skriv en metode der søger efter heltal i en fil.

	public static boolean linFileSearch(String fileName, int target)

	Metoden skal returnere om tallet findes i filen eller ej. Anvend søgeskabelonen.
	Du kan fx lave en talfil.txt med følgende indhold:
		9140
		4450
		5885
		1284
		219
		4203
		8965
		4919
		982
		959
	 */
	
	public static void main(String[] args) {
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		try {
			FileInputStream file = new FileInputStream("talfil.txt");
			Scanner scan = new Scanner(file);
			while (scan.hasNext()) {
				numbers.add(Integer.parseInt(scan.nextLine()));
			}
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int lookup = 982;
		System.out.printf("Searching for %s. Found: %s%n", lookup, linFileSearch(numbers, lookup));
		lookup = 32;
		System.out.printf("Searching for %s. Found: %s%n", lookup, linFileSearch(numbers, lookup));
		lookup = 1012;
		System.out.printf("Searching for %s. Found: %s%n", lookup, linFileSearch(numbers, lookup));
	}
	
	public static boolean linFileSearch(ArrayList<Integer> list, int target) {
		boolean found = false;
		int i = 0;
		while (i < list.size() && !found) {
			if (target == list.get(i)) {
				found = true;
			}
			i++;
		}
		return found;
	}
	
}
