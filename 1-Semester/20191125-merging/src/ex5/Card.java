package ex5;

public class Card implements Comparable<Card> {
	private Suit suit;
	private Value value;

	public Card(Suit suit, Value value) {
		this.suit = suit;
		this.value = value;
	}

	public Suit getSuit() {
		return suit;
	}

	public void setSuit(Suit suit) {
		this.suit = suit;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	@Override
	public int compareTo(Card c) {
		if (value.compareTo(c.value) == 0) {
			return suit.compareTo(c.suit);
		} else {
			return value.compareTo(value);
		}
	}

	@Override
	public String toString() {
		return value + " of " + suit;
	}
}
