package ex5;

import java.util.ArrayList;
import java.util.Collections;

public class Hand {
	private ArrayList<Card> cards = new ArrayList<>();

	public Hand() {

	}

	/*
	 * addCard(Card card): boolean and removeCard(Card card): boolean (the
	addCard method return true unless addCard tries to add a card already in the hand; the
	removeCard method returns true unless removeCard tries to remove a card not in the hand).
	Add a toString() method that returns the cards in ascending order.
	Test your classes in a main() method.
	 */

	public boolean addCard(Card card) {
		boolean added = false;
		if (!cards.contains(card)) {
			cards.add(card);
			added = true;
		}
		return added;
	}

	public boolean removeCard(Card card) {
		boolean removed = false;
		if (cards.contains(card)) {
			cards.remove(card);
			removed = true;
		}
		return removed;
	}

	@Override
	public String toString() {
		ArrayList<Card> cardsInHand = new ArrayList<Card>(cards);
		Collections.sort(cardsInHand);
		return cardsInHand.toString();
	}
}
