package ex3Swimmers;

import java.util.ArrayList;

/**
 * Models a training plan for a Swimmer
 */
public class TrainingPlan {
	private char level;
	private int weeklyWaterHours;
	private int weeklyStrengthHours;
	private ArrayList<Swimmer> swimmers = new ArrayList<>(); // nullable

	public TrainingPlan(char level, int weeklyWaterHours, int weeklyStrengthHours) {
		this.level = level;
		this.weeklyWaterHours = weeklyWaterHours;
		this.weeklyStrengthHours = weeklyStrengthHours;
	}

	public char getLevel() {
		return level;
	}

	public void setLevel(char niveau) {
		this.level = niveau;
	}

	public int getWeeklyStrengthHours() {
		return weeklyStrengthHours;
	}

	public void setWeeklyStrengthHours(int weeklyStrengthHours) {
		this.weeklyStrengthHours = weeklyStrengthHours;
	}

	public int getWeeklyWaterHours() {
		return weeklyWaterHours;
	}

	public void setWeeklyWaterHours(int weeklyWaterHours) {
		this.weeklyWaterHours = weeklyWaterHours;
	}

	public int allTrainingHours() {
		return weeklyWaterHours + weeklyStrengthHours;
	}

	public ArrayList<Swimmer> getSwimmers() {
		return swimmers;
	}

	public void removeSwimmer(Swimmer s) {
		if (swimmers.contains(s)) {
			swimmers.remove(s);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Niveau: " + level + "\n");
		sb.append("Timer i vand: " + weeklyWaterHours + "\n");
		sb.append("Timer i fitness: " + weeklyStrengthHours + "\n");

		if (swimmers.size() != 0) {
			sb.append("Svømmere: \n");
			for (Swimmer s : swimmers) {
				sb.append("* " + s.getName() + "\n");
			}
		}
		return sb.toString();
	}

	public void addSwimmer(Swimmer swimmer) {
		if (!swimmers.contains(swimmer)) {
			swimmers.add(swimmer);
			swimmer.setTrainingPlan(this);
		}
	}
}
