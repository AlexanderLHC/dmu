package ex2Gifts;

import java.util.ArrayList;

public class Person {
	private String name;
	private int age;
	private ArrayList<Gift> recievedGifts = new ArrayList<>();
	private ArrayList<Gift> givenGifts = new ArrayList<>();

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("-----------\n");
		sb.append(String.format("%s (%d old)%n", name, age));
		if (recievedGifts.size() > 0) {
			sb.append("Recieved gifts: \n");
			for (Gift gift : recievedGifts) {
				sb.append("- " + gift.getDescription() + "\n");
			}
		}
		if (givenGifts.size() > 0) {
			sb.append("Given gifts: \n");
			for (Gift gift : givenGifts) {
				sb.append("- " + gift.getDescription() + "\n");
			}
		}
		return sb.toString();
	}

	public void recieveGift(Gift gift) {
		recievedGifts.add(gift);
	}

	public void giveGift(Gift gift, Person reciever) {
//		gift.setGiver(this);
		givenGifts.add(gift);
		reciever.recieveGift(gift);
	}

	public double recivedGiftSumVal() {
		double recievedSumVal = 0;
		for (Gift gift : recievedGifts) {
			recievedSumVal += gift.getPrice();
		}
		return recievedSumVal;
	}

	public ArrayList<Person> getGiftGivers() {
		ArrayList<Person> giftGivers = new ArrayList<>();
		for (Gift gift : recievedGifts) {
			// if person is not already in the list
			if (!giftGivers.contains(gift.getGiver())) {
				giftGivers.add(gift.getGiver());

			}
		}
		return giftGivers;
	}
}
