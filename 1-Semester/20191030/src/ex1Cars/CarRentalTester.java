package ex1Cars;

public class CarRentalTester {

	/*
	Opgave 1:
	a. Kopier klasserne fra din løsning til opgave 3 på sidste opgaveseddel. 
	b. Programmér associeringen mellem Car og Rental, så det er en dobbeltrettet associering,
		som vist på diagrammet. 
	c. Tilføj en metode til Car, som finder det største antal dage en bil har været udlejet. 
	d. Lav en afprøv-klasse, der opretter nogle objekter laver sammenhæng mellem disse.
		Afprøv også metoden fra c).
	 */
	public static void main(String[] args) {
		// Dummy data
		Car car1 = new Car("carone1", 2000);
		car1.setDayPrice(100);
		Car car2 = new Car("cartwo2", 2010);
		car2.setDayPrice(80);
		Car car3 = new Car("carthree3", 2002);
		car3.setDayPrice(120);
		Car car4 = new Car("carfour4", 2014);
		Car car5 = new Car("carfive5", 2019);
		Rental r1 = new Rental(1, "20191028", 2);
		Rental r2 = new Rental(2, "20190910", 5);
		Rental r3 = new Rental(3, "20180621", 8);
		Rental r4 = new Rental(4, "20151028", 1);
		// a) check
		// b) check
		// c) 
		r1.addCar(car1);
		r2.addCar(car1);
		r3.addCar(car1);
		r4.addCar(car1);
		System.out.printf("Longest renting period for %s is %s for %d.%n%n", car1.getLicense(),
				car1.longestRentalPeriod().getDate(), car1.longestRentalPeriod().getDays());
	}
}