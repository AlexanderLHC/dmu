/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
/* 
 * 1: klasse definition
 * 2: String med småt
 * 3: Int med stor
 * 4: parameter adskilt komma metode Hund
 * 5: set navn er void
 * 6: parameter refererer til sig selv, burde være feltvariabel.
 * 7: getName returnerer string
 * 8: Navn i getName er med stort.
 * 9: getNummer mangler ()
 * 10: getNummer returnerer ingen int
 */
public class Hund_FindFejl {
// attribut variable
	private String navn;
	private int nummer;

	public Hund_FindFejl(String n, int nr) {
		navn = n;
		nummer = nr;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public int getNummer() {
		return nummer;
	}
}