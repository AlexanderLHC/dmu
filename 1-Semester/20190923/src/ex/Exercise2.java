/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class Exercise2 {

	/**
	 * @param args
	 */
	/* # Exercise 2
	 * To solve the following exercises, add some code to a main() method 
		1. Make a for loop that prints all uneven numbers in [1;100].
		2. Make a for loop that prints all multiples of 3 from 300 down to 3.
	 */
	public static void main(String[] args) {

		// 1)
		for (int i = 1; i <= 100; i++) {
			if (i % 2 != 0) {
				System.out.println(i);
			}
		}
		// 2)
		for (int i = 300; i >= 3; i -= 3) {
			System.out.println(i);
		}

	}

}
