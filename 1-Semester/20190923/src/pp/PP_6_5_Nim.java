package pp;

import java.util.Random;
import java.util.Scanner;

/*
 * The game of Nim. This is a well-known game with a number of variants.
 * The following variant has an interesting winning strategy.
 * Two players alternately take marbles from a pile.
 * In each move, a player chooses how many marbles to take.
 * The player must take at least one but at most half of the marbles.
 * Then the other player takes a turn. The player who takes the last marble loses.
 * 
 * Write a program in which the computer plays against a human opponent.
 * Generate a random integer between 10 and 100 to denote the initial size of the pile.
 * Generate a random integer between 0 and 1 to decide whether the computer or the human takes the first turn.
 * Generate a random integer between 0 and 1 to decide whether the computer plays smart or stupid.
 * In stupid mode the computer simply takes a random legal value (between 1 and n/2)
 *   from the pile whenever it has a turn.
 * In smart mode the computer takes off enough marbles to make the size of the pile a power 
 * of two minus 1—that is, 3, 7, 15, 31, or 63. That is always a legal move, 
 * except when the size of the pile is currently one less than a power of two.
 * In that case, the computer makes a random legal move.
 * You will note that the computer cannot be beaten in smart mode when it has the first move,
 * unless the pile size happens to be 15, 31, or 63. Of course, 
 * a human player who has the first turn and knows the winning strategy can win against the computer.
 */
public class PP_6_5_Nim {
	private int marblePile;
	private boolean isHumanTurn;
	private boolean doSmartMovePC; // decides whether computer moves smart
	private int pcIQ;
	private int turnNumber;

	public PP_6_5_Nim() {
		Random rng = new Random();
		marblePile = rng.nextInt(90) + 10;
		//		isHumanTurn = (rng.nextInt(2) == 1);
		isHumanTurn = true;
		doSmartMovePC = (rng.nextInt(2) == 1);
		turnNumber = 1;

	}

	public void start() {
		System.out.println("Fra 0-100 hvor kløgtig skal PC være?: ");
		Scanner scan = new Scanner(System.in);
		pcIQ = scan.nextInt();

		System.out.println("================");
		System.out.println("Antal kugler: " + marblePile);
		System.out.println("................");
		while (marblePile > 1) {
			turn();
		}
	}

	public void turn() {
		System.out.println("================");
		System.out.println("Tur: " + turnNumber + ". Antal kugler: " + marblePile);
		if (isHumanTurn) {
			playerMove();
		} else {
			pcMove();
		}
		turnNumber++;
		isHumanTurn = !isHumanTurn;
	}

	public void takeMarble(int amount) {
		if (marblePile - amount > 1) {
			marblePile -= amount;
		} else { // end of game
			System.out.println("================");
			System.out.println("================");
			if (!isHumanTurn) {
				System.out.println("PC Won. You lost..");
			} else {
				System.out.println("You won!!");
			}
			marblePile = 0; // ends turn loop
		}

	}

	public void playerMove() {
		System.out.println("Pick a number: ");
		Scanner scan = new Scanner(System.in);
		int amount = scan.nextInt();
		// if illegal move
		if (amount == 0 || amount > marblePile || amount > marblePile / 2) {
			System.out.println("Illegal move. Try again: ");
			playerMove();
		} else { // legal move
			takeMarble(amount);
		}

	}

	public int getLargestPowOfTwo(int pileSize) {
		int largestSum = 0;
		int expo = 1;

		while (largestSum <= pileSize) {
			largestSum = (int) Math.pow(2, expo);
			expo++;
		}
		return largestSum;
	}

	public void pcMove() {
		Random rng = new Random();
		int amount = 0;
		if (rng.nextInt(100) <= pcIQ) { // smart move
			if (getLargestPowOfTwo(marblePile) - 1 == marblePile) {
				System.out.println("random smart move");
				amount = rng.nextInt(marblePile / 2) + 1;
			} else {
				System.out.println("calculating smart move");
				int bestPossibleMove = 0;
				while ((marblePile / 2) - 1 > bestPossibleMove) {

				}
				//				amount = getLargestPowOfTwo(marblePile / 2) - 1;
			}
		} else { // "stupid" move
			amount = rng.nextInt(marblePile / 2) + 1;
		}
		System.out.println("PC picked: " + amount);
		takeMarble(amount);
	}

}
