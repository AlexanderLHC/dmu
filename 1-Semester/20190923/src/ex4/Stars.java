package ex4;

public class Stars {
	public void starExA(int rowCount) {
		for (int row = 1; row <= rowCount; row++) {
			int starCount = rowCount - row + 1;
			int dashCount = rowCount - starCount;
			printRow(row, starCount, dashCount);
		}

	}

	public void starExB(int rowCount) {
		for (int row = 1; row <= rowCount; row++) {
			int dashCount = rowCount - row;
			int starCount = rowCount - dashCount;
			printRow(row, starCount, dashCount);
		}

	}

	public void starExC(int rowCount) {
		for (int row = 1; row <= rowCount; row++) {
			int starCount = rowCount - row + 1;
			int dashCount = rowCount - starCount;
			printRow(row, starCount, dashCount);
		}

	}

	public void starExD(int rowCount) {
		int row = 0;
		int starCount = 1;
		for (int i = 0; i <= rowCount; i++) {
			System.out.printf("%2d: ", row);
			for (int j = 0; j <= rowCount; j++) {
				if (j <= 10 / 2 - starCount || j >= 10 / 2 + starCount) {
					System.out.print("-");
				} else {
					System.out.print("*");
				}
			}
			if (i < rowCount / 2) {
				starCount++;
			} else {
				starCount--;
			}
			System.out.println("");
			row++;

		}
	}

	public void printRow(int row, int starCount, int dashCount) {
		System.out.printf("%2d: ", row);
		for (int i = 1; i <= starCount; i++) {
			System.out.print("*");
		}
		for (int i = 1; i <= dashCount; i++) {
			System.out.print("-");
		}
		System.out.println();

	}

}
