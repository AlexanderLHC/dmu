package loops;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Exercise8 extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(1000, 1000);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D());
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(GraphicsContext gc) {
		// ======== Exercise 8 ======== //
		/* 
		 Create an application that draws 10 vertical lines as shown in the figure below.
		 The first line’s height is 80 and the height of the next lines is decreased by 8.
		 The lower endpoint of the lines are at */

		int x = 16;
		int y = 80;
		int y2 = 160;

		for (int i = 0; i < 10; i++) {
			gc.strokeLine(x * i, y - (4 * i), x * i, y2 - (12 * i));
		}
	}

}
