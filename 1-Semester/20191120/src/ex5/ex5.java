package ex5;

import java.util.ArrayList;

import com.github.javafaker.Faker;

/*
 Exercise 5
	Write a method that returns all indexes of a given String in a given ArrayList<String>.
	The header of the method:  
	public static ArrayList<Integer> findAllIndices(ArrayList<String> list, String 	target)
 
 */
public class ex5 {

	public static void main(String[] args) {
		Faker faker = new Faker();
		String toInfiniteStringAndBeyond = faker.hitchhikersGuideToTheGalaxy().quote();
		for (int i = 0; i < 10; i++) {
			toInfiniteStringAndBeyond += " " + faker.hitchhikersGuideToTheGalaxy().quote();
		}
		System.out.println(toInfiniteStringAndBeyond);
		String[] bookOfQuotes = toInfiniteStringAndBeyond.split(" ");
		System.out.printf("1: for above quote the word 'are' appears %s%n", findAllIndices(bookOfQuotes, "are").size());

		String morgenstundString = "Morgenstund"
				+ "har guld i mund \n"
				+ "for natten Gud vi love \n"
				+ "han lærte os, i Jesu navn, \n"
				+ "som barnet i sin moders favn \n"
				+ "vi alle sødt kan sove. \n"
				+ "Morgenstund \n"
				+ "har guld i mund; \n"
				+ "vi til vort arbejd ile, \n"
				+ "som fuglen glad i skov og vang \n"
				+ "udflyver med sin morgensang, \n"
				+ "genfødt ved nattehvile. \n"
				+ "Morgenstund \n"
				+ "har guld i mund, \n"
				+ "og guld betyder glæde, \n"
				+ "og glædelig er hver en dag, \n"
				+ "som leves til Guds velbehag, \n"
				+ "om end vi måtte græde. \n"
				+ "Gå da frit \n"
				+ "enhver til sit \n"
				+ "og stole på Guds nåde! \n"
				+ "Da får vi lyst og lykke til \n"
				+ "at gøre gavn, som Gud det vil, \n"
				+ "på allerbedste måde. \n"
				+ "Sol opstår, \n"
				+ "og sol nedgår, \n"
				+ "når den har gjort sin gerning \n"
				+ "Gud give os at skinne så, \n"
				+ "som himmellys, skønt af de små! \n"
				+ "Da randt for os guldterning \n";

		String[] morgenStundArray = morgenstundString.split(" ");

		System.out.println(morgenstundString);
		System.out.printf("2: for above quote the word 'sol' appears in above %s%n",
				findAllIndices(morgenStundArray, "sol").size());
		System.out.printf("3: for above quote the word 'guldterning' appears in above %s%n",
				findAllIndices(morgenStundArray, "guldterning").size());

	}

	public static ArrayList<Integer> findAllIndices(String[] list, String target) {
		ArrayList<Integer> locations = new ArrayList<>();

		for (int i = 0; i < list.length; i++) {
			if (list[i].compareTo(target) == 0) {
				locations.add(i);
			}
		}
		return locations;
	}

}
