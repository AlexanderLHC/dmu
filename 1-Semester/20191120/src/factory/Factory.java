package factory;

import java.util.Random;

public class Factory {

	public static int evenOrOdd(int chanceOdd) {
		Random random = new Random();
		int n = random.nextInt(10);
		int r = 2 * n;
		if (chanceOdd > random.nextInt(100)) {
			r += 1;
		}
		return r;
	}

	public static int[] getArray(int size, int from, int to) {
		int[] array = new int[size];
		Random random = new Random();
		for (int i = 0; i < size; i++) {
			array[i] = random.nextInt(to) + from;
		}
		return array;
	}

}
