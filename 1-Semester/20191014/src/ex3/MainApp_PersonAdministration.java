package ex3;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp_PersonAdministration extends Application {
	private ListView<PersonEx3> lwPeople = new ListView<PersonEx3>();
	private ArrayList<PersonEx3> people = new ArrayList<PersonEx3>();

	private WindowAddPerson personWindow;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 3: Person Administration");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();

		personWindow = new WindowAddPerson("Add a person", stage);
	}

	// -------------------------------------------------------------------------

	// Field variables

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// Label
		Label lblPeople = new Label("People: ");

		// Fields

		// Buttons
		Button btnAddPerson = new Button("Add");
		btnAddPerson.setOnAction(event -> addPerson());

		// Pane
		pane.add(lblPeople, 0, 0);
		pane.add(lwPeople, 0, 1, 2, 2);
		pane.add(btnAddPerson, 3, 1);

	}

	// -------------------------------------------------------------------------
	private void addPerson() {
		personWindow.showAndWait();

		if (personWindow.getPerson() != null) {
			PersonEx3 person = personWindow.getPerson();
			people.add(person);
			lwPeople.getItems().setAll(people);
		}
	}
}