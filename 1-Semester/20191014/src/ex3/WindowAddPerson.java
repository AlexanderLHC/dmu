package ex3;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WindowAddPerson extends Stage {

	public WindowAddPerson(String title, Stage stageOwner) {
		this.initOwner(stageOwner);
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(100);
		this.setMinWidth(200);

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// -------------------------------------------------------------------------

	// Field variables
	TextField txfName = new TextField();
	TextField txfTitle = new TextField();
	CheckBox chkIsSenior = new CheckBox();
	PersonEx3 person = null;

	private void initContent(GridPane pane) {
		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// Label
		Label lblName = new Label("Name:");
		Label lblTitle = new Label("Title:");
		Label lblIsSenior = new Label("Senior");

		// Fields

		// Buttons
		Button btnOK = new Button("OK");
		btnOK.setOnAction(event -> okAction());
		Button btnCancel = new Button("Cancel");

		// Pane
		// Row 0
		int row = 0;
		pane.add(lblName, 0, row);
		pane.add(txfName, 1, row);
		// Row 1
		row++;
		pane.add(lblTitle, 0, row);
		pane.add(txfTitle, 1, row);
		// Row 2
		row++;
		pane.add(lblIsSenior, 1, row);
		pane.add(chkIsSenior, 0, row);
		// Row 3
		row++;
		pane.add(btnOK, 0, row);
		pane.add(btnCancel, 1, row);

	}

	// -------------------------------------------------------------------------
	private void addPerson() {
		String name = txfName.getText();
		String title = txfTitle.getText();
		boolean isSenior = false;
		if (chkIsSenior.isSelected()) {
			isSenior = true;
		}
		if (name.length() > 0 && title.length() > 0) {
			person = new PersonEx3(name, title, isSenior);
			txfName.clear();
			txfTitle.clear();
			txfName.requestFocus();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Person adder");
			alert.setHeaderText("Information missing");
			alert.setContentText("Enter both title and name");
			alert.show();
		}
	}

	private void okAction() {
		this.addPerson();
		WindowAddPerson.this.hide();
	}

	public PersonEx3 getPerson() {
		return person;
	}

	public void cancelAction() {
		person = null;
	}
}
