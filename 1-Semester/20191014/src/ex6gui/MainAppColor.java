package ex6gui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MainAppColor extends Application {
	Slider sliderBlue = new Slider(0, 255, 1);
	Slider sliderRed = new Slider(0, 255, 1);
	Slider sliderGreen = new Slider(0, 255, 1);

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 2: Incrementer and Decrementer");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		pane.add(sliderBlue, 1, 1);
		pane.add(sliderRed, 2, 1);
		pane.add(sliderGreen, 3, 1);
		// Sliders
		sliderBlue.valueProperty().addListener(
				event -> this.sliderValueChanged(pane));
		sliderRed.valueProperty().addListener(
				event -> this.sliderValueChanged(pane));

		sliderGreen.valueProperty().addListener(
				event -> this.sliderValueChanged(pane));
	}

	private String getColor() {
		int blue = (int) sliderBlue.getValue();
		int red = (int) sliderRed.getValue();
		int green = (int) sliderGreen.getValue();
		String blueDec = String.format("%02X", blue);
		String redDec = String.format("%02X", red);
		String greenDec = String.format("%02X", green);
		return "#" + blueDec + redDec + greenDec;
	}

	private void sliderValueChanged(Pane pane) {
		String color = getColor();
		pane.setStyle("-fx-background: " + color + ";");
	}

}
