package ex1And2;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 1 and 2: Add person");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	// Fieldvariables
	private ArrayList<Person> persons = new ArrayList<Person>();
	private TextField txfName = new TextField();
	private TextField txfTitle = new TextField();
	private CheckBox chkIsSenior = new CheckBox();
	private ListView<Person> lwPersons = new ListView<>();
	private HBox boxSenior = new HBox();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// ??

		// Label
		Label lblName = new Label("Name: ");
		Label lblTitle = new Label("Title: ");
		Label lblPersons = new Label("Persons: ");
		Label lblSenior = new Label("Senior");
		// Boxes
		boxSenior.getChildren().add(chkIsSenior);
		boxSenior.getChildren().add(lblSenior);
		// Fields

		// Buttons
		Button btnAddPerson = new Button("Add person");
		btnAddPerson.setOnAction(event -> addPerson());

		// Pane
		int row = 0;
		// Row 0
		pane.add(lblName, 0, row);
		pane.add(txfName, 1, row);
		// Row 1
		row = 1;
		pane.add(lblTitle, 0, row);
		pane.add(txfTitle, 1, row);
		// Row 2
		row = 2;
		pane.add(boxSenior, 1, row);
		pane.add(btnAddPerson, 2, row);
		// Row 3
		row = 3;
		pane.add(lblPersons, 0, row);
		pane.add(lwPersons, 1, row);

	}

	// -------------------------------------------------------------------------
	private void addPerson() {
		String name = txfName.getText();
		String title = txfTitle.getText();
		boolean isSenior = false;
		if (chkIsSenior.isSelected()) {
			isSenior = true;
		}
		if (name.length() > 0 && title.length() > 0) {
			Person person = new Person(name, title, isSenior);
			persons.add(person);
			lwPersons.getItems().setAll(persons);
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Add person");
			alert.setHeaderText("Input name or title missing");
			alert.setContentText("Type the name and title of the person.");
			alert.show();
		}
	}
}
