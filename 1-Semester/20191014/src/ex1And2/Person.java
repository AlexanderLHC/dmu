package ex1And2;

public class Person {
	private String name;
	private String title;
	private boolean isSenior;

	public Person(String name, String title, boolean isSenior) {
		this.name = name;
		this.title = title;
		this.isSenior = isSenior;
	}

	@Override
	public String toString() {
		String s = title + " " + name;
		if (isSenior) {
			s = s + " (senior)";
		}
		return s;
	}
}