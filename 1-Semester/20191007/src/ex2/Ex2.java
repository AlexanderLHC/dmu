package ex2;

import java.util.ArrayList;

public class Ex2 {

	/*
	 Exercise 2
		Add the following methods to the Ex2 class (use for-each loops 
		in all the methods). Also add code to test the methods.
	
	 */
	/**
	 * Tests all the methods.
	 */
	public static void main(String[] args) {
		ArrayList<Integer> ints = new ArrayList<Integer>();
		ints.add(12);
		ints.add(0);
		ints.add(45);
		ints.add(7);
		ints.add(-16);
		ints.add(0);
		ints.add(23);
		ints.add(-10);
		//        ints.addAll(Arrays.asList(12, 0, 45, 7, -16, 0, 23, -10));
		System.out.println("ints: " + ints);
		System.out.println();

		// Test of sum() method: correct sum is 61.
		int total = sum1(ints);
		System.out.println("Sum: " + total);

		// Test of minimum() method: correct minimum is -16.
		System.out.println("Minimum (expected -16)" + minimum(ints));

		// Test of maximum() method: correct maximum is 45.
		System.out.println("Maximum (expected 45)" + maximum(ints));

		// Test of average() method: correct average is 7.625.
		System.out.println("Average (expected 7.625)" + average(ints));

		// Test of zeroes() method: correct number of zeroes is 2.
		System.out.println("Zeroes (expected 2)" + zeroes(ints));

		// Test of evens() method: correct result is [12, 0, -16, 0, -10].
		System.out.println("Evens (expected [12, 0, -16, 0, -10])" + evens(ints));
	}

	// sum made with for statement
	public static int sum1(ArrayList<Integer> list) {
		int sum = 0;
		for (int i = 0; i < list.size(); i++) {
			int number = list.get(i);
			sum = sum + number;
		}
		return sum;
	}

	/**
	 * Returns the sum of all numbers in the list. Returns 0 if the list is empty.
	 */
	public static int sum(ArrayList<Integer> list) {
		int sum = 0;
		for (Integer integer : list) {
			sum += integer;
		}
		return sum;
	}

	/**
	 * Returns the minimum of all numbers in the list. Requires: The list is not
	 * empty.
	 */
	public static int minimum(ArrayList<Integer> list) {
		int min = list.get(0);
		for (Integer integer : list) {
			if (min > integer) {
				min = integer;
			}
		}
		return min;
	}

	/**
	 * Returns the maximum of all numbers in the list. Requires: The list is not
	 * empty.
	 */
	public static int maximum(ArrayList<Integer> list) {
		int max = list.get(0);
		for (Integer integer : list) {
			if (max < integer) {
				max = integer;
			}
		}
		return max;
	}

	/**
	 * Returns the average of the numbers in the list. Requires: The list is not
	 * empty.
	 */
	public static double average(ArrayList<Integer> list) {
		return sum(list) * 1.0 / list.size();
	}

	/** Returns the number of zeros in the list. */
	public static int zeroes(ArrayList<Integer> list) {
		int zeroCounter = 0;
		for (Integer integer : list) {
			if (integer == 0) {
				zeroCounter++;
			}

		}
		return zeroCounter;
	}

	/** Returns a new list containing the even numbers in the list. */
	public static ArrayList<Integer> evens(ArrayList<Integer> list) {
		ArrayList<Integer> evens = new ArrayList<>();

		for (Integer integer : list) {
			if (integer % 2 == 0) {
				evens.add(integer);
			}
		}
		return evens;
	}

}
