package ex5;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileUtil {

	public static int max(String filename) {
		int max = -1;
		File f = new File(filename);

		try (Scanner file = new Scanner(f);) {
			while (file.hasNext()) {
				int number = Integer.parseInt(file.nextLine());

				if (number > max) {
					max = number;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return max;
	}

	public static int min(String filename) {
		int min = -1;
		File f = new File(filename);

		try (Scanner file = new Scanner(f)) {
			min = Integer.parseInt(file.nextLine()); // makes sure that valid min value is returned.
			while (file.hasNext()) {
				int number = Integer.parseInt(file.nextLine());

				if (number < min) {
					min = number;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return min;
	}

	public static double average(String filename) throws IOException {
		double average = 0;
		File f = new File(filename);
		Scanner file = new Scanner(f);
		int lines = 0;
		while (file.hasNext()) {
			average += Double.parseDouble(file.nextLine());
			lines++;
		}
		file.close();
		return average / lines;
	}

}
