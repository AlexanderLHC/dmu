package ex5;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Ex5MainApp {
	private static Scanner console;
	/*
	Opgave 5
	Til denne opgave benyttes filen filer.jar, som i klassen NumberFileGenerator har et eksempel
	på skrivning af ikke-negative tal til en fil.
	Lav med denne applikation et par filer med et antal heltal. Den første fil kan passende
	indeholde 10 tal, den næste 2500 tal.
	Skriv en applikation med to klasser: FileUtil og FileUtilTest.
	a) FileUtil-klassen skal have 3 metoder:
		- public int max(String filename)
		som finder maximum for tallene i filen med filnavnet filename.
		- public int min(String filename)
		som finder minimum for tallene i filen med filnavnet filename.
		- public double gennemsnit(String filename) throws IOException som finder gennemsnittet af tallene 
			i filen med filnavnet filename.
	
		Det antages, at filerne kun indeholder heltal. Metoderne må ikke gemme tallene i en	ArrayList.
	b) FileUtilTest-klassen skal teste de tre metoder på de to filer, du lavede i starten af opgaven.
	 */

	public static void main(String[] args) {
		boolean finished = false;
		console = new Scanner(System.in);
		int fileNum = 1;

		while (!finished) {
			System.out.println("Proceed (y/n):");
			String answer = console.next().toLowerCase().trim();
			if (answer.compareTo("n") == 0) {
				finished = true;
				System.out.println("Finished");
			} else {
				String filename = "ex5-" + fileNum + ".txt";
				createPositiveIntFile(filename);
				FileUtilTest.testFileUtils(filename);
				fileNum++;
			}
		}
		console.close();
	}

	public static void createPositiveIntFile(String filename) {
		int antal = 0;

		while (antal <= 0) {
			try {
				System.out.print("Antal tal der skal skrives i filen: ");
				antal = console.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("Antal skal være et positivt heltal.");
			}
		}

		try (PrintWriter printWriter = new PrintWriter(filename)) {
			Random rnd = new Random();
			for (int i = 1; i <= antal; i++) {
				int number = rnd.nextInt(10000);
				printWriter.println(number);
			}
			printWriter.close();
			System.out.println("Fil med " + antal + " tal nu er lavet.");
		} catch (FileNotFoundException ex) {
			System.out.println("Error finding or creating file: " + filename);
		}
	}
}
