package exercise2;

import java.util.InputMismatchException;
import java.util.regex.Pattern;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class PersonInputWindow extends Stage {
	private Person person;
	private TextField txfName, txfAge;
	private Label lblNameError, lblAgeError, lblError;
	private GridPane pane;

	public PersonInputWindow(Stage owner) {
		this.initOwner(owner);

		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(200);
		this.setMinWidth(200);
		this.setResizable(false);

		this.setTitle("Add person");
		pane = new GridPane();
		this.initContent();

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private void initContent() {
		Label lblName = new Label("Name");
		Label lblAge = new Label("Age");
		txfName = new TextField();
		lblNameError = new Label("");
		txfAge = new TextField();
		lblAgeError = new Label("");
		lblError = new Label("");
		Button btnOk = new Button("OK");
		btnOk.setOnAction(e -> this.okAction());
		Button btnCancel = new Button("Cancel");
		btnCancel.setOnAction(e -> this.cancelAction());

		txfName.focusedProperty().addListener((obs, oldVal, newVal) -> {
			if (newVal) {
				verifyName(txfName.getText().trim());	
			}
		}
		);
		txfAge.focusedProperty().addListener((obs, oldVal, newVal) -> verifyAge(txfAge.getText().trim()));

		pane.add(lblName, 0, 0);
		pane.add(txfName, 1, 0);
		pane.add(lblNameError, 2, 0);
		pane.add(lblAge, 0, 1);
		pane.add(txfAge, 1, 1);
		pane.add(lblAgeError, 2, 1);
		pane.add(btnOk, 0, 2);
		pane.add(btnCancel, 2, 2);
		pane.add(lblError, 0, 3, 2, 1);

	}

	private void verifyName(String name) throws InputMismatchException {
		lblNameError.setText("");
		lblError.setText("");

		if (txfName.getText().length() == 0) {
			throw new InputMismatchException();
		}
	}

	private void verifyAge(String age) throws InputMismatchException {
		lblAgeError.setText("");
		lblError.setText("");

		try {
			if (Pattern.matches("^[1-9]\\d*", age)) {
				throw new InputMismatchException();
			}
		} catch (InputMismatchException e) {
			lblAgeError.setText("**");
			lblError.setText("Age is invalid");
		}
	}

	private void okAction() {
		String name = "";
		String age = "";

		lblNameError.setText("");
		lblAgeError.setText("");
		try {
			verifyName(name);
			verifyAge(age);
			person = new Person(name, Integer.parseInt(age));
			this.closeWindow();
		} catch (InputMismatchException e) {
			if (e.toString() == "name") {
				lblNameError.setText("**");
				lblError.setText("Name is invalid");
			} else if (e.toString() == "age") {
				lblAgeError.setText("**");
				lblError.setText("Age is invalid");
			}
		}

	}

	public Person getPerson() {
		return person;
	}

	public void clearPerson() {
		person = null;
	}

	private void clearWindow() {
		for (Node node : pane.lookupAll("TextField")) {
			if (node instanceof TextField) {
				((TextField) node).setText("");
			}
		}
		lblNameError.setText("");
		lblAgeError.setText("");
	}

	private void closeWindow() {
		this.clearWindow();
		this.hide();
	}

	private void cancelAction() {
		this.closeWindow();
	}
}
