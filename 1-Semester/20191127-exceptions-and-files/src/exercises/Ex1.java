package exercises;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ex1 {
	/*
	 * The program below reads an index and prints the prime number in the array at
	 * the index. The program tries to force the user to to input an integer in
	 * 0..9, but it doesn't work.
	 * 
	 * 
	 * a) Run the program and input an integer below and above 9. Input a
	 * non-integer. Remember the exceptions thrown.
	 * 
	 * - java.lang.ArrayIndexOutOfBoundsException - java.util.InputMismatchException
	 * 
	 * b) Correct the program: Catch the exceptions (tell the user that the index
	 * was not an integer, or an integer not in 0..9).
	 * 
	 * c) Replace the catch for an integer not in 0..9 with an if-else statement.
	 */

	public static void main(String[] args) {
		int[] primes = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29 };
		try (Scanner scanner = new Scanner(System.in)) {
			int index = -1;
			while (index < 0 || index > 9) {
				System.out.println("Type in an index (0..9): ");
				index = scanner.nextInt();
				if (index > 0 && index <= 9) {
					System.out.printf("Prime at index %d is %d", index, primes[index]);
				} else {
					System.out.println("FEJL: Ugyldigt tal. Tal skal være mellem 0-9.");
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("FEJL: Tallet ligger ikke mellem [0-9]!");
		} catch (InputMismatchException e) {
			System.out.println("FEJL: Ikke gyldigt tal. Kun tal fra [0-9]!");
		}
	}

}
