package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Opgave3 {
	/*
	 Opgave 3
	Lav en applikation der i main-metoden skriver en fil med alle positive, 
	ulige tal mindre end 100.
	 */

	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("even-numbers.txt");
		PrintWriter out = new PrintWriter(file);

		// for (int i = 0; i < 100; i++) {
		// if (i % 2 != 0) {
		// out.println(i);
		// }
		// }
		for (int i = 1; i < 100; i += 2) {
			out.println(i);
		}

		out.close();
		Scanner in = new Scanner(file);

		while (in.hasNext()) {
			System.out.println(in.next());
		}

		in.close();
	}

}
