package exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Opgave2 {
	/*
	 Opgave 2
	Læs filen fra opgave 1 og indsæt tallene i en ArrayList<Integer> efterhånden som de	indlæses.
	Udskriv dernæst tallene fra arraylisten i omvendt rækkefølge, dvs. 285, 177, ..., 34.
	 */

	public static void main(String[] args) {
		File file = new File("ex1.txt");

		try {
			System.out.println(getFileAsReverseArray(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/*
	 * precondition: the file only contains valid integers
	 */
	public static ArrayList<Integer> getFileAsReverseArray(File inputFile) throws FileNotFoundException {
		ArrayList<Integer> fileReversed = new ArrayList<>();
		Scanner file = new Scanner(inputFile);

		while (file.hasNext()) {
			fileReversed.add(0, Integer.parseInt(file.next()));
		}

		file.close();
		return fileReversed;
	}

}
