package ex3;

public class Person {
	private String name;
	private String address;
	private Integer monthlySalary;
	private Double bonus;

	public Person(String name, String adress, Integer monthlySalary) {
		this.name = name;
		this.address = adress;
		this.monthlySalary = monthlySalary;
		this.bonus = 0.025;
	}

	/**
	 * Set persons name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns persons name
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set persons address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Returns persons address
	 * 
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Set persons monthly salary
	 */
	public void setMonthlySalary(Integer monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	/**
	 * Returns monthly salary
	 * 
	 * @return montlySalary
	 */
	public Integer getMonthlySalary() {
		return monthlySalary;
	}

	/**
	 * Returns the yearly payout in dollars
	 * 
	 * @return yearlyPayout
	 */
	public double getYearlyPayout() {
		double yearlyPayout = monthlySalary * 12 * (1 + bonus);
		return yearlyPayout;
	}

	public void printPerson() {
		System.out.println("########################");
		System.out.println("# Name          : " + name);
		System.out.println("# Address       : " + address);
		System.out.println("# Salary        : " + monthlySalary);
		System.out.println("# Yearly Payout : " + this.getYearlyPayout() + "$");
		System.out.println("########################");
	}

	/**
	 * Returns a description of the person (for test purposes).
	 */
	@Override
	public String toString() {
		return name + " (address: " + address + "). Salary: " + monthlySalary;
	}

}
