package semesterprøve2017;

import java.util.ArrayList;

public class Arrangement {
	private String navn;
	private boolean offentlig;
	private ArrayList<Reservation> reservationer = new ArrayList<>();

	public Arrangement(String navn, boolean offentlig) {
		this.navn = navn;
		this.offentlig = offentlig;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public boolean isOffentlig() {
		return offentlig;
	}

	public void setOffentlig(boolean offentlig) {
		this.offentlig = offentlig;
	}

	public void addReservation(Reservation reservation) {
		if (!reservationer.contains(reservation)) {
			reservationer.add(reservation);
		}
	}

	public void removeReservation(Reservation reservation) {
		if (reservationer.contains(reservation)) {
			reservationer.remove(reservation);
		}
	}

	public ArrayList<Reservation> getReservationer() {
		return reservationer;
	}

	public int antalReserveredePladser() {
		int antalReservationer = 0;
		for (Reservation reservation : reservationer) {
			antalReservationer += reservation.getPladser().size();
		}
		return antalReservationer;
	}

}
