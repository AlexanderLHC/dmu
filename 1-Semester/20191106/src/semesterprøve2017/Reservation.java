package semesterprøve2017;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Reservation {
	private LocalDateTime start;
	private LocalDateTime slut;
	private ArrayList<Plads> pladser = new ArrayList<>();

	public Reservation(LocalDateTime start, LocalDateTime slut, Plads plads) {
		this.start = start;
		this.slut = slut;
		pladser.add(plads);
	}

	public Reservation(LocalDateTime start, LocalDateTime slut, ArrayList<Plads> pladser) {
		this.start = start;
		this.slut = slut;
		this.pladser = pladser;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public LocalDateTime getSlut() {
		return slut;
	}

	public void setSlut(LocalDateTime slut) {
		this.slut = slut;
	}

	public void addPlads(Plads plads) {
		if (!pladser.contains(plads)) {
			pladser.add(plads);
			plads.addReservation(this);
		}
	}

	public void removePlads(Plads plads) {
		if (pladser.contains(plads)) {
			pladser.remove(plads);
			plads.removeReservationer(this);
		}
	}

	public ArrayList<Plads> getPladser() {
		return pladser;
	}

}
