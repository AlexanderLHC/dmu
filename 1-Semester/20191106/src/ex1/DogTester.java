package ex1;

import java.util.ArrayList;

public class DogTester {
	/*
	 * Lav en opremsningstype med navn Race, der kan antage værdierne: 
	
		PUDDEL, BOKSER og TERRIER. 
		 
		Lav en klasse Hund der har attributterne: 
		• navn der en String 
		• stamtavle der er en boolean 
		• pris der er et heltal 
		• race der er af typen Race 
		 
		Lav i en test klasse metoden: 
		 
		public static int samletPris(ArrayList<Hund> hunde, Race race){} 
	 */
	public static void main(String[] args) {
		Dog d1Dog = new Dog("Fido", true, 500, Race.POODLE);
		Dog d2Dog = new Dog("Mads", true, 500, Race.TERRIER);
		Dog d3Dog = new Dog("Victor", false, 200, Race.BOXER);
		Dog d4Dog = new Dog("Søren", false, 150, Race.POODLE);
		Dog d5Dog = new Dog("Alexander", true, 1500, Race.TERRIER);
		ArrayList<Dog> dogs = new ArrayList<>();
		dogs.add(d1Dog);
		dogs.add(d2Dog);
		dogs.add(d3Dog);
		dogs.add(d4Dog);
		dogs.add(d5Dog);

		for (Race race : Race.values()) {
			System.out.printf("Price for dogs of race: %s = %d.%n", race.name(), overallPrice(dogs, race));
		}
	}

	public static int overallPrice(ArrayList<Dog> dogs, Race race) {
		int price = 0;
		for (Dog dog : dogs) {
			if (dog.getRace().equals(race)) {
				price += dog.getPrice();
			}
		}
		return price;
	}
}
