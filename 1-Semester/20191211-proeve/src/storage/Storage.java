package storage;

import java.util.ArrayList;

import model.Bil;
import model.Bilmærke;
import model.Parkeringshus;

public class Storage {

	private static final ArrayList<Parkeringshus> parkeringshuse = new ArrayList<>();
	private static final ArrayList<Bil> biler = new ArrayList<>();
	
	// ------------------- Parkeringshus -------------------
	public static Parkeringshus createParkeringshus(String adresse) {
		Parkeringshus parkeringshus = new Parkeringshus(adresse);
		parkeringshuse.add(parkeringshus);
		return parkeringshus;
	}
	public static void addParkeringshus(Parkeringshus parkeringsHus) {
		parkeringshuse.add(parkeringsHus);
	}
	// Husk at returnerer en ny arrayliste
	public static ArrayList<Parkeringshus> getParkeringshuse(){
		return new ArrayList<Parkeringshus>(parkeringshuse);
	}
	// ------------------- Bil -------------------
	public static Bil createBil(String regNr, Bilmærke mærke) {
		Bil bil = new Bil(regNr, mærke);
		biler.add(bil);
		return bil;
	}
	public static void addBil(Bil bil) {
		biler.add(bil);
	}
	
	public static ArrayList<Bil> getBiler() {
		return new ArrayList<Bil>(biler);
	}
}

