package model;
import java.util.ArrayList;

public class Parkeringshus {
	private final ArrayList<Parkeringsplads> parkeringsPladser = new ArrayList<>();
	private String adresse;
	private double saldo;
	
	public Parkeringshus(String adresse) {
		this.adresse = adresse;
		saldo = 0;
	}

	public double getSaldo() {
		return saldo;
	}

	// Er det ikke meningsløst med en saldo der kan sættes i værdi?
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public void insertSaldo(double amount) {
		saldo += amount;
	}
	
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	

	/*
	 * Foreslået at have createParkeringsplads og createInvalidPlads
	 */
	public Parkeringsplads createParkeringsplads(int nummer) {
		Parkeringsplads pPlads = new Parkeringsplads(nummer);
		pPlads.setParkeringsHus(this);
		parkeringsPladser.add(pPlads);
		return pPlads;
	}
	
	public Invalideplads createParkeringsplads(int nummer, double areal) {
		Invalideplads pPlads = new Invalideplads(nummer, areal);
		pPlads.setParkeringsHus(this);
		parkeringsPladser.add(pPlads);
		return pPlads;
	}

	
	public void removeParkeringsplads(Parkeringsplads pPlads) {
		if (parkeringsPladser.contains(pPlads)) {
			pPlads.setParkeringsHus(null);
			parkeringsPladser.remove(pPlads);
		}
	}
	
	public ArrayList<Parkeringsplads> getParkeringspladser(){
		return new ArrayList<>(parkeringsPladser);
	}
	
	public int antalLedigePladser() {
		int ledigePladser = 0;
		for (Parkeringsplads plads : parkeringsPladser) {
			if (plads.getBil() == null) {
				ledigePladser++;
			}
		}
		return ledigePladser;
	}
	
	public int findPladsMedBil(String regNummer) {
		int pladsFundet = -1;
		int i = 0;
		while (pladsFundet == -1 && i < parkeringsPladser.size()) {
			Parkeringsplads plads = parkeringsPladser.get(i);
			// Husk at tjekke om der holder en bil
			if (plads.getBil() != null && plads.getBil().getRegNr().equals(regNummer)) {
				pladsFundet = plads.getNummer();
			}
			i++;
		}
		return pladsFundet;
	}
	
	public int findAntalBiler(Bilmærke mærke) {
		int found = 0;
		for (Parkeringsplads p : parkeringsPladser) {
			if (p.getBil() != null && p.getBil().getMærke().equals(mærke)) {
				found++;
			}
		}
		return found;
	}
	
	public ArrayList<String> optagnePladser() {
		ArrayList<String> optagnePladser = new ArrayList<String>();
		for (Parkeringsplads p : parkeringsPladser) {
			Bil bil = p.getBil();
			if (bil != null) {
				optagnePladser.add(String.format("Regnr: %s. Mærke: %s", bil.getRegNr(), bil.getMærke()));
			}
		}
		return optagnePladser;
	}
	
	@Override
	public String toString() {
		return String.format("%s (saldo: %skr - ledige pladser: %s)", adresse, saldo, antalLedigePladser());
	}
	
	
}
