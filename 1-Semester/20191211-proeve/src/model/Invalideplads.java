package model;
import java.time.LocalTime;

public class Invalideplads extends Parkeringsplads {
	// FEJL: nedarver ikke statiske variable
	private final double PRICE = 0; // unused but clarifying that these places are free
	
	private double areal;

	public Invalideplads(int nummer, double areal) {
		super(nummer);
		this.areal = areal;
	}
	
	public double getAreal() {
		return areal;
	}

	public void setAreal(double areal) {
		this.areal = areal;
	}
	
	@Override
	public double getPris(LocalTime finishedParking) {
		return 0.0;
	}

}
