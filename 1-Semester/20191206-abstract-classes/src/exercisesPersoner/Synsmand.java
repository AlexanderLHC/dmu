package exercisesPersoner;

import java.time.LocalDate;

public class Synsmand extends Mekaniker{
	private static Double tillæg = 29.0; // kr
	private int ugentligeSyn;
	
	public Synsmand(String navn, String adresse, LocalDate svendePrøveDate) {
		super(navn, adresse, svendePrøveDate);
		ugentligeSyn = 0;
	}
	
	public void addSyn() {
		ugentligeSyn++;
	}
	
	public void nulstilSyn() {
		ugentligeSyn = 0;
	}
	
	public int getUgentligeSyn() {
		return ugentligeSyn;
	}

	@Override
	public String toString() {
		return super.toString() + String.format(" %-5skr | %-10s |", tillæg, ugentligeSyn);
	}

	@Override
	public double ugeLøn() {
		return super.ugeLøn() + (ugentligeSyn * tillæg);
	}
}
