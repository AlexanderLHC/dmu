package exercisesPersoner;

import java.time.LocalDate;
import java.util.Date;

public class Værkfører extends Ansat{
	private LocalDate udnævnelsesDato;
	private static Double tillæg = 50.0; //kr
	
	public Værkfører(String navn, String adresse, LocalDate svendePrøveDate, LocalDate udnævnelsesDato,
			double timeløn, double arbejdstimer) {
		super(navn, adresse, timeløn, arbejdstimer);
		this.udnævnelsesDato = udnævnelsesDato;
	}

	public LocalDate getUdnævnelsesDato() {
		return udnævnelsesDato;
	}

	public static Double getTillæg() {
		return tillæg;
	}
	
	@Override
	public double ugeLøn() {
		return getArbejdsTimer()*getTimeløn() + (getArbejdsTimer() * tillæg);
	}
	
	@Override
	public String toString() {
		return super.toString() + String.format(" %-5skr | %-10s |", tillæg, udnævnelsesDato);
	}
}