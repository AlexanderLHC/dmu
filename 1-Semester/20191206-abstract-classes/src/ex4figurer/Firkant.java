package ex4figurer;

public class Firkant extends Figur{

	private double side1;
	private double side2;


	public Firkant(int x, int y, double side) {
		this(x,y,side,side);
	}

	public Firkant(int x, int y, double side1, double side2) {
		super(x, y);
		this.side1 = side1;
		this.side2 = side2;
	}

	@Override
	public double getStørrelse() {
		return side1*side2;
	}

	@Override
	public String toString() {
		return String.format("%s : (%s,%s) areal = %s", this.getClass().getName(), getX(),getY(), getStørrelse());
	}

}
