package ex4figurer;

public class Cirkel extends Figur{
	private double radius;

	public Cirkel(int x, int y, double radius) {
		super(x, y);
		this.radius = radius;
	}

	@Override
	public double getStørrelse() {
		return Math.PI*radius*radius;
	}

	@Override
	public String toString() {
		return String.format("%s : (%s,%s) areal = %s", this.getClass().getName(), getX(),getY(), getStørrelse());
	}

}
