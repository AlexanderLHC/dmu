package controller;

import model.Konto;
import model.KontoType;
import storage.Storage;

public class Controller {

	/*
	 * 
	 * 2.
	 *  Controller : Luk Konto
	 *  Konto: Luk konto??
	 *  createTransaktion: check if lukket
	 */
	public static KontoType createKontoType(String navn, String beskrivelse) {

		KontoType kontoType = new KontoType(navn, beskrivelse);
		Storage.addKontoType(kontoType);
		return kontoType;
	}

	public static Konto createKonto(KontoType kontoType) {
		Konto konto = null;

		konto = new Konto(kontoType);

		return konto;
	}

	public static void lukKonto(Konto konto) {
		konto.lukKonto();
	}

	public static void aabnKonto(Konto konto) {
		konto.aabnKonto();
	}

	public static void haevEllerIndsaetBeloeb(Konto konto, int beloeb) {
		try {
			konto.haevEllerIndsaetBeloeb(beloeb);
		} catch (RuntimeException exception) {
			System.out.println(exception.getMessage());
		}
	}

}
