package model;

import controller.Controller;

public class TilstandeTest {

	public static void main(String[] args) {

		KontoType kontotype = Controller.createKontoType("Ungdomskonto",
				"En kontotype til unge under 25, som for at beskytte du unge ikke tillader at der haeves paa en konto med saldo under 0");

		Konto k1 = Controller.createKonto(kontotype);

		// Test 1
		System.out.println("Test #1: ny konto indsættes 100kr");
		Controller.haevEllerIndsaetBeloeb(k1, 100);
		System.out.println(k1);
		// Test 2
		System.out.println("\nTest #2: hæves 200kr");
		Controller.haevEllerIndsaetBeloeb(k1, -200);
		System.out.println(k1);
		// Test 3
		System.out.println("\nTest #3: hæves 100kr");
		Controller.haevEllerIndsaetBeloeb(k1, -100);
		System.out.println(k1);
		// Test 4
		System.out.println("\nTest #4: indsættes 532kr");
		Controller.haevEllerIndsaetBeloeb(k1, 532);
		System.out.println(k1);
		// Test 5
		System.out.println("\nTest #5: konto lukkes og forsøger hævet 100kr");
		Controller.lukKonto(k1);
		Controller.haevEllerIndsaetBeloeb(k1, -100);
		System.out.println(k1);
		// Test 6
		System.out.println("\nTest #6: konto genåbnes og forsøgt hævet 100kr");
		Controller.aabnKonto(k1);
		Controller.haevEllerIndsaetBeloeb(k1, -100);
		System.out.println(k1);

	}
}
