/**
 * 
 */
package ex2;

/**
 * @author alexander
 *
 */
public class Konto {
	private int nr;
	private int saldo;
	private String kontoType;

	public Konto(String kontoType, int nr) {
		this.kontoType = kontoType;
		this.nr = nr;
	}

	public void indsætBeløb(int beløb) {
		saldo += beløb;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setKontoType(String kontoType) {
		this.kontoType = kontoType;
	}

	public String getKontoType() {
		return kontoType;
	}

	public int getNr() {
		return nr;
	}

}
