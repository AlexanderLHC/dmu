/**
 * 
 */
package fodboldturnering;

/**
 * @author Christian
 *
 */
public class Person {

	private String navn;
	private int telefon;

	public Person(String navn, int telefon) {
		this.navn = navn;
		this.telefon = telefon;
	}

	/**
	 * @return the navn
	 */
	public String getNavn() {
		return navn;

	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public int getTelefon() {
		return telefon;
	}

	public void setTelefon(int telefon) {
		this.telefon = telefon;

	}
}
