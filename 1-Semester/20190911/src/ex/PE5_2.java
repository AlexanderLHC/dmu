/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class PE5_2 {

	/**
	 * @param args
	 */
	/*
	 * E5.2 Write a program that reads a floating-point number and prints “zero” if
	 * the number is zero. Otherwise, print “positive” or “negative”. Add “small” if
	 * the absolute value of the number is less than 1 , or “large” if it exceeds
	 * 1,000,000.
	 */
	public static void main(String[] args) {
		double d1 = -5.4;
		System.out.println("------\n1: " + d1);
		mathChecker(d1);
		double d2 = 1000000.4;
		System.out.println("------\n2: " + d2);
		mathChecker(4);
		double d3 = -0.12;
		System.out.println("------\n3: " + d3);
		mathChecker(d3);
		double d4 = 0;
		System.out.println("------\n4: " + d4);
		mathChecker(d4);
	}

	public static void mathChecker(double d) {
		if (d == 0) {
			System.out.println("Zero");
		} else {
			// tjek sigma
			if (d > 0) {
				System.out.println("Positive");

			} else {
				System.out.println("Negative");
			}
		}
		// tjek værdiens størrelse
		if (Math.abs(d) > 1000000) {
			System.out.println("large");
		} else if (Math.abs(d) < 1) {
			System.out.println("small");
		}
	}

}
