/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class PE5_16_CardTester {

	/**
	 * @param args
	 */
	/*
	 * E5.16Write a program that takes user input describing a playing card in the
	 * following shorthand notation:
	 * 
	 * A Ace 2 ... 10 Card values J Jack Q Queen K King D Diamonds H Hearts S Spades
	 * C Clubs Your program should print the full description of the card. For
	 * example,
	 * 
	 * Enter the card notation: QS Queen of Spades Implement a class Card whose
	 * constructor takes the card notation string and whose getDescription method
	 * returns a description of the card. If the notation string is not in the
	 * correct format, the getDescription method should return the string "Unknown".
	 * 
	 * 
	 */
	public static void main(String[] args) {
		PE5_16_Card card = new PE5_16_Card("10S");
		System.out.println(card.getDescription());
		card.setCardNotation("AS");
		System.out.println(card.getDescription());
		card.setCardNotation("QH");
		System.out.println(card.getDescription());
		card.setCardNotation("8D");
		System.out.println(card.getDescription());
		card.setCardNotation("11K");
		System.out.println(card.getDescription());

	}

}
