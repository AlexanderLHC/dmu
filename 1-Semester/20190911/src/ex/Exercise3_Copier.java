package ex;

public class Exercise3_Copier {
	private int paperInPrinter;
	private boolean paperStuck;

	public Exercise3_Copier() {
		this.paperInPrinter = 0;
		this.paperStuck = false;
	}

	public Exercise3_Copier(int paperInPrinter) {
		this.paperInPrinter = paperInPrinter;
	}

	public void insertPaper(int amount) {
		if (paperInPrinter + amount < 500) {
			paperInPrinter += amount;
		} else {
			System.out.println("FEJL: Maks 500 sider. Indsæt " + (paperInPrinter - amount) + " færre.");
		}
	}

	public int getPaperCount() {
		return paperInPrinter;
	}

	public void makeCopy(int printPagesCount) {
		if (!paperStuck) {
			if (paperInPrinter - printPagesCount >= 0) {
				paperInPrinter -= printPagesCount;
				System.out.println("Printer: " + printPagesCount + ". Resterende papir: " + paperInPrinter);
			} else {
				System.out.println("FEJL: Indsæt papir før print");
			}
		} else {
			System.out.println("FEJL: Papir sætter fast");
		}
	}

	public void makePaperJam() {
		paperStuck = true;
	}

	public void fixJam() {
		paperStuck = false;
	}
}
