/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class PE5_3 {

	/**
	 * @param args
	 */
	/*
	 * E5.3 Write a program that reads an integer and prints how many digits the
	 * number has, by checking whether the number is ≥ 10, ≥ 100, and so on. (Assume
	 * that all integers are less than ten billion.) If the number is negative,
	 * first multiply it with –1.
	 */
	public static void main(String[] args) {
		int i1 = 1000000000;
		System.out.println("------\n1: " + i1);
		mathChecker(i1);
		int i2 = -100;
		System.out.println("------\n2: " + i2);
		mathChecker(i2);
		int i3 = 50114;
		System.out.println("------\n3: " + i3);
		mathChecker(i3);
//		int i4 = 32;
//		System.out.println("------\n4: " + i4);
//		mathChecker(i4);
	}

	public static void mathChecker(int i) {
		int potens = 10;
		// to string
		if (i < 0) {
			i = i * -1;
		}
		while (i < Math.pow(10, potens)) {
			potens--;
		}
		System.out.println("Ciffre: " + (potens + 1));

	}

}
