/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class PP5_9_Year {

	/**
	 * @param args
	 */
	/*
	 * P5.9
	 * A year with 366 days is called a leap year. 
	 * Leap years are necessary to keep the calendar synchronized with the sun
	 * because the earth revolves around the sun once every 365.25 days.
	 * Actually, that figure is not entirely precise, 
	 * and for all dates after 1582 the Gregorian correction applies.
	 * Usually years that are divisible by 4 are leap years, for example 1996.
	 * However, years that are divisible by 100 (for example, 1900) are not leap years, 
	 * but years that are divisible by 400 are leap years (for example, 2000).
	 * Write a program that asks the user for a year and computes whether that year is a leap year.
	 * Provide a class Year with a method isLeapYear.
	 * Use a single if statement and Boolean operators.
	 */
	public static void main(String[] args) {
//		isLeapYear(2000);
//		isLeapYear(1000);
//		isLeapYear(1582);
//		isLeapYear(1337);
//		isLeapYear(1900);
		for (int i = 0; i < 3000; i++) {
			isLeapYear(i);
		}
	}

	public static void isLeapYear(int year) {
		if (year % 4 == 0 && (year % 100 != 0 || (year % 400 == 0 && year % 100 == 0))) {
			System.out.println(year + ": is a leap year");
		}
	}

}
