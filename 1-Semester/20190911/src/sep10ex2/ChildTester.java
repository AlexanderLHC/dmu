/**
 * 
 */
package sep10ex2;

/**
 * @author alexander
 *
 */
public class ChildTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Age | Gender | Institution");

		Child child1 = new Child(15, true);
		System.out.println(child1);

		Child child2 = new Child(15, false);
		System.out.println(child2);

		Child child3 = new Child(7, false);
		System.out.println(child3);

		Child child4 = new Child(7, true);
		System.out.println(child4);

	}

}
