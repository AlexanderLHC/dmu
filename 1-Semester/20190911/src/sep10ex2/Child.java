package sep10ex2;

public class Child {
	private int age;
	private boolean isMale;
	private String institution;

	public Child(int age, boolean isMale) {
		this.age = age;
		this.isMale = isMale;
		setInstitution();
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
		setInstitution();
	}

	public String getGender() {
		if (isMale) {
			return "Male";
		} else {
			return "Female";
		}
	}

	public void setGender(boolean isMale) {
		this.isMale = isMale;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution() {
		/*
		 * Institution 0 = Home 1-2 = Nursery 3-5 = Kindergarten 6-16 = School 17- = Out
		 * of school
		 */
		if (age >= 17) {
			institution = "Out of school";
		} else if (age >= 6) {
			institution = "School";
		} else if (age >= 3) {
			institution = "Kindergarten";
		} else if (age >= 1) {
			institution = "Nursery";
		} else if (age >= 0) {
			institution = "Home";
		}

	}

	public String team() {

		if (isMale) {
			if (age < 8) {
				return "Young Boys";
			} else {
				return "Cool boys";
			}
		} else {
			if (age < 8) {
				return "Tumbling girls";
			} else {
				return "Springy girls";
			}
		}
	}

	@Override
	public String toString() {
		return age + ":" + getGender() + ":" + institution + ":" + team();
	}

}
