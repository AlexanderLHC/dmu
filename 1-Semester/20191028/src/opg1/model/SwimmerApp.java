package opg1.model;

import java.util.ArrayList;

/*
 * Opgave 1 
	a. Importer filen swimmer.jar i Eclipse og implementer metoden bestLapTime i klassen 
		Swimmer. 
	b. Læs klassen SwimmerApp, så du forstår hvad der sker i denne klasse. Check at main()-
		metoden udskriver det forventede resultat, når du kører programmet. For hver svømmer skal 
		den hurtigste tid udskrives. 
	c. Læs koden for klassen TrainigPlan, så du har forstået hvad klassen gør. 
	d. Tilføj til main()-metoden i klassen SwimmerApp to træningsplaner
	 	1) en der har level A. På level A er svømmeren i vandet 16 timer og styrketræner 10 timer om ugen. 
		2) en level B. level B er svømmeren i vandet 10 timer om ugen og styrketræner 6 timer om ugen. 
	e. Du skal nu implementere associeringen mellem klasserne Swimmer og TrainingPlan givet ved
		nedenstående UML diagram. Hent inspiration fra noten afsnit 3.
 */

public class SwimmerApp {

	public static void main(String[] args) {

		ArrayList<Double> lapTimes = new ArrayList<>();
		lapTimes.add(1.02);
		lapTimes.add(1.01);
		lapTimes.add(0.99);
		lapTimes.add(0.98);
		lapTimes.add(1.02);
		lapTimes.add(1.04);
		lapTimes.add(0.99);
		Swimmer s1 = new Swimmer("Jan", 1994, lapTimes, "AGF");

		lapTimes = new ArrayList<>();
		lapTimes.add(1.05);
		lapTimes.add(1.01);
		lapTimes.add(1.04);
		lapTimes.add(1.06);
		lapTimes.add(1.08);
		lapTimes.add(1.04);
		lapTimes.add(1.02);
		Swimmer s2 = new Swimmer("Bo", 1995, lapTimes, "Lyseng");

		lapTimes = new ArrayList<>();
		lapTimes.add(1.03);
		lapTimes.add(1.01);
		lapTimes.add(1.02);
		lapTimes.add(1.05);
		lapTimes.add(1.03);
		lapTimes.add(1.06);
		lapTimes.add(1.03);
		Swimmer s3 = new Swimmer("Mikkel", 1993, lapTimes, "AIA-Tranbjerg");

		ArrayList<Swimmer> swimmers = new ArrayList<>();
		swimmers.add(s1);
		swimmers.add(s2);
		swimmers.add(s3);

		TrainingPlan planA = new TrainingPlan('A', 16, 10);
		TrainingPlan planB = new TrainingPlan('B', 10, 6);

		s1.setTrainingPlan(planA);
		s2.setTrainingPlan(planB);

		for (Swimmer s : swimmers) {
			System.out.println(s.getName() + " bedste tid: " + s.bestLapTime());
			if (s.getTrainingPlan() != null) {
				TrainingPlan plan = s.getTrainingPlan();
				System.out.printf("Træningsplan: %n* Niveau: %c.%n* Timer i vand: %d.%n* Timer i fitness: %d.%n",
						plan.getLevel(), plan.getWeeklyWaterHours(), plan.getWeeklyStrengthHours());
				System.out.printf("* Total træningstid: %d.%n", s.allTrainingHours());
			}
			System.out.printf("%n");
		}

	}

}
