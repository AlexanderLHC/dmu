package opg5;

import java.util.ArrayList;

public class Episode {
	private String title;
	private int number;
	private ArrayList<String> guestActors = new ArrayList<>();
	private int lengthMinutes;

	public Episode(String title, int number, int lengthMinutes, ArrayList<String> guestActors) {
		this.title = title;
		this.number = number;
		this.lengthMinutes = lengthMinutes;
		this.guestActors = guestActors;
	}

	public String getTitle() {
		return title;
	}

	public int getNumber() {
		return number;
	}

	public int getLength() {
		return lengthMinutes;
	}

	public ArrayList<String> getGuestActors() {
		return guestActors;
	}

}
