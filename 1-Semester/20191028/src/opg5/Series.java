package opg5;

import java.util.ArrayList;

public class Series {
	private String title;
	private ArrayList<String> cast;
	private ArrayList<Episode> episodes = new ArrayList<>();

	public Series(String title, ArrayList<String> cast) {
		this.title = title;
		this.cast = cast;
	}

	public String getTitle() {
		return title;
	}

	public ArrayList<String> getCast() {
		return cast;
	}

	public ArrayList<Episode> getEpisodes() {
		return episodes;
	}

	public Episode createEpisode(String title, int number, int lengthMinutes, ArrayList<String> guestActors) {
		Episode episode = new Episode(title, number, lengthMinutes, guestActors);
		episodes.add(episode);
		return episode;
	}

	public int totalLength() {
		int totalLength = 0;
		for (Episode e : episodes) {
			totalLength += e.getLength();
		}
		return totalLength;
	}

	public ArrayList<String> getGuestActors() {
		ArrayList<String> guestActors = new ArrayList<>();
		for (Episode e : episodes) {
			guestActors.addAll(e.getGuestActors());
		}
		return guestActors;
	}

}
