package opg5;

import java.util.ArrayList;

public class ShowTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 Opgave 5.1 
			Implementér klasserne Series og Episode uden at tage hensyn til kompositionen i diagrammet. 
		Opgave 5.2  
			Implementér kompositionen mellem Series og Episode. Du skal også tilføje en konstruktor til 
		Opgave 5.3 
			Programmér følgende metode tilhørende klassen Series: 
			/**  
			  * Return the total length (in minutes) of all the  
			  * episodes in the series. 
			  *| 
			public int totalLength()
		
		Opgave 5.4
			Programmér følgende metode tilhørende klassen Series: 
			/** 
			  * Return the total list of all guest actors from all 
			  *|
		 */
		ArrayList<String> cast = new ArrayList<>();
		cast.add("Donald Trump");
		cast.add("Mike Pompeo");
		cast.add("Bernie Sanders");
		Series series = new Series("Murica Today", cast);

		// Episode "Biggest parade ever"
		ArrayList<String> e01Cast = new ArrayList<>();
		e01Cast.add("Ivanka Trump");
		e01Cast.add("Melania Trump");
		e01Cast.add("Mike Pence");
		Episode e01 = series.createEpisode("Biggest Parade Ever", 1, 6, e01Cast);

		// Episode "Knock knock"
		ArrayList<String> e02Cast = new ArrayList<>();
		e02Cast.add("K9-Dog");
		e02Cast.add("Lindsey Graham");
		Episode e02 = series.createEpisode("Knock knock", 2, 60, e02Cast);

		// Episode "The Wall"
		ArrayList<String> e03Cast = new ArrayList<>();
		e03Cast.add("Enrique Peña Nieto");
		Episode e03 = series.createEpisode("The Wall", 3, 600, e03Cast);

		// Test Series class
		System.out.println("# Show: " + series.getTitle());
		System.out.println("Duration: " + series.totalLength() + " minutes.");
		System.out.println("Actors:");
		for (String actor : series.getCast()) {
			System.out.println("    * " + actor);
		}
		System.out.println("## Episodes:");
		System.out.println("------");
		for (Episode e : series.getEpisodes()) {
			System.out.println("Title: #" + e.getNumber() + " " + e.getTitle());
			System.out.println("Duration: " + e.getLength());
			if (e.getGuestActors().size() > 0) {
				System.out.println("Guest actors:");
				for (String actor : e.getGuestActors()) {
					System.out.println("  - " + actor);
				}
			}
			System.out.println("------");
		}

	}
}
