package ex4;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp_InvestCalc extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 4: Degree converter");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private final TextField txfValueLeft = new TextField(); // left side in gui
	private final TextField txfValueRight = new TextField(); // right side in gui
	private final TextField txfMessage = new TextField(); // for potential errors

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// Label
		Label lblValueLeft = new Label("Celsius:");
		pane.add(lblValueLeft, 0, 1);
		Label lblValueRight = new Label("Fahrenheit:");
		pane.add(lblValueRight, 1, 1);
		Label lblResult = new Label("Result:");
		pane.add(lblResult, 0, 3, 2, 1);

		// Fields
		pane.add(txfValueLeft, 0, 0);
		pane.add(txfValueRight, 1, 0);
		pane.add(txfMessage, 0, 4, 2, 1);

		// Buttons
		Button btnClear = new Button("Clear");
		pane.add(btnClear, 0, 2);
		btnClear.setOnAction(event -> this.clearValueFields());
		Button btnCalc = new Button("Calculate");
		pane.add(btnCalc, 1, 2);
		btnCalc.setOnAction(event -> this.calcConversion());

	}

	// -------------------------------------------------------------------------

	private void clearValueFields() {
		txfValueLeft.setText("");
		txfValueRight.setText("");
	}

	private void calcConversion() {
		if (txfValueLeft.getText().length() > 0 && txfValueRight.getText().length() > 0) {
			txfMessage.setText("Error: only one value can be entered at the time.");
		} else if (txfValueLeft.getText().length() > 0) {
			double celcius = Double.parseDouble(txfValueLeft.getText());
			double fahrenheit = 1.8 * celcius + 32;
			txfMessage.setText(fahrenheit + "°F");
		} else if (txfValueRight.getText().length() > 0) {
			double fahrenheit = Double.parseDouble(txfValueRight.getText());
			double celcius = (fahrenheit - 32) / 1.8;
			txfMessage.setText(celcius + "°C");
		}
	}

}
