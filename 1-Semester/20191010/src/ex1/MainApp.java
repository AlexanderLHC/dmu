package ex1;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Ex 1: Names to full");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------
	private final TextField txfFirstName = new TextField();
	private final TextField txfLastName = new TextField();
	private final TextField txfFullName = new TextField();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);

		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components 
		pane.setHgap(10);
		// set vertical gap between components 
		pane.setVgap(10);

		int colFirstName = 0;
		int colLastName = 2;
		// Labels
		Label lblFirstName = new Label("First Name:");
		pane.add(lblFirstName, colFirstName, 0);

		Label lblLastName = new Label("Last Name:");
		pane.add(lblLastName, colLastName, 0);

		Label lblFullName = new Label("Full Name:");
		pane.add(lblFullName, 0, 2);

		// Textfields
		pane.add(txfFirstName, colFirstName, 1, 1, 1);
		pane.add(txfLastName, colLastName, 1, 1, 1);
		pane.add(txfFullName, 0, 3, 3, 1);

		// Buttons
		Button btnFullName = new Button("Full name");
		pane.add(btnFullName, 1, 4);
		btnFullName.setOnAction(event -> this.setFullName());
		// add a text field to the pane (at col=1, row=0, extending 2 columns and 1 row)
		// add a button to the pane (at col=1, row=1)
		//		Button btnUpperCase = new Button("Upper Case");
		//		pane.add(btnUpperCase, 1, 1);
		//		GridPane.setMargin(btnUpperCase, new Insets(10, 10, 0, 10));
		// add a button to the pane (at col=2, row=1)
		//		Button btnLowerCase = new Button("Lower Case");
		//		pane.add(btnLowerCase, 2, 1);
		//		GridPane.setMargin(btnLowerCase, new Insets(10, 10, 0, 10));
	}

	private void setFullName() {
		txfFullName.setText(txfFirstName.getText().trim() + " " + txfLastName.getText().trim());
	}
}
