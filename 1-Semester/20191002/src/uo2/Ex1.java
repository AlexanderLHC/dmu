package uo2;

import java.util.Arrays;

public class Ex1 {

	/*
	 * 
	 * Create a class with a main() method. Add code to the main() method to fill 8 arrays with the 
		following values: 
		Use a for loop in c) - h) to fill the array. (To print an array, use Arrays.toString()).
					a.        0   0   0   0   0   0   0   0   0   0
					b.        2   44   -23   99   8   -5   7   10   20   30
					Requires for loops:
					c.        0   1   2   3   4   5   6   7   8   9
					d.        2   4   6   8  10  12  14  16  18 20
					e.        1   4   9  16  25  36  49  64  81 100
					f.        0   1   0   1   0   1   0   1   0   1
					g.        0   1   2   3   4   0   1   2   3   4
					h.        0   3   4   7   8   11   12   15   16   19
	 */
	public static void main(String[] args) {

		/*
		 * a: 0   0   0   0   0   0   0   0   0   0
		 */
		int[] a = new int[10];
		System.out.println("a) " + Arrays.toString(a));

		/*			
		 * b: 2   44   -23   99   8   -5   7   10   20   30
		 */
		int[] b = { 2, 44, -23, 99, 8, -5, 7, 10, 20, 30 };
		System.out.println("b) " + Arrays.toString(b));

		/*			
		 * c. 0   1   2   3   4   5   6   7   8   9
		 */
		int[] c = new int[10];
		for (int i = 0; i < c.length; i++) {
			c[i] = i;
		}
		System.out.println("c) " + Arrays.toString(c));

		/*
		 * d. 2   4   6   8  10  12  14  16  18 20
		 */
		int[] d = new int[10];
		for (int i = 0; i < d.length; i++) {
			d[i] = 2 + i * 2;
		}
		System.out.println("d) " + Arrays.toString(d));

		/*
		 * e.  1   4   9  16  25  36  49  64  81 100
		 */
		int[] e = new int[10];
		for (int i = 0; i < e.length; i++) {
			e[i] = (i + 1) * (i + 1);
		}
		System.out.println("e) " + Arrays.toString(e));

		/*
		 *	f. 0   1   0   1   0   1   0   1   0   1
		 */
		int[] f = new int[10];
		for (int i = 0; i < f.length; i++) {
			f[i] = i % 2;
		}
		System.out.println("f) " + Arrays.toString(f));

		/*
		 * g. 0   1   2   3   4   0   1   2   3   4
		 */
		int[] g = new int[10];
		for (int i = 0, value = 0; i < g.length; i++) {
			g[i] = value;
			if ((g.length / 2) - 1 > value) {
				value++;
			} else {
				value = 0;
			}
		}
		System.out.println("g) " + Arrays.toString(g));

		/*
		 * h. 0   3   4   7   8   11   12   15   16   19
		 */
		int[] h = new int[10];
		for (int i = 0; i < h.length; i++) {
			if (i % 2 != 0) {
				h[i] = h[i - 1] + 3;
			} else if (i != 0) {
				h[i] = h[i - 1] + 1;
			}
		}
		System.out.println("h) " + Arrays.toString(h));
	}

}
