package uo2;

import java.util.Arrays;

public class Ex2_3ArrayTest {

	/*
	 * Exercise 2 
		Create a class named ArrayTest with a main() method. 
		To the ArrayTest class, add a method (below the main() method) 
		public static int sum(int[] t) 
		that returns the sum of the numbers in the array.
		If t = {4,6,7,2,3}, the method should return 22.
		(The method is made static, so it can be called in the main() method.)
		Test your method in main(). 
		Implement a similar method using an array of doubles. 
	 */
	/*
	 * Exercise 3
		To the ArrayTest class, add a method
		public static int[] sumArrays(int[] a, int[] b) 
		that takes two arrays of the same length as parameters, and returns a new array with the same 
		length as a and b. The returned array must contain the sum of the values in a and b. 
		If a = {4,6,7,2,3} and b = {4,6,8,2,6}, the method should return {8,12,15,4,9}. 
		Make another method that works for arrays of unequal lengths (the result array must have the
		length of the longest array). Test your methods in main().
	 */
	/*
	 * Exercise 4 
		To the ArrayTest class, add a method
		public static boolean hasUneven(int[] t) 
		that returns true, if t includes at least one uneven number.
		If t = {4,6,7,2,3}, true must be returned; if t = {4,6,8,2,6}, false must be returned. 
		Test your method in main(). 
	 */
	public static void main(String[] args) {
		// Exercise 2: Test of sum method
		System.out.println("Exercise 2");
		int[] t = { 4, 6, 7, 2, 3 };
		System.out.println("Array: " + Arrays.toString(t) + ". Sum = " + sum(t));
		int[] r = { 3, 5, 8, 13, 21 };
		System.out.println("Array: " + Arrays.toString(r) + ". Sum = " + sum(r));
		// Exercise 3:
		System.out.println("Exercise 3");
		int[] a = { 4, 6, 7, 2, 3 };
		int[] b = { 4, 6, 8, 2, 6 };
		System.out.println("Sum of arrays: " + Arrays.toString(a) + ", " + Arrays.toString(b) + ". Sum = "
				+ Arrays.toString(sumArrays(a, b)));
		System.out.println("Sum of arrays: " + Arrays.toString(t) + ", " + Arrays.toString(r) + ". Sum = "
				+ Arrays.toString(sumArrays(t, r)));
		// Exercise 4:
		System.out.println("Exercise 4");
		System.out.println("Check uneven in: " + Arrays.toString(a) + "=" + hasUneven(a));
		System.out.println("Check uneven in: " + Arrays.toString(b) + "=" + hasUneven(b));

	}

	public static int sum(int[] t) {
		int sum = 0;
		for (int i = 0; i < t.length; i++) {
			sum += t[i];
		}
		return sum;
	}

	public static int[] sumArrays(int[] a, int[] b) {
		int[] sumArrays = new int[a.length];

		for (int i = 0; i < a.length; i++) {
			sumArrays[i] = a[i] + b[i];
		}
		return sumArrays;
	}

	public static boolean hasUneven(int[] a) {
		boolean hasUneven = false;
		for (int i = 0; i < a.length; i++) {
			if (a[i] % 2 != 0) {
				hasUneven = true;
				break;
			}
		}
		return hasUneven;
	}

}
