package ex5student;

public class Student {
	private String name;
	private int[] grades;// containing the student's grades
	private int gradeCount;// the current number of grades

	private static final int MAX_GRADE_COUNT = 10;// maximum number of grades

	public Student(String name) {
		this.name = name;
		this.grades = new int[Student.MAX_GRADE_COUNT];
		this.gradeCount = 0;
	}

	public String getName() {
		return this.name;
	}

	public int[] getGrades() {
		return this.grades;
	}

	public int getGradeCount() {
		return this.gradeCount;
	}

	public void addGrade(int grade) {
		if (gradeCount <= MAX_GRADE_COUNT) {
			grades[gradeCount] = grade;
			gradeCount++;
		}
	}

	public double gradeAverage() {
		int gradeSum = 0;
		for (int i = 0; i < gradeCount; i++) {
			gradeSum += grades[i];
		}
		return gradeSum * 1.0 / gradeCount;
	}

	public int maxGrade() {
		int maxGrade = 0;
		for (int i = 0; i < gradeCount; i++) {
			if (grades[i] > maxGrade) {
				maxGrade = grades[i];
			}
		}
		return maxGrade;
	}

	public int minGrade() {
		int minGrade = grades[0];
		for (int i = 0; i < gradeCount; i++) {
			if (grades[i] < minGrade) {
				minGrade = grades[i];
			}
		}
		return minGrade;
	}

	public int[] getActualGrades() {
		int[] actualGrades = new int[gradeCount];
		for (int i = 0; i < gradeCount; i++) {
			actualGrades[i] = grades[i];
		}
		return actualGrades;
	}
}
