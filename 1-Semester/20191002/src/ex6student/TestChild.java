package ex6student;

/*
 * Exercise 6
	Import the file array_child.jar into a project in Eclipse. 
	The Child class has a field weights that can contain the weights 
		of the child from age 1 to age 11. 
	As an example, it may contain: 
				{4.2, 9.3, 12.4, 17.5, 23.2, 25.3, 28.6, 30.4, 33.9, 35.1, 	37.3},
 	when the child is 11, but fewer weights, when the child is younger than 11. 
	
	- Add a method to the to Child class that returns the weight of the child at a specific age.
	- Add a method to the Child class that adds the weight for the child  
		(it is assumed, that weights are always added starting with the lowest age).
	- Add a method to the Child class that returns the maximum weight gain in a year:
		public double maxYearlyWeightGain()

	Test your methods in the TestChild class.
 */
public class TestChild {

	public static void main(String[] args) {
		Child b1 = new Child("Emma");
		Child b2 = new Child("Oliver");

		System.out.println(b1.getName());
		System.out.println(b2.getName());

		b1.addWeight(3.2);
		b1.addWeight(6.3);
		b1.addWeight(11.4);
		b1.addWeight(15.5);
		b1.addWeight(23.2);
		b1.addWeight(25.3);
		b1.addWeight(28.6);
		b1.addWeight(30.4);
		b1.addWeight(33.9);
		b1.addWeight(35.1);
		b1.addWeight(37.3);

		System.out.printf("At year 8 %s weighed: %.2f%n", b1.getName(), b1.getWeightAtAge(8));

		System.out.println("Biggest growth year: " + b1.maxYearlyWeightGain());
	}
}
