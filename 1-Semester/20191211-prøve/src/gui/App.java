package gui;

import java.util.ArrayList;

import app.controller.Controller;
import app.model.Bil;
import app.model.Bilmærke;
import app.model.Parkeringshus;
import app.model.Parkeringsplads;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class App extends Application {
	private ListView<Parkeringshus> lwParkeringshuse = new ListView<>();
	private ListView<String> lwOptagnePladser = new ListView<>();
	private ArrayList<Parkeringshus> parkeringshuse = new ArrayList<>();
	private ArrayList<String> optagnePladser = new ArrayList<String>();
	private TextField txfRegNr = new TextField();
	private ComboBox<Bilmærke> cbBilMærker = new ComboBox();
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	@Override
	public void start(Stage stage) {
		stage.setTitle("Gui Demo 1");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	private void initContent(GridPane pane) {
		Controller.createSomeObjects(); // create dummy data
		parkeringshuse = Controller.getParkeringshuse();

		// Styling
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);
		
		// Initialize Nodes		
		Label lblPhus = new Label("Parkeringshuse");
		Label lblOptagnePladser = new Label("Optagne pladser");
		Label lblOpretBil = new Label("Opret bil");
		Label lblRegNr = new Label("Reg nr:");
		Label lblBilMærke = new Label("Bilmærke:");
		Button btnOpret = new Button("Opret");
		/*
		 * Eftersom opgaven ikke formulerer dette som krav, er dette spildt.
		 * Opgaven giver 20 point og alt derudover giver ikke bonus.
		 * Hold dig til opgaven <3
		 */
		Bilmærke[] mærker = Bilmærke.values();
		ArrayList<Bilmærke> mærkerListe = new ArrayList<>();
		for (Bilmærke mærke : mærker) {
			mærkerListe.add(mærke);
		}
		cbBilMærker.getItems().addAll(mærkerListe);

		
		// onAction
		btnOpret.setOnAction(e -> this.opretBil());
		lwParkeringshuse.getItems().setAll(parkeringshuse);
		ChangeListener<Parkeringshus> husListener =
				(ov, oldString, newString) -> this.selectionPhusChanged();
		lwParkeringshuse.getSelectionModel().selectedItemProperty().addListener(husListener);		
		
		// Parkeringshuse
		pane.add(lblPhus, 0, 0);
		pane.add(lwParkeringshuse, 0, 1, 1, 2);
		
		// Optagne pladser
		pane.add(lblOptagnePladser, 1, 0);
		pane.add(lwOptagnePladser, 1, 1, 1, 2);
		
		// Opret bil
		pane.add(lblOpretBil, 0, 3);
		pane.add(lblRegNr, 0, 4);
		pane.add(txfRegNr, 1, 4);
		pane.add(lblBilMærke, 0, 5);
		pane.add(cbBilMærker, 1, 5);
		pane.add(btnOpret, 1, 6);
		
	}

	private void opretBil() {
		String regNr = txfRegNr.getText().trim();
		Bilmærke mærke = cbBilMærker.getSelectionModel().getSelectedItem();
		
		if (regNr != null && mærke != null) {
			Bil bil = Controller.createBil(regNr, mærke);
			Parkeringshus pHus = (Parkeringshus) lwParkeringshuse.getSelectionModel().getSelectedItem();
			int i = 0;
			boolean parked = false;
			while (i < pHus.getParkeringspladser().size() && parked == false) {
				Parkeringsplads plads = pHus.getParkeringspladser().get(i);
				if (plads.getBil() == null) {
					plads.setBil(bil);
					parked = true;
				}
				i++;
			}
			txfRegNr.setText("");
			this.selectionPhusChanged();
		}
	}

	private void selectionPhusChanged() {
		Parkeringshus selected = (Parkeringshus) lwParkeringshuse.getSelectionModel().getSelectedItem();
		if (selected != null) {
			lwOptagnePladser.getItems().setAll(selected.optagnePladser());
		} else {
			lwOptagnePladser.setItems(null);
		}
	}
	

}
