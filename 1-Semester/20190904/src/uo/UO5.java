package uo;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class UO5 extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		Pane root = this.initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Shapes");
		stage.setScene(scene);
		stage.show();
	}

	private Pane initContent() {
		Pane pane = new Pane();
		pane.setPrefSize(400, 400);
		this.drawShapes(pane);
		return pane;
	}

	// ------------------------------------------------------------------------

	private void drawShapes(Pane pane) {
		int row1 = 200;
		int row2 = 300;
		int strokeWidth = 8;
		double ringColumn = 100; // initial position

		Circle ringBlue = new Circle(ringColumn, row1, 100);
		Circle ringBlack = new Circle(ringColumn * 3, row1, 100);
		Circle ringRed = new Circle(ringColumn * 5, row1, 100);
		Circle ringYellow = new Circle(ringColumn * 2, row2, 100);
		Circle ringGreen = new Circle(ringColumn * 4, row2, 100);

		ringBlue.setFill(Color.TRANSPARENT);
		ringBlack.setFill(Color.TRANSPARENT);
		ringRed.setFill(Color.TRANSPARENT);
		ringYellow.setFill(Color.TRANSPARENT);
		ringGreen.setFill(Color.TRANSPARENT);

		ringBlue.setStroke(Color.BLUE);
		ringBlack.setStroke(Color.BLACK);
		ringRed.setStroke(Color.RED);
		ringYellow.setStroke(Color.YELLOW);
		ringGreen.setStroke(Color.GREEN);

		ringBlue.setStrokeWidth(strokeWidth);
		ringBlack.setStrokeWidth(strokeWidth);
		ringRed.setStrokeWidth(strokeWidth);
		ringYellow.setStrokeWidth(strokeWidth);
		ringGreen.setStrokeWidth(strokeWidth);

		// add shapes
		pane.getChildren().addAll(ringBlue, ringBlack, ringRed, ringYellow, ringGreen);
	}

}
