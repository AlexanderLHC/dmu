package storage;

import java.util.ArrayList;

import app.model.Arrangement;
import app.model.Tutor;
import app.model.Uddannelse;

public class Storage {
	private static final ArrayList<Uddannelse> uddannelser = new ArrayList<Uddannelse>();
	private static final ArrayList<Tutor> tutorer = new ArrayList<Tutor>();
	private static final ArrayList<Arrangement> arrangementer = new ArrayList<Arrangement>();

	// ---------------------- Uddannelse ---------------------
	public static void addUddannelse(Uddannelse u) {
		uddannelser.add(u);
	}

	public static ArrayList<Uddannelse> getUddannelser() {
		return new ArrayList<Uddannelse>(uddannelser);
	}

	// ---------------------- Tutorer ---------------------
	public static void addTutor(Tutor t) {
		tutorer.add(t);
	}

	public static ArrayList<Tutor> getTutorer() {
		return new ArrayList<Tutor>(tutorer);
	}

	// ---------------------- Arrangement ---------------------
	public static void addArrangement(Arrangement a) {
		arrangementer.add(a);
	}

	public static ArrayList<Arrangement> getArrangementer() {
		return new ArrayList<Arrangement>(arrangementer);
	}
}
