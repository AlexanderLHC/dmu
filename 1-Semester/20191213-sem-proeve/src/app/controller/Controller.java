package app.controller;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.InputMismatchException;

import javax.management.RuntimeErrorException;

import app.model.Arrangement;
import app.model.Hold;
import app.model.Tutor;
import app.model.Uddannelse;
import storage.Storage;

public class Controller {

	// ---------------------- Uddannelse ---------------------
	public static Uddannelse createUddannelse(String navn) {
		Uddannelse u = new Uddannelse(navn);
		Storage.addUddannelse(u);
		return u;
	}

	// ---------------------- Tutorer ---------------------
	public static Tutor createTutor(String navn, String email) {
		Tutor t = new Tutor(navn, email);
		Storage.addTutor(t);
		return t;
	}

	public static void tutorAddArrangement(Tutor t, Arrangement a) {
		boolean gyldigTidsrum = true;

		for (Arrangement tutorArrangement : t.getArrangementer()) {
			if (tutorArrangement.getDate().equals(a.getDate()) && tutorArrangement.getSlutTid().compareTo(a.getStartTid()) > 0) {
				gyldigTidsrum = false;
			}
		}
		if (gyldigTidsrum) {
			t.addArrangement(a);
		} else {
			throw new RuntimeException("Tutor allerede booket i tidsrummet");
//			throw new InputMismatchException("Tutor allerede booket i tidsrummet.");
		}
	}

	// ---------------------- Arrangement ---------------------
	public static Arrangement createArrangement(String title, LocalDate date, LocalTime startTid, LocalTime slutTid,
			double pris) {
		Arrangement a = new Arrangement(title, date, startTid, slutTid, pris);
		Storage.addArrangement(a);
		return a;
	}

	// ---------------------- Hold ---------------------
	public static ArrayList<Hold> holdUdenTutorer() {
		ArrayList<Hold> hold = new ArrayList<Hold>();
		for (Uddannelse u : Storage.getUddannelser()) {
			for (Hold h : u.getHoldListe()) {
				if (h.getTutorer().size() == 0) {
					hold.add(h);
				}
			}
		}
		return hold;
	}

	// ---------------------- Tester ---------------------
	public static void tutorOversigtTilFil(String filnavn) {
		try (PrintWriter fil = new PrintWriter(filnavn)) {
			for (Uddannelse u : Storage.getUddannelser()) {
				fil.println(u.getNavn());
				for (String tutor : u.tutorOversigt()) {
					fil.println(tutor);
				}
				fil.println();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void initStorage() {
		Uddannelse dmu = createUddannelse("DMU");
		Uddannelse finø = createUddannelse("FINØ");

		Hold e17s = dmu.createHold("1dmE17S", "Magrethe Dybdahl");
		Hold e17t = dmu.createHold("1dmE17T", "Kristian Dorland");
		Hold e17u = dmu.createHold("1dmE17U", "Kell Ørhøj");
		Hold e17b = finø.createHold("1fiE17B", "Karen Jensen");

		Tutor tAnders = createTutor("Anders Hansen", "aaa@students.eaaa.dk");
		Tutor tPeter = createTutor("Peter Jensen", "ppp@students.eaaa.dk");
		Tutor tNiels = createTutor("Niels Madsen", "nnn@students.eaaa.dk");
		Tutor tLone = createTutor("Lone Andersen", "lll@students.eaaa.dk");
		Tutor tMads = createTutor("Mads Miller", "mmm@students.eaaa.dk");

		Arrangement rusfest = createArrangement("Rusfest", LocalDate.of(2017, 8, 31), LocalTime.of(18, 00),
				LocalTime.of(23, 30), 250.0);
		Arrangement fodbold = createArrangement("Fodbold", LocalDate.of(2017, 8, 30), LocalTime.of(14, 00),
				LocalTime.of(17, 30), 100);
		Arrangement brætspil = createArrangement("Brætspil", LocalDate.of(2017, 8, 29), LocalTime.of(12, 00),
				LocalTime.of(16, 30), 25.0);
		Arrangement mindeparken = createArrangement("Mindeparken", LocalDate.of(2017, 8, 30), LocalTime.of(18, 00),
				LocalTime.of(22, 00), 25.0);

		tAnders.setHold(e17s);
		tLone.setHold(e17s);

		tPeter.setHold(e17u);
		tNiels.setHold(e17u);

		tMads.setHold(e17b);

		tutorAddArrangement(tAnders, rusfest);
		tutorAddArrangement(tPeter, rusfest);
		tutorAddArrangement(tMads, rusfest);

		tutorAddArrangement(tNiels, fodbold);

		tutorAddArrangement(tLone, brætspil);
		tutorAddArrangement(tNiels, brætspil);

		tutorAddArrangement(tMads, mindeparken);
		tutorAddArrangement(tAnders, mindeparken);

	}
}
