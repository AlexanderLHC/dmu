package app.model;

import java.util.ArrayList;

public class Tutor {
	private String navn;
	private String email;
	private Hold hold;
	private ArrayList<Arrangement> arrangementer = new ArrayList<Arrangement>();

	public Tutor(String navn, String email) {
		this.navn = navn;
		this.email = email;
	}

	public String getNavn() {
		return navn;
	}

	public String getEmail() {
		return email;
	}

	public void setHold(Hold hold) {
		if (!hold.equals(this.hold)) {
			this.hold = hold;
			hold.addTutor(this);
		}
	}

	public void addArrangement(Arrangement arrangement) {
		if (!arrangementer.contains(arrangement)) {
			arrangementer.add(arrangement);
		}
	}

	public void removeArrangement(Arrangement arrangement) {
		if (arrangementer.contains(arrangement)) {
			arrangementer.remove(arrangement);
		}
	}

	public double arrangementsPris() {
		double sum = 0;
		for (Arrangement a : arrangementer) {
			sum += a.getPris();
		}
		return sum;
	}

	public ArrayList<Arrangement> getArrangementer() {
		return arrangementer;
	}

	@Override
	public String toString() {
		return String.format("%-15s %-6s %-10s %s %s", navn, hold.getUddannelse().getNavn(), hold.getBetegnelse(),
				email, hold.getHoldleder());
	}

}
