import java.util.ArrayList;

import app.controller.Controller;
import app.model.Arrangement;
import app.model.Hold;
import app.model.Tutor;
import app.model.Uddannelse;
import storage.Storage;

public class Test {
	public static void main(String[] args) {
		Controller.initStorage();

		ArrayList<Uddannelse> uddannelser = Storage.getUddannelser();
		ArrayList<Arrangement> arrangementer = Storage.getArrangementer();
		ArrayList<Tutor> tutorer = Storage.getTutorer();

		System.out.println("Uddannelser: ");
		for (Uddannelse u : uddannelser) {
			System.out.println(u.toString());
		}

		System.out.println("Arrangementer: ");
		for (Arrangement a : arrangementer) {
			System.out.println(a.toString());
		}

		System.out.println("Tutorer: ");
		for (Tutor t : tutorer) {
			System.out.println(t.toString());
		}

		
		System.out.println("Hold uden tutor: " + Controller.holdUdenTutorer().toString());

		System.out.printf("Holdoversigt for %s: %n", uddannelser.get(0).getNavn());
		for (String s : uddannelser.get(0).tutorOversigt()) {
			System.out.println(s);
		}

		// Test overlappende tider
		Hold h1 = uddannelser.get(0).getHoldListe().get(0);
		Tutor t1 = h1.getTutorer().get(0);
		Arrangement a1 =  h1.getTutorer().get(0).getArrangementer().get(0);
		Arrangement a2 =  Controller.createArrangement("Test projekt", a1.getDate(), a1.getStartTid(), a1.getSlutTid(), 200.0);
		
		
		
//		System.out.printf("Overlappende arrangementer for %s: %n%s og %n%s %n",tutorer.get(0), a1, a2);
//		Controller.tutorAddArrangement(t1, a1);
//		Controller.tutorAddArrangement(t1, a2);
//		System.out.println("Arrangementer tilmeldt: " + t1.getArrangementer());

		h1.addTutor(t1);
		System.out.printf("%s har overlappende arrangement med %s = %s%n", h1.getBetegnelse(), a2.getTitle(), h1.harTidsoverlap(a2));

		
		Controller.tutorOversigtTilFil("testTutorList.txt");
	}

}
