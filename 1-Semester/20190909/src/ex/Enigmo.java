/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class Enigmo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String message = "Something is rotten in the state of Denmark";
		// TODO distort kludrer ved højere værdier
		int distort = 1;
		Encryptor enigma = new Encryptor(message, distort);
		String messageEncrypted = enigma.getMessageEncrypted();

		System.out.println("Initial message  : " + enigma.getMessage());
		System.out.println("Encrypted message: " + messageEncrypted);

		Decryptor submarine = new Decryptor(messageEncrypted, distort);
		System.out.println("Decrypted message: " + submarine.getMessage());

		// test setters
		System.out.println("\n......\nTesting setters:");
		String message2 = "Though this be madness, yet there is method in it";
		enigma.setMessage(message2);
		String message2Encrypted = enigma.getMessageEncrypted();
		System.out.println("Initial message  : " + enigma.getMessage());
		System.out.println("Encrypted message: " + message2Encrypted);

		submarine.setMessageEncrypted(message2Encrypted);
		System.out.println("Decrypted message: " + submarine.getMessage());

	}

}
