/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class Ex4_NameTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Ex4_Name user1 = new Ex4_Name("Zames", "Bond");
//		Ex4_Name user2 = new Ex4_Name("Lars", "Løkke", "Rasmussen");

		System.out.println(user1);

		System.out.println(user1.getInits());
		System.out.println(user1.getCryptoInits(8));
	}

}
