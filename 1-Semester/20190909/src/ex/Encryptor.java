/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class Encryptor {
	// TODO: name secretMessage vs message
	private String message; // only English letters allowed.
	private String messageEncrypted; // only English letters allowed.
	private int distort;
	// UNICODE position for capitals English letters
	private static int ALPHABET_END = 90;
	private static int ALPHABET_START = 65;

	public Encryptor(String message, int distort) {
		this.message = message;
		this.distort = distort;
		setMessageEncrypted();
	}

	public void setMessage(String message) {
		this.message = message;
		setMessageEncrypted();
	}

	public String getMessage() {
		return message;
	}

	public void setMessageEncrypted() {
		String messageUpperCase = message.toUpperCase();
		String messageDuringEncryption = "";
		int distortCount = 0;
		for (char ch : messageUpperCase.toCharArray()) {
			messageDuringEncryption += String.valueOf(getScrambledChar(ch, distortCount));
			distortCount++;
		}
		messageEncrypted = messageDuringEncryption;
	}

	public String getMessageEncrypted() {
		return messageEncrypted;
	}

	private char getScrambledChar(char ch, int distortCount) {
		int distortSum = distort + distortCount;
		char scrambledChar = (char) ((ch + distortSum - ALPHABET_END) % ALPHABET_START + ALPHABET_END);
//		System.out.println(ch + " " + (int) ch + " " + scrambledChar);
		return scrambledChar;
	}

}
