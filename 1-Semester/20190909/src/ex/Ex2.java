/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
/*
 * Declarations of local variables: int i1; char ch1, ch2; Write down the value
 * of the changed variable: ch2 = '3'; i1 = ch1; i1 = 66; ch2 = (char) i1;
 */
public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int i1;
		char ch1, ch2;

		ch1 = 'A';
		System.out.println(ch1);
		ch2 = '3';
		System.out.println(ch2);
		i1 = ch1;
		System.out.println(ch1);
		i1 = 66;
		System.out.println(i1);
		ch2 = (char) i1;
		System.out.println(ch2);
	}

}
