/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */

public class Ex4_Name {
	private String firstName, middleName, lastName, fullName;

	public Ex4_Name(String firstName, String lastName) {
		this.firstName = firstName;
		this.middleName = "";
		this.lastName = lastName;
	}

	public Ex4_Name(String firstName, String middleName, String lastName) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFullName() {
		return firstName + " " + middleName + " " + lastName;
	}

	public String getUsername() {
		return firstName.substring(0, 2).toUpperCase() + middleName.length()
				+ lastName.substring(lastName.length() - 2).toLowerCase();
	}

	public String getInits() {
		return firstName.substring(0, 1) + lastName.substring(0, 1);
	}

	public String getCryptoInits(int count) {
		return Character.toString((char) ((int) firstName.charAt(0) + count))
				+ Character.toString((char) ((int) lastName.charAt(0) + count));
	}

	@Override
	public String toString() {
		return firstName + " " + middleName + " " + lastName;
	}

}
