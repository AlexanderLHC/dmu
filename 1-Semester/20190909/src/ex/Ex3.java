/**
 * 
 */
package ex;

/**
 * @author alexander
 *
 */
public class Ex3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String ord1 = "Datamatiker";
		String ord2 = "Uddannelsen";

		// a) Færdiggør programmet så det udskriver ord1 med store bogstaver.
		System.out.println("a) Blokbogstaver: " + ord1.toUpperCase());

		// b) Færdiggør programmet så det udskriver ord2 med små bogstaver.
		System.out.println("b) Små bogstaver: " + ord2.toLowerCase());

		// c) Færdiggør programmet så det sammensætter ord1 og ord2
		// med et mellemrum imellem og udskriver resultatet.
		System.out.println("c) " + ord1 + " " + ord2);
		System.out.printf("c) %s %s %n", ord1, ord2);

		// d) Færdiggør programmet så det i en ny streng,
		// ord3, sammensætter ord1 og ord2,
		// hvor ord2 er med små bogstaver. Udskriv resultatet.
		String ord3 = ord1 + " " + ord2.toLowerCase();
		System.out.println("d) " + ord3);

		// e) Udskriv længden af strengen fra opgave d).
		System.out.println("e) " + ord3.length());

		// f) Udskriv de første 7 bogstaver af ord1.
		System.out.println("f) " + ord1.substring(0, 7));

		// g) Udskriv bogstav 3-7 fra ord2.
		System.out.println("g) " + ord2.substring(2, 7));

		// h) Udskriv den sidste halvdel af strengen fra opgave d).
		int stringLength = ord3.length();
		System.out.println("h) " + ord3.substring(stringLength / 2));

	}

}
