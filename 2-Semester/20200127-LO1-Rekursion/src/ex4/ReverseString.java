package ex4;

public class ReverseString {

	public static void main(String[] args) {
		String s1 = "RANSLIRPA";
		String s2 = "Leon Noel";

		System.out.printf("%s reversed : %s%n", s1, reverse(s1));
		System.out.printf("%s reversed : %s%n", s2, reverse(s2));
	}
	
	public static String reverse(String s) {
		int length = s.length();
		if (length == 0) 
			return "";
		else {
			return s.substring(length-1, length) + reverse(s.substring(0, length-1));
		}
	}
}
