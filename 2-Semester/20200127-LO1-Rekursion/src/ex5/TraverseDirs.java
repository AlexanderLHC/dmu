package ex5;

import java.io.File;

public class TraverseDirs {
	
	/*
	 * Exercise 5
		Make a method that recursively traverses through all directories contained in the folder given
		by the path parameter and prints the names of all directories found:

		public static void scanDir(String path) { ... }

		Hint: Create a File object from the path and use the list() and isDirectory() methods in the File
		class.
		Make a method that can also write the level of the directory before the directory name (level
		of the start directory is 1).
	 */

	public static void main(String[] args) {
		scanDir("/home/alexander/.config");
//		scanDir2("/home/alexander/.config", 0);
	}
	
	public static void scanDir(String path) {
		System.out.printf("   ");
		String[] rootContent = new File(path).list();

		for (String file : rootContent) {
			File f = new File(path +"/"+ file);
			if (f.isDirectory()) {
				scanDir(f.toString());
				System.out.println("└── " + f.toString());
			}
		}
	}
	
	public static void scanDir2(String path, int counter) {
		System.out.printf("   ");
		String[] rootContent = new File(path).list();

		for (String file : rootContent) {
			File f = new File(path +"/"+ file);
			if (f.isDirectory()) {
				System.out.println("└── " + counter + "=" + f.toString());
				scanDir2(f.toString(), counter + 1);
			}
		}
	}
	
	public static void scanDir3(String path, int level) {
		File f = new File(path);
		if (f.isDirectory()) {
			String absPath = f.getAbsolutePath();
			System.out.println("dir");
			for (String s : f.list()) {
				scanDir3(absPath, level + 1 );
			}
		}
	}

}
