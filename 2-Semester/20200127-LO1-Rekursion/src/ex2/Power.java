package ex2;

public class Power {
	private static int p1counter = 0;
	private static int p2counter = 0;
	/*
	 Exercise 2
		Write a recursive method public int power(int n, int p) that calculates n^p ,
		p>=0.
		The recursive definition is
		n^0 = 1
		n p = n^(p-1)*n, p>0
		Write another recursive method public int power2(int n, int p) that calculates
		n^p , p>=0 too, but this time with the definition:
		n^0 = 1
		n^p = n^(p-1)*n, p>0 and p is uneven
		n^p = (n*n)^(p div 2) , p>0 and p is even
	 */

	public static void main(String[] args) {
		//System.out.printf("%s^%s = %s%n", 2, 6, power(2, 6));
		System.out.printf("%s^%s = %s. Iterations %s%n", 2, 13, power(2, 13), p1counter);
		System.out.printf("%s^%s = %s. Iterations %s%n", 2, 13, power2(2, 13), p2counter);
	}
	
	public static int power(int n, int p) {
		p1counter++;
		return p != 0 ? n * power(n, p-1) : 1;
	}
	
	public static int power2(int n, int p) {
		p2counter++;
		if (p == 0) {
			return 1;
		} else if (p % 2 == 0) { // even
			return power2(n*n, p/2);
		} else {
			return n*power2(n, p-1);
		}
	}

}
