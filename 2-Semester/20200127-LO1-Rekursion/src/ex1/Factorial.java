package ex1;

public class Factorial {

	/*
	 * Exercise 1
		Write a recursive method public int factorial(int n) that calculates n!, n > 0.
		The recursive definition is:
		1! = 1
		n! = n*(n-1)!, n>0
	 */

	public static void main(String[] args) {
		int n1 = 1;
		int n2 = 2;
		int n3 = 3;

		System.out.printf("%s! = %s%n", n1, factorial(n1));
		System.out.printf("%s! = %s%n", n2, factorial(n2));
		System.out.printf("%s! = %s%n", n3, factorial(n3));
	}
	
	public static int factorial(int n) {
	    return n == 1 ? 1 : n * factorial( n-1 );
	}
	

}
