package ex7;

public class Domino {

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println("For f(" + i + ") = "+ posiblePositions(i));
		}

	}

	public static int posiblePositions(int width){
		if (width <= 2) {
			return width;
		} else {
			return posiblePositions(width-1)+posiblePositions(width-2);
		}
		
	}
}
