package ex6;

public class GreatestCommonDivisor {
	
	/*
	 Exercise 6
		Write a recursive method that implements Euclid's algorithm for finding the greatest common
		divisor of two integers (both >= 2). The greatest common divisor is the largest integer that
		divides both values (with no remainder).
		Greatest common divisor gcd(b,a) is defined by:

		gcd(a, b) = b				if b <= a and b divides a
		gcd(a, b) = gcd(b, a) 		if a < b
		gcd(a, b) = gcd(b, a % b)	otherwise
		
		
		|    a    |    b    |
		|   12    |    8    |
		|    8    |  12%8=4 |
		|    4    |  8%2=0  |
			    HALT
		

	 */

	public static void main(String[] args) {
		int a = 12;
		int b = 4;
		System.out.printf("gcd(%s,%s)=%s%n",a,b, gcd(a,b));
		a=12;b=8;
		System.out.printf("gcd(%s,%s)=%s%n",a,b, gcd(a,b));
		a=12000;b=800000000;
		System.out.printf("gcd(%s,%s)=%s%n",a,b, gcd(a,b));

	}
	
	public static int gcd(int a, int b) {
		return (b == 0) ? a : gcd(b,a % b);
	}

}
