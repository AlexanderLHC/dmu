package ex3;

public class Product {
	/*
	 * Exercise 3
		Write a recursive method public int prod(int a, int b) that calculates a*b (a
		and b are non-negative integer numbers >= 0). The method has to use the following recursive
		definition of multiplication (use of java’s * oprator is not allowed):
		a * b = (a-1) * b + b
		1 * b = b
		0 * b = 0
		Now suppose that your machine is capable of doubling and halving integer numbers.
		Write a recursive method public int prodRus(int a, int b) that calculates a*b
		using the following recursive definition of multiplication:
		a * b = (a div 2) * (2*b)
		if a is even and a >= 2
		a * b = (a-1) * b + b
		if a is odd and a >= 1
		0 * b = 0
		Which of the two methods is the faster one and why?
	 */

	public static void main(String[] args) {
		System.out.printf("%s*%s = %s", 2, 4, prod(2,4));
	}

	public static int prod(int a, int b) {
		return (a == 0) ? 0 :  b + prod(a-1, b);
	}
	
	private static int prodRus(int a, int b) {
		if (a == 0) 
			return 0;			
		else if (a % 2 != 0) {
			return prodRus(a-1, b) + b;
		} else {
			return prodRus((a / 2), (2*b));
		}
	}
	
}
