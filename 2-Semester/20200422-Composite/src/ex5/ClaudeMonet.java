package ex5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class ClaudeMonet {

    public static void main(String[] args) {
        EllipsesLeaf ellipse = new EllipsesLeaf("Ellipse", 10,10);
        RectangleLeaf rectangle = new RectangleLeaf("Rectangle",10,10);
        TriangleLeaf triangle = new TriangleLeaf("Triangle",10,10,10);
        ArrayList<FigureInterface> figures = new ArrayList<>(Arrays.asList(ellipse, rectangle, triangle));
        DrawingComposite drawing = new DrawingComposite("Drawing", new ArrayList<>(figures));
        figures.add(drawing);

        for (FigureInterface figure : figures) {
            System.out.println("---------");
            System.out.printf("%-10s %-15s%n", "Name", figure.getName());
            System.out.printf("%-10s ", "Drawn"); figure.draw();
            System.out.printf("%-10s %-15s%n", "Circumference", figure.calcCirc());
        }

        System.out.println("\n\n==========\nTesting iterator");
        Iterator<FigureInterface> figureIterator = drawing.createIterator();
        while (figureIterator.hasNext()){
            FigureInterface figure = figureIterator.next();
            System.out.println("- " + figure.getName());
        }
        System.out.println("And then adding a nest");

        DrawingComposite drawingNest = new DrawingComposite("Drawing Nest",  figures);
        Iterator<FigureInterface> figureNestIterator = drawingNest.createIterator();
        while (figureNestIterator.hasNext()){
            FigureInterface figure = figureNestIterator.next();
            System.out.println("- " + figure.getName());
        }
    }
}
