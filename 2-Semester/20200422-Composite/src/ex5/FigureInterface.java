package ex5;

public interface FigureInterface {
    String getName();
    void setName(String name);
    void draw();
    double calcCirc();
}
