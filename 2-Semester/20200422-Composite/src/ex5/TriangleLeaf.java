package ex5;

public class TriangleLeaf implements FigureInterface{
    private String name;
    private double a;
    private double b;
    private double c;

    public TriangleLeaf(String name, double a, double b, double c){
       this.name = name;
       this.a = a;
       this.b = b;
       this.c = c;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void draw() {
        System.out.printf("Triangle %s%n", name);
    }

    @Override
    public double calcCirc() {
        double s = (a+b+c)/2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }
}
