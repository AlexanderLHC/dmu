package ex5;

import java.util.ArrayList;
import java.util.Iterator;

public class DrawingComposite implements FigureInterface {
    private String name;
    private ArrayList<FigureInterface> figures = new ArrayList<>();

    public DrawingComposite(String name, ArrayList<FigureInterface> figures){
       this.name = name;
       this.figures = figures;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void draw() {
        System.out.println("Drawing " + name);
    }

    @Override
    public double calcCirc() {
        double circumference = 0.0;
        for (FigureInterface figure : figures) {
            circumference += figure.calcCirc();
        }
        return circumference;
    }

    public Iterator<FigureInterface> createIterator() {
        return new FigureIterator(figures.iterator());
    }
}
