package ex5;

import java.lang.reflect.Constructor;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class FigureIterator implements Iterator<FigureInterface> {
    private Deque<Iterator<FigureInterface>> stack = new ArrayDeque<>();


    public FigureIterator(Iterator<FigureInterface> iterator) {
        stack.push(iterator);
    }

    @Override
    public boolean hasNext() {
        if (stack.isEmpty())
            return false;
        else {
            Iterator<FigureInterface> iterator = stack.peek();
            if (!iterator.hasNext()) {
                stack.pop();
                return hasNext();
            } else
                return true;
        }
    }

    @Override
    public FigureInterface next() {
        if (!hasNext())
            return null;
        else {
            Iterator<FigureInterface> iterator = stack.peek();
            FigureInterface element = iterator.next();

            if (element instanceof DrawingComposite) {
                DrawingComposite drawingComposite = (DrawingComposite) element;
                stack.push(drawingComposite.createIterator());
            }
            return element;
        }
    }
}
