package ex5;

public class EllipsesLeaf implements FigureInterface {
    private String name;
    private double a;
    private double b;

    public EllipsesLeaf(String name, double a, double b){
        this.name = name;
        this.a = a;
        this.b = b;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void draw() {
        System.out.println("Ellipses " + name);
    }

    @Override
    public double calcCirc() {
        return Math.PI * a * b;
    }
}
