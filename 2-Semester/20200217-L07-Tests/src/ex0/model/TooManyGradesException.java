package ex0.model;

public class TooManyGradesException extends RuntimeException {

	public TooManyGradesException(String msg) {
		super(msg);
	}
}
