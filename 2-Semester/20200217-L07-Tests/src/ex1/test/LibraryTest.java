package ex1.test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

import ex1.Library;

public class LibraryTest {
	private LocalDate actualDate;
	private Library l;

	@Before
	public void setUp() throws Exception {
		actualDate = LocalDate.of(2020, 02, 17);
		l = new Library();
	}

	@Test
	public void testCalculateFineChild6Days() {
		LocalDate calcDate = ChronoUnit.DAYS.addTo(actualDate, 6);
		int fine = l.calculateFine(calcDate, actualDate, false);
		assertEquals(10, fine);
	}

	@Test
	public void testCalculateFineChild10Days() {
		LocalDate calcDate = ChronoUnit.DAYS.addTo(actualDate, 10);
		int fine = l.calculateFine(calcDate, actualDate, false);
		assertEquals(30, fine);
	}

	@Test
	public void testCalculateFineChild16Days() {
		LocalDate calcDate = ChronoUnit.DAYS.addTo(actualDate, 16);
		int fine = l.calculateFine(calcDate, actualDate, false);
		assertEquals(45, fine);
	}

	@Test
	public void testCalculateFineAdult6Days() {
		LocalDate calcDate = ChronoUnit.DAYS.addTo(actualDate, 6);
		int fine = l.calculateFine(calcDate, actualDate, true);
		assertEquals(20, fine);
	}

	@Test
	public void testCalculateFineAdult10Days() {
		LocalDate calcDate = ChronoUnit.DAYS.addTo(actualDate, 10);
		int fine = l.calculateFine(calcDate, actualDate, true);
		assertEquals(60, fine);
	}

	@Test
	public void testCalculateFineAdult16Days() {
		LocalDate calcDate = ChronoUnit.DAYS.addTo(actualDate, 16);
		int fine = l.calculateFine(calcDate, actualDate, true);
		assertEquals(90, fine);
	}
}
