package ex1;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Library {

	public static void main(String[] args) {

	}

	public int calculateFine(LocalDate calculatedDate, LocalDate actualDate, boolean adult) {
		int fine = 0;
		int days = (int) ChronoUnit.DAYS.between(actualDate, calculatedDate);

		if (days <= 7) {
			fine = 10;
		} else if (days <= 14) {
			fine = 30;
		} else {
			fine = 45;
		}

		return (adult) ? fine * 2 : fine;
	}
}
