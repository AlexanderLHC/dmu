package ex3;

public class CarInsurance {
	private double basisPremium;

	public CarInsurance(double basisPremium) {
		this.basisPremium = basisPremium;
	}

	/**
	 * Calculates and returns a premium on basis of the following rules:
	 * (basisPremium is the basis for the calculation) - if it is a person with age
	 * below 25, 25% is added to the premium, - if it is a woman the premium is
	 * reduced with 5%, - if the person have been driving without damages for: - 0
	 * to 2 years, the premium is reduced with 0%, - 3 to 5 years, the premium is
	 * reduced with 15%, - 6 to 8 years, the premium is reduced with 25%, - more
	 * than 8 years, the premium is reduced with 35%. (The above has to be
	 * calculated in the stated sequence.) Throws a RuntimeException, if age < 18,
	 * or yearsWithoutDamage < 0, or YearsWithoutDamage > age -18.
	 */
	public double calculatePremium(int age, boolean isWoman, int yearsWithoutDamage) {
		// TODO: throws runtimeexception
		double discount = 0;

		if (age < 25) {
			discount -= 0.25;
		}

		if (isWoman) {
			discount += 0.05;
		}

		if (yearsWithoutDamage <= 2) {
			;
		} else if (yearsWithoutDamage <= 5) {
			discount += 0.15;
		} else if (yearsWithoutDamage <= 8) {
			discount += 0.25;
		} else if (yearsWithoutDamage > 8) {
			discount += 0.35;
		}

		return basisPremium * (1 - discount);
	}

}
