package ex3;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CarInsuranceTest {
	private CarInsurance ci;

	@Before
	public void setUp() throws Exception {
		ci = new CarInsurance(1000.0);
	}

	/*
	 * 19 y/o
	 * Male
	 * 0 years w/o dmg
	 */
	@Test
	public void testAge19MaleYWOD0() {
		assertEquals(1250, ci.calculatePremium(19, false, 0), 0.001);
	}

	/*
	 * 26 y/o
	 * Male
	 * 3 years w/o dmg
	 */
	@Test
	public void testAge26MaleYWOD3() {
		assertEquals(850, ci.calculatePremium(26, false, 3), 0.001);
	}

	@Test
	public void testAge61MaleYWOD6() {
		assertEquals(750, ci.calculatePremium(61, false, 6), 0.001);
	}

	@Test
	public void testAge62MaleYWOD9() {
		assertEquals(650, ci.calculatePremium(62, false, 9), 0.001);
	}

	@Test
	public void testAge19FemaleYWOD2() {
		assertEquals(1200, ci.calculatePremium(19, true, 2), 0.001);
	}

	@Test
	public void testAge24FemaleYWOD5() {
		assertEquals(1050, ci.calculatePremium(24, true, 5), 0.001);
	}

	@Test
	public void testAge48FemaleYWOD7() {
		assertEquals(700, ci.calculatePremium(48, true, 7), 0.001);
	}

	@Test
	public void testAge54FemaleYWOD10() {
		assertEquals(600, ci.calculatePremium(54, true, 10), 0.001);
	}

	@Test
	public void testAge20FemaleYWOD2() {
		assertEquals(1200, ci.calculatePremium(20, true, 2), 0.001);
	}
}
