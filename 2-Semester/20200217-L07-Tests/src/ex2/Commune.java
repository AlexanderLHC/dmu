package ex2;

public class Commune {
	private final double DAYCARE = 2000;
	private final double SFA = 1000;

	/* 
	 * Returns the total payment for a family minus discount
	 * Pre: small >= 0, big >= 0
	 * (small is the number of pre-school children
	 * and big is the number of children attending school)
	 */
	public int totalPayment(int small, int big) {
		int payment = 0;
		int childCount = small + big;
		double discount = 1;

		for (int i = 1; i <= childCount; i++) {
			// get discount
			if (i > 1 && i <= 3) {
				discount = 0.75;
			} else if (i >= 4) {
				discount = 0.5;
			}

			if (small >= i) {
				payment += (DAYCARE * discount);
			} else {
				payment += (SFA * discount);
			}
		}

		return payment;
	}

	/*
	 * Returns the total payment for a family minus discount 
	 * Pre: small >= 0, big >=
	 * 0 (small is the number of pre-school children and big is the number of
	 * children attending school)
	 */
	public double totalPaymentRec(int small, int big) {
		if (small + big == 1)
			return (small == 1) ? DAYCARE : SFA;

		double discount = (small + big >= 4) ? 0.5 : 0.75;

		return ((big > 0) ? discount * SFA + totalPaymentRec(small, big - 1)
				: discount * DAYCARE + totalPaymentRec(small - 1, big));

	}
}
