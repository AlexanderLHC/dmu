package ex2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CommuneTest {
	private Commune c = new Commune();

	@Test
	public void testTotalPaymentChildSmall1() {
		assertEquals(2000, c.totalPayment(1, 0));
	}

	@Test
	public void testTotalPaymentChildSmall3() {
		assertEquals(5000, c.totalPayment(3, 0));
	}

	@Test
	public void testTotalPaymentChildSmall5() {
		assertEquals(7000, c.totalPayment(5, 0));
	}

	@Test
	public void testTotalPaymentChildBig1() {
		assertEquals(1000, c.totalPayment(0, 1));
	}

	@Test
	public void testTotalPaymentChildBig3() {
		assertEquals(2500, c.totalPayment(0, 3));
	}

	@Test
	public void testTotalPaymentChildBig5() {
		assertEquals(3500, c.totalPayment(0, 5));
	}

	@Test
	public void testTotalPaymentChildBig3Small3() {
		assertEquals(6500, c.totalPayment(3, 3));
	}

	@Test
	public void testTotalPaymentChildBig2Small2() {
		assertEquals(4750, c.totalPayment(2, 2));
	}

	// recursive
	@Test
	public void testTotalPaymentRecChildSmall1() {
		assertEquals(2000, c.totalPaymentRec(1, 0), 0.001);
	}

	@Test
	public void testTotalPaymentRecChildSmall3() {
		assertEquals(5000, c.totalPaymentRec(3, 0), 0.001);
	}

	@Test
	public void testTotalPaymentRecChildSmall5() {
		assertEquals(7000, c.totalPaymentRec(5, 0), 0.001);
	}

	@Test
	public void testTotalPaymentRecChildBig1() {
		assertEquals(1000, c.totalPaymentRec(0, 1), 0.001);
	}

	@Test
	public void testTotalPaymentRecChildBig3() {
		assertEquals(2500, c.totalPaymentRec(0, 3), 0.001);
	}

	@Test
	public void testTotalPaymentRecChildBig5() {
		assertEquals(3500, c.totalPaymentRec(0, 5), 0.001);
	}

	@Test
	public void testTotalPaymentRecChildBig3Small3() {
		assertEquals(6500, c.totalPaymentRec(3, 3), 0.001);
	}

	@Test
	public void testTotalPaymentRecChildBig2Small2() {
		assertEquals(4750, c.totalPaymentRec(2, 2), 0.001);
	}

}
