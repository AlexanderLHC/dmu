package app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class opg7 {

	public static void main(String[] args) {
		Connection c = App.getConnection("testdb");

		try (Scanner s = new Scanner(System.in);
				PreparedStatement ps = c.prepareStatement(
						"INSERT INTO TestTabel (letter, firstname, age, height, birthday, isMale) VALUES (?,?,?,?,?,1);")) {

			ps.clearBatch();

			System.out.println("Indtast et bogstav");
			ps.setString(1, s.next());

			System.out.println("Indtast et navn");
			ps.setString(2, s.next());

			System.out.println("Indtast en alder");
			ps.setInt(3, s.nextInt());

			System.out.println("Indtast en højde");
			ps.setDouble(4, s.nextDouble());

			System.out.println("Indtast et fødselstidspunkt (YYYY-MM-DD HH:MM:SS");
			ps.setString(5, s.next());

			System.out.println("Male (y/n)");
			s.next();
			ps.setByte(6, Byte.valueOf((s.next().toLowerCase().equals("y")) ? "1" : "0"));
			s.next();

			System.out.println("Satisfied with the input?");
			if (s.next().toLowerCase().equals("y")) {
				ps.executeQuery();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try (Statement stmt = c.createStatement();) {
			ResultSet res = stmt.executeQuery("SELECT * FROM TestTabel;");
			while (res.next()) {
				System.out.printf("%s %s %s %s %s %s%n", res.getString(1), res.getString(2), res.getInt(3),
						res.getDouble(4), res.getDate(5), res.getBoolean(6));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
