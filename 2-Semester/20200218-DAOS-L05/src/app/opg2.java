package app;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class opg2 {
	private static Connection c;
	private static Statement stmt;

	public static void main(String[] args) throws SQLException {
		c = App.getConnection("AndeByBank");
		stmt = c.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

		getAfdelinger();
	}

	private static void getAfdelinger() {

		try (ResultSet res = stmt.executeQuery("SELECT regNr, navn, adresse, tlfNr FROM Afdeling");) {
			while (res.next()) {
				System.out.printf("%5s %5s %5s %5s %n", res.getString("regNr"), res.getString(2), res.getString(3),
						res.getString(4));
			}
			//
			res.previous();
			System.out.printf("%5s %5s %5s %5s %n", res.getString(1), res.getString(2), res.getString(3),
					res.getString(4));

			res.absolute(3);
			System.out.printf("%5s %5s %5s %5s %n", res.getString(1), res.getString(2), res.getString(3),
					res.getString(4));
			res.relative(-1);
			System.out.printf("%5s %5s %5s %5s %n", res.getString(1), res.getString(2), res.getString(3),
					res.getString(4));

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
