package app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLOprOrdrePre {

	public static void main(String[] args) {
		try {
			System.out.println("Vi vil nu oprette en ny Ordre");
			BufferedReader inLine = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Indtast KundeId (Kunden skal være oprettet på forhånd");
			String kundeId = inLine.readLine();
			System.out.println("Indtast VareNr (Vare skal være oprettet på forhånd");
			String vareNummer = inLine.readLine();
			int vareNr = Integer.parseInt(vareNummer);
			System.out.println("Indtast Ordredato");
			String ordreDato = inLine.readLine();
			System.out.println("Indtast antal");
			String antalOrdre = inLine.readLine();
			int antal = Integer.parseInt(antalOrdre);
			Connection minConnection;
			minConnection = DriverManager
					.getConnection("jdbc:sqlserver://EAA-SH-SM-BO;databaseName=OrdreSystem;user=sa;password=Sm030456;");

			String sql = "insert into Ordre values (?,?,?,?)";
			PreparedStatement prestmt = minConnection.prepareStatement(sql);
			prestmt.clearParameters();
			prestmt.setString(1, kundeId);
			prestmt.setInt(2, vareNr);
			prestmt.setString(3, ordreDato);
			prestmt.setInt(4, antal);
			prestmt.execute();
			System.out.println("Ansættelsen er nu registreret");
			if (prestmt != null)
				prestmt.close();
			if (minConnection != null)
				minConnection.close();
			System.out.println("Ordren er nu registreret");
		} catch (SQLException e) {
			switch (e.getErrorCode()) {
			case 547: {
				if (e.getMessage().indexOf("Kunde") != -1)
					System.out.println("Kunde er ikke oprettet");
				if (e.getMessage().indexOf("Vare") != -1)
					System.out.println("Vare er ikke oprettet");
				break;
			}
			case 2627: {
				System.out.println("den pågældende ordre er allerede oprettet");
				break;
			}
			default:
				System.out.println("fejlSQL:  " + e.getMessage());
			}
			;
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}
}
