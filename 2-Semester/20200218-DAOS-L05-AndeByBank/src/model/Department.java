package model;

public class Department {
	private int regNo;
	private String name;
	private String address;
	private int postal;
	private String phone;

	public Department(int regNo, String name, String address, int postal, String phone) {
		this.regNo = regNo;
		this.name = name;
		this.address = address;
		this.postal = postal;
		this.phone = phone;
	}

	public int getRegNo() {
		return regNo;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public int getPostal() {
		return postal;
	}

	public String getPhone() {
		return phone;
	}

	@Override
	public String toString() {
		return name;
	}

}
