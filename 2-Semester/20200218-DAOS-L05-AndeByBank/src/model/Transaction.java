package model;

import java.time.LocalDate;

public class Transaction {
	private int regNo;
	private int accNo;
	private LocalDate date;
	private String text;
	private double amount;

	public Transaction(int regNo, int accNo, LocalDate date, String text, double amount) {
		this.regNo = regNo;
		this.accNo = accNo;
		this.date = date;
		this.text = text;
		this.amount = amount;
	}

	public int getRegNo() {
		return regNo;
	}

	public int getAccNo() {
		return accNo;
	}

	public LocalDate getDate() {
		return date;
	}

	public String getText() {
		return text;
	}

	public double getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return String.format("%s . %s %15s %30s %-10skr", accNo, regNo, date, text, amount);
	}

}
