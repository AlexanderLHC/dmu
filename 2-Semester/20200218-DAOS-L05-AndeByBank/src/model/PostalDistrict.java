package model;

public class PostalDistrict {
	private int postalCode;
	private String cityName;

	public PostalDistrict(int postalCode, String cityName) {
		this.postalCode = postalCode;
		this.cityName = cityName;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public String getCityName() {
		return cityName;
	}

	@Override
	public String toString() {
		return postalCode + "";
	}
}
