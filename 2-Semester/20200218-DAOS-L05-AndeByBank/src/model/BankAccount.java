package model;

public class BankAccount {
	private int regNo;
	private int accNo;
	private String text;
	private double balance;
	private float rateIn;
	private float rateOut;

	public BankAccount(int regNo, int accNo, String text, double balance, float rateIn, float rateOut) {
		this.regNo = regNo;
		this.accNo = accNo;
		this.text = text;
		this.balance = balance;
		this.rateIn = rateIn;
		this.rateOut = rateOut;
	}

	public int getRegNo() {
		return regNo;
	}

	public int getAccNo() {
		return accNo;
	}

	public String getText() {
		return text;
	}

	public double getBalance() {
		return balance;
	}

	public float getRateIn() {
		return rateIn;
	}

	public float getRateOut() {
		return rateOut;
	}

	@Override
	public String toString() {
		return String.format("%s %s %s %s %s %s", regNo, accNo, text, balance, rateIn, rateOut);
	}

}
