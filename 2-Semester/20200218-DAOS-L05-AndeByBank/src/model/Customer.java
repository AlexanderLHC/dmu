package model;

public class Customer {
	private int cprNr;
	private String name;
	private String adresse;
	private int postNr;

	public Customer(int cprNr, String name, String adresse, int postNr) {
		this.cprNr = cprNr;
		this.name = name;
		this.adresse = adresse;
		this.postNr = postNr;
	}

	public int getCprNr() {
		return cprNr;
	}

	public String getName() {
		return name;
	}

	public String getAdresse() {
		return adresse;
	}

	public int getPostNr() {
		return postNr;
	}

}
