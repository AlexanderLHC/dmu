package model;

public class Employee {
	private String cprNo;
	private int regNo;
	private String title;
	private String name;
	private String address;
	private int postNr;

	public Employee(String cprNo, int regNo, String title, String name, String address, int postNr) {
		this.cprNo = cprNo;
		this.regNo = regNo;
		this.title = title;
		this.name = name;
		this.address = address;
		this.postNr = postNr;
	}

	public String getCprNo() {
		return cprNo;
	}

	public int getRegNo() {
		return regNo;
	}

	public String getTitle() {
		return title;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public int getPostNr() {
		return postNr;
	}

	@Override
	public String toString() {
		return String.format("%10s %25s %25s", cprNo, title, name);
	}

}
