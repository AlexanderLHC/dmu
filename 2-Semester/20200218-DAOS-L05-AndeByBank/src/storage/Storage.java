package storage;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import model.BankAccount;
import model.Department;
import model.Employee;
import model.PostalDistrict;
import model.Transaction;

public class Storage {

	public static ArrayList<BankAccount> getBankAccounts() {
		ArrayList<BankAccount> bankaccounts = new ArrayList<>();

		Connection c = getConnection();

		try (PreparedStatement ps = c
				.prepareStatement("SELECT K.* FROM Konto K INNER JOIN KundeHarKonto KK ON K.ktoNr = KK.ktoNr;")) {

			ResultSet res = ps.executeQuery();
			while (res.next()) {
				bankaccounts.add(new BankAccount(res.getInt(1), res.getInt(2), res.getString(3), res.getDouble(4),
						res.getFloat(5), res.getFloat(6)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return bankaccounts;
	}

	public static ArrayList<BankAccount> getBankAccounts(String cprNr) {
		ArrayList<BankAccount> bankaccounts = new ArrayList<>();

		Connection c = getConnection();

		try (PreparedStatement ps = c.prepareStatement(
				"SELECT K.* FROM Konto K INNER JOIN KundeHarKonto KK ON K.ktoNr = KK.ktoNr WHERE KK.cprNr = ? ;")) {

			ps.clearParameters();
			ps.setString(1, cprNr);

			ResultSet res = ps.executeQuery();
			while (res.next()) {
				bankaccounts.add(new BankAccount(res.getInt(1), res.getInt(2), res.getString(3), res.getDouble(4),
						res.getFloat(5), res.getFloat(6)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return bankaccounts;
	}

	public static ArrayList<Department> getDepartments() {
		ArrayList<Department> departments = new ArrayList<>();

		Connection c = getConnection();

		try (PreparedStatement ps = c.prepareStatement("SELECT * FROM Afdeling;")) {

			ResultSet res = ps.executeQuery();
			while (res.next()) {
				departments.add(new Department(res.getInt(1), res.getString(2), res.getString(3), res.getInt(4),
						res.getString(5)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return departments;
	}

	public static ArrayList<PostalDistrict> getPostalDistricts() {
		ArrayList<PostalDistrict> postalDistricts = new ArrayList<>();

		Connection c = getConnection();

		try (PreparedStatement ps = c.prepareStatement("SELECT * FROM PostDistrikt;")) {

			ResultSet res = ps.executeQuery();
			while (res.next()) {
				postalDistricts.add(new PostalDistrict(res.getInt(1), res.getString(2)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return postalDistricts;
	}

	public static ArrayList<Employee> getEmployees(Department department) {
		ArrayList<Employee> employees = new ArrayList<>();

		Connection c = getConnection();

		try (PreparedStatement ps = c.prepareStatement("SELECT * FROM Medarbejder WHERE afdeling=?;")) {
			ps.clearBatch();
			ps.setInt(1, department.getRegNo());

			ResultSet res = ps.executeQuery();
			while (res.next()) {
				employees.add(new Employee(res.getString(1), res.getInt(2), res.getString(3), res.getString(4),
						res.getString(5), res.getInt(6)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return employees;
	}

	public static Employee addEmployee(Employee employee) {
		Connection c = getConnection();

		try (PreparedStatement ps = c.prepareStatement(
				"INSERT INTO Medarbejder (cprNr, afdeling, titel, navn, adresse, postNr) VALUES (?,?,?,?,?,?);")) {
			ps.clearBatch();
			ps.setString(1, employee.getCprNo());
			ps.setInt(2, employee.getRegNo());
			ps.setString(3, employee.getTitle());
			ps.setString(4, employee.getName());
			ps.setString(5, employee.getAddress());
			ps.setInt(6, employee.getPostNr());

			ps.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employee;
	}

	public static Queue<Transaction> getTransactions(int accNo) {
		Queue<Transaction> transactions = new LinkedList<>();
		Connection c = getConnection();

		try (PreparedStatement ps = c.prepareStatement("SELECT * FROM Transaktion WHERE ktoNr = ?;")) {
			ps.clearBatch();
			ps.setString(1, accNo + "");
			ResultSet res = ps.executeQuery();

			while (res.next()) {
				transactions.add(new Transaction(res.getInt(1), Integer.parseInt(res.getString(2)),
						LocalDate.parse(res.getString(3)), res.getString(4), res.getDouble(5)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return transactions;
	}

	// --------------------- Database authentication
	private static String getUser() throws Exception {
		File file = new File("auth.xml");
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.parse(file);
		return document.getElementsByTagName("user").item(0).getTextContent();
	}

	private static String getPass() throws Exception {
		File file = new File("auth.xml");
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.parse(file);
		return document.getElementsByTagName("password").item(0).getTextContent();
	}

	private static String getAddr() throws Exception {
		File file = new File("auth.xml");
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.parse(file);
		return document.getElementsByTagName("address").item(0).getTextContent();
	}

	private static Connection getConnection() {
		Connection c = null;
		try {
			c = DriverManager.getConnection("jdbc:sqlserver://" + getAddr() + ";databaseName=AndeByBank;user="
					+ getUser() + ";password=" + getPass() + ";");
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
		return c;
	}
}
