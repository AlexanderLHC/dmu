package gui;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.PostalDistrict;
import storage.Storage;

public class AddEmployeeWindow extends Stage {
	private TextField txfCpr = new TextField();
	private TextField txfTitle = new TextField();
	private TextField txfName = new TextField();
	private TextField txfAddress = new TextField();

	private ComboBox<PostalDistrict> cbPostalDistrict = new ComboBox<PostalDistrict>();

	public AddEmployeeWindow(Stage owner) {
		this.initOwner(owner);
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinWidth(200);
		this.setMinHeight(500);

		this.setTitle("Tilføj medarbejder");

		GridPane gp = new GridPane();
		gp.add(new Label("CPR"), 0, 0);
		gp.add(txfCpr, 1, 0);
		gp.add(new Label("Titel"), 0, 1);
		gp.add(txfTitle, 1, 1);
		gp.add(new Label("Navn"), 0, 2);
		gp.add(txfName, 1, 2);
		gp.add(new Label("Adresse"), 0, 3);
		gp.add(txfAddress, 1, 3);
		gp.add(new Label("Postnummer"), 0, 4);

		for (PostalDistrict pd : Storage.getPostalDistricts()) {
			cbPostalDistrict.getItems().add(pd);
		}
		cbPostalDistrict.getSelectionModel().select(0);
		gp.add(cbPostalDistrict, 1, 4);

		Button btnOk = new Button("OK");
		btnOk.setOnAction(e -> this.okAction());
		Button btnCancel = new Button("FORTRYD");
		btnCancel.setOnAction(e -> this.cancelAction());

		gp.add(btnOk, 0, 5);
		gp.add(btnCancel, 1, 5);

		Scene scene = new Scene(gp);
		this.setScene(scene);
	}

	private void okAction() {
		this.hide();
	}

	private void cancelAction() {
		this.hide();
	}

	public String getCpr() {
		return txfCpr.getText();
	}

	public String getTitleInput() {
		return txfTitle.getText();
	}

	public String getName() {
		return txfName.getText();
	}

	public String getAddress() {
		return txfAddress.getText();
	}

	public PostalDistrict getPostalDistrict() {
		return cbPostalDistrict.getSelectionModel().getSelectedItem();
	}
}
