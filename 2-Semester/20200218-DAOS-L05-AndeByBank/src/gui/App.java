package gui;

import java.util.Queue;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.BankAccount;
import model.Department;
import model.Employee;
import model.Transaction;
import storage.Storage;

public class App extends Application {
	private TableView<BankAccount> tvAccounts = new TableView<BankAccount>();
	private final ObservableList<BankAccount> bankAccounts = FXCollections.observableArrayList();
	private final ObservableList<Employee> employees = FXCollections.observableArrayList();

	private TextField txfAccCprNo = new TextField();
	private ComboBox<Department> cbDepartments = new ComboBox<Department>();
	private ListView<Employee> lwEmployees = new ListView<>(employees);

	private ListView<Transaction> lwTransactions = new ListView<>();

	private Stage primaryStage;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		TabPane tp = new TabPane();

		// kunder
		Tab customers = new Tab();
		initCustomerTab(customers);
		tp.getTabs().add(customers);
		// afdeling
		Tab afdeling = new Tab();
		initDepartmentTab(afdeling);
		tp.getTabs().add(afdeling);

		Scene scene = new Scene(tp);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	// Bank konti

	private void initCustomerTab(Tab t) {
		GridPane gp = new GridPane();

		gp.add(new Label("CPR number"), 0, 0);
		gp.add(txfAccCprNo, 1, 0);
		txfAccCprNo.setText("1209631223");
		Button btnGetBankAccounts = new Button("Get accounts");
		btnGetBankAccounts.setOnAction(e -> this.setBankAccounts());
		gp.add(btnGetBankAccounts, 3, 0);

		TableColumn<BankAccount, Integer> tcReg = new TableColumn<BankAccount, Integer>("regNo");
		tcReg.setCellValueFactory(new PropertyValueFactory<BankAccount, Integer>("regNo"));
		TableColumn<BankAccount, Integer> tcAcc = new TableColumn<BankAccount, Integer>("accNo");
		tcAcc.setCellValueFactory(new PropertyValueFactory<BankAccount, Integer>("accNo"));
		TableColumn<BankAccount, String> tcText = new TableColumn<BankAccount, String>("Text");
		tcText.setCellValueFactory(new PropertyValueFactory<BankAccount, String>("text"));

		TableColumn<BankAccount, Double> tcBalance = new TableColumn<BankAccount, Double>("Balance");
		tcBalance.setCellValueFactory(new PropertyValueFactory<BankAccount, Double>("balance"));

		TableColumn<BankAccount, Float> tcRateDeposit = new TableColumn<BankAccount, Float>("rate deposit");
		tcRateDeposit.setCellValueFactory(new PropertyValueFactory<BankAccount, Float>("rateIn"));

		TableColumn<BankAccount, Float> tcRateLoan = new TableColumn<BankAccount, Float>("rate loan");
		tcRateLoan.setCellValueFactory(new PropertyValueFactory<BankAccount, Float>("rateOut"));

		tvAccounts.getColumns().addAll(tcReg, tcAcc, tcText, tcBalance, tcRateDeposit, tcRateLoan);

		gp.add(tvAccounts, 0, 1, 4, 3);

		Button btnGetTransaction = new Button("Få transaktioner");
		btnGetTransaction.setOnAction(e -> setTransactions());
		gp.add(btnGetTransaction, 0, 4);

		gp.add(lwTransactions, 0, 5, 2, 4);

		t.setText("Kunder");
		t.setContent(gp);
		t.setClosable(false);
	}

	private void setBankAccounts() {
		bankAccounts.setAll(Storage.getBankAccounts(txfAccCprNo.getText()));
		tvAccounts.setItems(bankAccounts);
	}

	// Afdeling
	private void initDepartmentTab(Tab t) {
		GridPane gp = new GridPane();
		cbDepartments.setOnAction(e -> this.setEmployees());
		for (Department d : Storage.getDepartments()) {
			cbDepartments.getItems().add(d);
		}
		cbDepartments.getSelectionModel().select(0);

		gp.add(new Label("Afdelinger: "), 0, 0);
		gp.add(cbDepartments, 1, 0, 1, 2);
		gp.add(lwEmployees, 0, 2, 2, 1);

		Button btnAddEmployee = new Button("Tilføj medarbejder");
		btnAddEmployee.setOnAction(e -> this.addEmployee());
		gp.add(btnAddEmployee, 0, 3);

		t.setText("Afdelinger");
		t.setContent(gp);
		t.setClosable(false);
	}

	private void addEmployee() {
		AddEmployeeWindow aew = new AddEmployeeWindow(primaryStage);
		aew.showAndWait();
		// close your eyes
		if (aew.getCpr() != null) {
			Employee e = Storage.addEmployee(
					new Employee(aew.getCpr(), cbDepartments.getSelectionModel().getSelectedItem().getRegNo(),
							aew.getTitle(), aew.getName(), aew.getAddress(), aew.getPostalDistrict().getPostalCode()));
		}
		setEmployees();
	}

	private void setEmployees() {
		Department department = cbDepartments.getSelectionModel().getSelectedItem();
		employees.setAll(Storage.getEmployees(department));
	}

	private void setTransactions() {
		int accNo = tvAccounts.getSelectionModel().getSelectedItem().getAccNo();
		Queue<Transaction> transactions = Storage.getTransactions(accNo);
		lwTransactions.getItems().setAll(transactions);

	}
}
