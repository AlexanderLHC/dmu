package ex4Fakultet;

public class FakultetSumBeregner implements Runnable {
    private int n;

    public FakultetSumBeregner(int n){
        this.n = n;
    }

    public int fakultet(int n) {
        if (n == 0 || n == 1)
            return 1;
        else {
            int fakultet = fakultet(n-1) * n;
            System.out.println("Rekursion: " + fakultet);
            return fakultet;
        }

    }

    @Override
    public void run() {
        System.out.println("Resultat : " + fakultet(n));
    }
}
