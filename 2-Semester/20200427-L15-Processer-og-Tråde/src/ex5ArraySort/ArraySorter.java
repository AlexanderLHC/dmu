package ex5ArraySort;

public class ArraySorter implements Runnable {
    private int[] arrayUnsorted;

    public ArraySorter(int[] arrayUnsorted) {
        this.arrayUnsorted = arrayUnsorted;
    }

    /**
     * SelectionSort
     *  TODO: returnerer et nyt array, burde vel modificere arrayet
     */
    private int[] sortArray(int[] arrayUnsorted) {
        int[] arraySorted = arrayUnsorted;
        int min;
        for (int index = 0; index < arraySorted.length-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < arraySorted.length; scan++)
                if (arraySorted[scan] < (arraySorted[min]) )
                    min = scan;
            swap (arraySorted, min, index);
        }

        return arraySorted;
    }

    private void swap(int[] array, int i1, int i2) {
        int tmp = array[i1];
        array[i1] = array[i2];
        array[i2] = tmp;
    }

    @Override
    public void run() {
        sortArray(arrayUnsorted);
    }
}
