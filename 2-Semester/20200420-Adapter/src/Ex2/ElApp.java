package Ex2;

public class ElApp extends Item {
    private final double VAT_PERCENT = 0.05;
    private final int VAT_MINIMUM = 3; // DKK

    public ElApp(String name, double netprice) {
        super(name, netprice);
    }

    @Override
    double calcVAT() {
        double VAT = VAT_PERCENT * getNetprice();
        return (VAT > VAT_MINIMUM) ? VAT : VAT_MINIMUM;
    }
}
