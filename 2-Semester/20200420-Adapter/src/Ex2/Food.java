package Ex2;

public class Food extends Item {
    private final double VAT_PERCENT = 0.05;

    public Food(String name, double netprice) {
        super(name, netprice);
    }

    @Override
    double calcVAT() {
        return getNetprice() * VAT_PERCENT;
    }
}
