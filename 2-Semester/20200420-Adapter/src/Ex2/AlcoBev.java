package Ex2;

public class AlcoBev {
    private double theNetPrice;
    private String theDescription;

    public AlcoBev(String theDescription, double theNetPrice) {
        this.theDescription = theDescription;
        this.theNetPrice = theNetPrice;
    }

    public double getTheNetPrice() {
        return theNetPrice;
    }

    public void setTheNetPrice(double theNetPrice) {
        this.theNetPrice = theNetPrice;
    }

    public String getTheDescription() {
        return theDescription;
    }

    public void setTheDescription(String theDescription) {
        this.theDescription = theDescription;
    }

   public double getVat(){
        return (theNetPrice > 150) ? 1.20 * theNetPrice : 0.8 * theNetPrice;
   }
}
