package Ex2;

public abstract class Item {
    private double netprice;
    private String name;

    public Item(String name, double netprice) {
        this.name = name;
        this.netprice = netprice;
    }

    public double getNetprice() {
        return netprice;
    }

    public void setNetprice(double netprice) {
        this.netprice = netprice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    abstract double calcVAT();
}
