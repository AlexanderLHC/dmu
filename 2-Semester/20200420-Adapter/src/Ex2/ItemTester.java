package Ex2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemTester {
    public static void main(String[] args) {
        Food bread = new Food("Bread",8.95);
        ElApp monitor = new ElApp("Monitor 2000", 1500);
        ElApp battery = new ElApp("Battery", 10);
        AlcoBevAdapter wine = new AlcoBevAdapter(new AlcoBev("Wine", 100));
        List<Item> items = new ArrayList<Item>(Arrays.asList(bread, monitor, battery, wine));

        System.out.printf("%-15s %10s %10s%n", "Vare" , "Netprice" , "VAT price");
        for (Item item : items){
            System.out.printf("%-15s %10s %10s%n", item.getName() , item.getNetprice() , item.calcVAT());
        }

    }
}
