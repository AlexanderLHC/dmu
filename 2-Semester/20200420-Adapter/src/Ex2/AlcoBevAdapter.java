package Ex2;

public class AlcoBevAdapter extends Item {
    private AlcoBev alcoBev;

    public AlcoBevAdapter(AlcoBev alcoBev){
        super(alcoBev.getTheDescription(), alcoBev.getTheNetPrice());
        this.alcoBev = alcoBev;
    }

    @Override
    double calcVAT() {
        return alcoBev.getVat();
    }
}
