package ex1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class UseMethodsWithHashSet {

	public static void main(String[] args) {
		Set<Integer> set1 = new HashSet<Integer>(Arrays.asList(34, 12, 23, 45, 67, 34, 98));

		// b)12
		System.out.println(set1);

		// d)
		set1.add(23);
		// e)
		System.out.println(set1);
		// f)
		set1.remove(67);
//		g)
		System.out.println(set1);
//		h)
		System.out.printf("Does set contain %s: %s%n", 23, set1.contains(23));
//		i)
		System.out.println("List length: " + set1.size());
	}

}
