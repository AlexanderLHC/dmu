package ex4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

public class FileWordCount {
	public static void main(String[] args) {
		System.out.println("======= OPGAVE A =========\n\n");
		findAlleOrdIFileTreeSet("Gjoengehoevdingen.txt");
		System.out.println("\n======= OPGAVE B =========\n\n");
		findAlleOrdIFileTreeMap("Gjoengehoevdingen.txt");

	}

	/**
	 * a) Write a program that • Reads the words in a text file and inserts the
	 * words in a TreeSet. (There is a big text file on Canvas.) • Prints all words
	 * in the set, the total number of words, and the number of different words.
	 * 
	 * @param path
	 */
	public static void findAlleOrdIFileTreeSet(String path) {

		TreeSet<String> ordListe = new TreeSet<>();
		int antalOrd = 0;

		File f = new File(path);

		try (Scanner s = new Scanner(f)) {

			while (s.hasNext()) {
				String key = s.next();
				key = key.replaceAll("[^a-zA-ZæøåÆØÅ\\d]", "").toLowerCase();

				antalOrd++;
				ordListe.add(key);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("printer alle ord");
		System.out.println(ordListe);
		System.out.printf("%-20s = %s%n", "Antal ord", antalOrd);
		System.out.printf("%-20s = %s%n", "Antal unikke ord", ordListe.size());
	}

	// b) Write a program that
	// • Reads the words in a text file and inserts the
	// words in a TreeMap. Use words as keys and the count of each word
	// as value in the map.
	// • Prints the words in the map with count bigger than or equal to*100,
	// the total number of words, and the number of different words.

	public static void findAlleOrdIFileTreeMap(String path) {

		TreeMap<String, Integer> ordListe = new TreeMap<>();
		int antalOrd = 0;

		File f = new File(path);

		try (Scanner s = new Scanner(f)) {

			while (s.hasNext()) {
				String key = s.next();
				key = key.replaceAll("[^a-zA-ZæøåÆØÅ\\d]", "").toLowerCase();
				antalOrd++;

				if (ordListe.containsKey(key)) {

					int value = ordListe.get(key) + 1;

					ordListe.put(key, value);
				} else {
					ordListe.put(key, 1);

				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("printer alle ord der fremgår over 100 gange");

		// Map<String, Integer> frequencies = new TreeMap<>();

		for (String ord : ordListe.keySet()) {
			if (ordListe.get(ord) > 100) {
				System.out.println("- " + ord + " " + ordListe.get(ord));
			}
		}
		System.out.printf("%-20s = %s%n", "Antal ord", antalOrd);
		System.out.printf("%-20s = %s%n", "Antal unikke ord", ordListe.size());
	}

	// c)
	//
	// Reads the words in a text file and inserts the words in a LinkedHashMap.
	// Use the hash code of a word as key and the set of words with the same hash
	// code as value.
	// Prints the hash code and the set of words, but only for sets containing more
	// than one

}
