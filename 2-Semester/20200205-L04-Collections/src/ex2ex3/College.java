package ex2ex3;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class College {
	private String name;

//	private List<Student> students = new ArrayList<Student>();
//	private Set<Student> students = new LinkedHashSet<Student>();
	private Map<Integer, Student> students = new LinkedHashMap<Integer, Student>();

	public College(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addStudent(Student s) {
		students.put(s.getStudentNo(), s);
	}

	public void removeStudent(Student s) {
		students.remove(s.getStudentNo());
	}

	public List<Student> getStudentsList() {
		return new ArrayList<Student>(students.values());
	}

	public Map<Integer, Student> getStudents() {
		return students;
	}
//	public void addStudent(Student s) {
//		students.add(s);
//	}
//
//	public Set<Student> getStudents() {
//		return students;
//	}
//	// List
//	public List<Student> getStudents() {
//		return students;
//	}

//	public void removeStudent(Student s) {
//		if (students.contains(s)) {
//			students.remove(s);
//		}
//	}

	public double calcAverage() {
		double average = 0;
		int studentsWithGrades = 0;
		for (int studentNo : students.keySet()) {
			double sAverage = 0;
			List<Integer> grades = students.get(studentNo).getGrades();
			// get average for this student
			if (grades.size() != 0) {
				studentsWithGrades++;
				for (double grade : grades) {
					sAverage += grade;
				}
				average += sAverage / grades.size();
			}
		}
		return average / studentsWithGrades;
	}
//	// set
//	public double calcAverage() {
//		double average = 0;
//		for (Student s : students) {
//			double sAverage = 0;
//			// get average for this student
//			for (double grade : s.getGrades()) {
//				sAverage += grade;
//			}
//			average += sAverage / s.getGrades().size();
//		}
//		return average / students.size();
//	}

	public Student findStudent(int studentNo) {
		return students.get(studentNo);
	}
//	// set
//	public Student findStudent(int studentNo) {
//		Student s = null;
//		Iterator<Student> i = students.iterator();
//		while (i.hasNext() && s == null) {
//			Student lookup = i.next();
//			if (lookup.getStudentNo() == studentNo) {
//				s = lookup;
//			}
//		}
//		return s;
//	}

//	// List
//	public Student findStudent(int studentNo) {
//		Student s = null;
//		int i = 0;
//		while (i < students.size() && s == null) {
//			if (students.get(i).getStudentNo() == studentNo)
//				s = students.get(i);
//			i++;
//		}
//		return s;
//	}

	@Override
	public String toString() {
		return super.toString();
	}
}
