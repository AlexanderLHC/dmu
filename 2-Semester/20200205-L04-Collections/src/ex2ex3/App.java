package ex2ex3;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class App {

	public static void main(String[] args) {
		Random r = new Random();
		Student s1 = new Student(1, "Hermione Granger");
		Student s2 = new Student(2, "Hannah Abbott");
		Student s3 = new Student(3, "Lily Evans");
		College c = new College("Hogwarts");
		c.addStudent(s1);
		c.addStudent(s2);
		c.addStudent(s3);

		System.out.println("Students of " + c.getName());
		for (int studentNo : c.getStudents().keySet()) {
			Student s = c.getStudents().get(studentNo);
			System.out.println("=========");
			System.out.printf("%s grades: ", s.getName());
			double sum = 0;
			for (int i = 0; i < 3; i++) {
				int grade = r.nextInt(12) + 1;
				sum += grade;
				s.addGrade(grade);
				System.out.printf("%s, ", grade);
			}
			System.out.println();
			System.out.printf("Average = %.4s%n", sum / s.getGrades().size() * 1.0);
		}
		// Null grade test
		Student s4 = new Student(4, "James Potter");
		c.addStudent(s4);

		System.out.println("\n\n---------");
		System.out.println("All average " + c.calcAverage());
		System.out.printf("Find student with number %s = %s %n", "1", c.findStudent(1));
		System.out.printf("Find student with number %s = %s %n", "4", c.findStudent(4));

		List<Student> studentList = c.getStudentsList();
		System.out.printf("Unsorted = %s %n", studentList);
		Collections.sort(studentList, new StudentNameComperator());
		System.out.printf("Sorted = %s %n", studentList);

	}

}
