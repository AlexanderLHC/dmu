package ex2ex3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Student implements Comparable<Student> {
	private int StudentNo;
	private String name;
	private List<Integer> grades = new ArrayList<Integer>();

	public Student(int studentNo, String name) {
		StudentNo = studentNo;
		this.name = name;
	}

	public int getStudentNo() {
		return StudentNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addGrade(int grade) {
		grades.add(grade);
	}

	public List<Integer> getGrades() {
		return grades;
	}

	@Override
	public String toString() {
		return String.format("(%s) %s, grades : %s.", StudentNo, name, Arrays.asList(grades));
	}

	@Override
	public int compareTo(Student o) {
		// Can run into integeroverflow thus not preferable
		// return StudentNo - o.StudentNo;
		return (StudentNo < o.StudentNo) ? -1 : ((StudentNo == o.StudentNo) ? 0 : 1);
	}

	@Override
	public int hashCode() {
		return StudentNo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		return (obj instanceof Student && (((Student) obj).getStudentNo()) == StudentNo) ? true : false;
	}
}
