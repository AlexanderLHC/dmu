package ex1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyTime implements Serializable {
    private int time;
    private List<String> times;

    public MyTime(){
       times = new ArrayList<>();
       time = 0;
    }

    public void increase(){
        time++;
    }

    public void reset(){
        time =0;
    }

    public int getTime() {
        return time;
    }

    public void saveTime(){
        times.add(time + "");
    }

}
