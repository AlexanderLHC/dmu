package ex1;

import java.io.*;

public class Tester {

    public static void main(String[] args)  {
        MyTime myTime = new MyTime();
        MyTime myTimeEmpty = new MyTime();

        System.out.printf("Current time  : %5s%n", myTime.getTime());
        myTime.increase();
        System.out.printf("Increased time: %5s%n", myTime.getTime());
        myTime.increase();
        System.out.printf("Increased time: %5s%n", myTime.getTime());

        try (FileOutputStream fileOut = new FileOutputStream("myobj.ser");
             ObjectOutputStream out = new ObjectOutputStream(fileOut);
        ) {
           out.writeObject(myTime);
        } catch (IOException e) {
            System.out.printf("%s%n", e.getMessage());
        }

        try (FileInputStream fileIn = new FileInputStream("myobj.ser");
             ObjectInputStream in = new ObjectInputStream(fileIn);) {
            myTimeEmpty = (MyTime) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.printf("%s%n", e.getMessage());
        }

        System.out.printf("MyTime1 %s compared to MyTimeEmpty %s%n", myTime.getTime(), myTimeEmpty.getTime());
    }
}
