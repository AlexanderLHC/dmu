package ex3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ListAndMap {

	/**
	 * Opgave 3)
	 * 
	 * 
	 * a) Lav en liste med 8 tal. Udskriv listen vha. en iterator.
	 * 
	 * b) Lav en map med parrene (2,4), (3,9), (4, 16), (5, 25) og (6,36).
	 * 
	 * Udskriv parrene vha. en iterator på keys (brug printF() til at få en pæn
	 * udskrift med paranteser omkring talparrene).
	 * 
	 */
	public static void main(String[] args) {
		List<Integer> list = new LinkedList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

		System.out.println("a)");
		list.forEach((i) -> System.out.printf("%s ", i));
		System.out.println();

		System.out.println("b)");
		Map<Integer, Integer> pairs = new HashMap<>();
		pairs.put(2, 4);
		pairs.put(3, 9);
		pairs.put(4, 16);
		pairs.put(5, 25);
		pairs.put(6, 36);

		Iterator<Map.Entry<Integer, Integer>> pI2 = pairs.entrySet().iterator();
		while (pI2.hasNext()) {
			Map.Entry<Integer, Integer> pair = pI2.next();
			System.out.printf("(%s, %s), ", pair.getKey(), pair.getValue());
		}

	}

}
