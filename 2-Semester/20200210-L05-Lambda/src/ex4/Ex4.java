package ex4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;

public class Ex4 {

	public static void main(String[] args) {
		List<Runner> runners = new ArrayList<>();
		runners.addAll(Arrays.asList(new Runner("Ib", 30), new Runner("Per", 50), new Runner("Ole", 27),
				new Runner("Ulla", 40), new Runner("Jens", 35), new Runner("Hans", 28)));
		System.out.println("a) \n" + runners);

		// a)
		// casts exception because runner is removed during iteration
		// OBS: Throws exception.
		// for (Runner runner : runners) {
		// if (runner.getLapTime() > 40)
		// runners.remove(runner);
		// }
		// System.out.println(runners);
		// System.out.println();

		// b)
		List<Runner> bRunners = new ArrayList<Runner>(runners);
		ListIterator<Runner> itRunners = bRunners.listIterator();
		while (itRunners.hasNext()) {
			if (itRunners.next().getLapTime() >= 40)
				itRunners.remove();

		}
		System.out.printf("b)%nRunners faster than 40: %s%n", bRunners);

		// c-d)
		List<Runner> cRunners = new ArrayList<Runner>(runners);
		boolean hasRemoved = removeIf(cRunners, (r) -> r.getLapTime() >= 40);
		System.out.printf("c-d)%nRunners faster than 40: %s. Any removed %s%n", cRunners, hasRemoved);

		// e)
		List<Runner> eRunners = new ArrayList<Runner>(runners);
		eRunners.removeIf((r) -> r.getLapTime() >= 40);
		System.out.printf("e)%nRunners faster than 40: %s%n", eRunners);

	}

	public static boolean removeIf(List<Runner> runners, Predicate<Runner> filter) {
		ListIterator<Runner> itRunners = runners.listIterator();
		boolean hasRemoved = false;
		while (itRunners.hasNext()) {
			if (filter.test(itRunners.next())) {
				itRunners.remove();
				hasRemoved = true;
			}
		}
		return hasRemoved;
	}

}
