package ex1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Ex1 {

	public static void main(String[] args) {
		List<Person> list = Arrays.asList(new Person("Bent", 25), new Person("Susan", 34), new Person("Mikael", 60),
				new Person("Klaus", 44), new Person("Birgitte", 17), new Person("Liselotte", 9));
		List<Person> persons = new ArrayList<Person>(list);
		System.out.println(persons);
		System.out.println();

		// a)
		System.out.printf("a) First person in list who is 44 years old: %s%n",
				findFirst(list, (p) -> p.getAge() == 44));
		// b)
		System.out.printf("b) First person in list starting with S: %s%n",
				findFirst(list, (p) -> p.getName().charAt(0) == 'S'));
		// c)
		System.out.printf("c) First person in list containing more than one i: %s%n",
				findFirst(list, (p) -> p.getName().indexOf('i') != p.getName().lastIndexOf('i')));
		// d)
		System.out.printf("d) First person in list with name length equal age: %s%n",
				findFirst(list, (p) -> p.getName().length() == p.getAge()));
		// e)
		System.out.printf("e) Find all people older than 30: %s%n", findAll(persons, p -> p.getAge() < 30));
		// f)
		System.out.printf("f) Find all people with i in name: %s%n", findAll(list, (p) -> p.getName().contains("i")));
		// g)
		System.out.printf("g) Find all people starting with S: %s%n",
				findAll(list, (p) -> p.getName().charAt(0) == 'S'));
		// h)
		System.out.printf("h) Find all people with name longer than 5: %s%n",
				findAll(list, (p) -> p.getName().length() > 5));
		// i)
		System.out.printf("i) Find all people with name longer than 6 and age lower than 40: %s%n",
				findAll(list, (p) -> p.getName().length() > 6 && p.getAge() < 40));

	}

	/**
	 * Returns from the list the first person that satisfies the predicate. Returns
	 * null, if no person satisfies the predicate.
	 */
	public static Person findFirst(List<Person> list, Predicate<Person> filter) {
		for (Person p : list) {
			if (filter.test(p))
				return p;
		}
		return null;
	}

	public static List<Person> findAll(List<Person> list, Predicate<Person> filter) {
		List<Person> found = new ArrayList<Person>();
		for (Person p : list) {
			if (filter.test(p))
				found.add(p);
		}
		return found;
	}
}
