package ex2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Predicate;

import ex4.Runner;

public class Ex2 {

	public static void main(String[] args) {
		List<Runner> runners = new ArrayList<>();
		runners.addAll(Arrays.asList(new Runner("Ib", 30), new Runner("Per", 50), new Runner("Ole", 27),
				new Runner("Ulla", 40), new Runner("Jens", 35), new Runner("Hans", 28)));
		System.out.println(runners);
		System.out.println();

		// a)
		// Udskriv en linie for hver løber med name og lapTime ved at bruge
		// List.forEach() metoden

		System.out.println("a) each runner");
		System.out.printf("| %-5s | %5s |%n", "name", "time");
		runners.forEach((r) -> System.out.printf("| %-5s | %5s |%n", r.getName(), r.getLapTime()));
		System.out.println();

		// b)
		// Som a), men udskriv kun løberne med lapTime < 30.
		System.out.println("b) each runner with laptime < 30");
		System.out.printf("| %-5s | %5s |%n|-------|-------|%n", "name", "time");

		runners.forEach(r -> {
			if (r.getLapTime() < 30)
				System.out.printf("| %-5s | %5s |%n", r.getName(), r.getLapTime());
		});
		System.out.println();

		// c)
		// Sorter løberne stigende efter lapTime ved at bruge List.sort() metoden med en
		// Comparator
		System.out.println("c) sorted ascending");
		System.out.printf("| %-5s | %5s |%n|-------|-------|%n", "name", "time");
		// hot: runners.sort(Comparator.comparingInt(Runner::getLapTime));
		runners.sort((r1, r2) -> Integer.compare(r1.getLapTime(), r2.getLapTime()));

		runners.forEach((r) -> System.out.printf("| %-5s | %5s |%n", r.getName(), r.getLapTime()));
		System.out.println();

		/*
		 **	d) 
			Extra (ikke en del af  pensum):
			Beregn summen af løbernes lapTime ved at bruge List.forEach() og en Consumer lambda. 
			Brug en lokal variabel sum i metoden, som bruger lamda udtrykket, til at gemme summen i.
			
			
			Hint: Erklær sum som et array med længde 1.
			Bemærk: Lambda udtryk kan benytte lokale variable fra metoden, som lambda udtrykket er 
			en del af, men det kræver at variablen er erklæret final (eller er effektivt final, dvs. aldrig 
			opdateres). Vi vil derfor ikke bruge lambda udtryk, som andvender lokale variable.
		 */

		System.out.println("d) sum");
		final int sum[] = new int[1];
		runners.forEach(r -> sum[0] += r.getLapTime());
		System.out.printf("sum of laptimes = %s %n", sum[0]);
	}

	public static int[] sumRunners(int[] sum, List<Runner> runners, Predicate<Runner> filter) {
		ListIterator<Runner> itRunners = runners.listIterator();
		while (itRunners.hasNext()) {
			Runner r = itRunners.next();
			if (filter.test(r)) {
				sum[0] += r.getLapTime();
			}
		}
		return sum;
	}

}