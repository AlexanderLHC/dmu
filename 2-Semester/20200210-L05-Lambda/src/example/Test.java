package example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Test {

	public static void main(String[] args) {

		ArrayList<String> strings = new ArrayList<String>(Arrays.asList("kartoffel", "hi", "string"));

		Filter f5 = (String str) -> {
			return str.length() > 5;
		};

		System.out.println("Liste: " + strings);
		System.out.println("Words longer than 5 chars: " + countAccepted(strings, f5));
		System.out.println(
				"Words longer than 5 chars (inline lambda): " + countAccepted(strings, str -> str.length() > 5));

		// Lambda i sort
		Collections.sort(strings, (String s1, String s2) -> s1.compareTo(s2));
		System.out.println("Sorted with lambda " + strings);
	}

	public static int countAccepted(ArrayList<String> list, Filter f) {
		int count = 0;
		for (String s : list) {
			if (f.accept(s)) {
				count++;
			}
		}
		return count;
	}
}
