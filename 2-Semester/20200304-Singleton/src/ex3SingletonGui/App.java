package ex3SingletonGui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class App extends Application {
    TextField txfName = new TextField();

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Exercise 4");
        GridPane pane = new GridPane();
        initContent(pane);
        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void initContent(GridPane pane){
        int row = 0;

        // Row 1
        Label lblName = new Label("Name: ");
        Button btnAdd = new Button("Add");
        pane.add(lblName, 0,row);
        pane.add(txfName, 1, row);

        pane.add(btnAdd, 2, row);
        row++;
    }
}
