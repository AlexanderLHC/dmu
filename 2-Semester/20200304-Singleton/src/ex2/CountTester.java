package ex2;

public class CountTester {

    public static void main(String[] args) {
        Counter c1 = Counter.getCounter();
        Counter c2 = Counter.getCounter();

        System.out.printf("Current count: (c1) %s (c2) %s%n", c1.getCount(), c2.getCount());
        c1.countUp();
        System.out.printf("Add one on c1: (c1) %s (c2) %s%n", c1.getCount(), c2.getCount());
        c2.countUp();
        System.out.printf("Add one on c2: (c1) %s (c2) %s%n", c1.getCount(), c2.getCount());
        c2.zero();
        System.out.printf("Resets %s%n", c2.getCount());

        System.out.printf("Points on same location in memory (c1) %s and (c2) %s%n", c1, c2);
    }
}
