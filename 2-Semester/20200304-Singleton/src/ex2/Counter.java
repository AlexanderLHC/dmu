package ex2;

public class Counter {
    private static Counter thisCounter;
    private int count;

    // Singleton
    private Counter(){ count = 0;}

    public static Counter getCounter(){
        if (thisCounter == null){
            thisCounter = new Counter();
        }
        return thisCounter;
    }

    public void countUp() {
       count++;
    }
    public void times2() {
        count = count * 2;
    }
    public void zero() {
        count = 0;
    }
    public int getCount() {
        return count;
    }
}
