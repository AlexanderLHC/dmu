package ex3;

public class CalcNoMethod {

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.printf("no_%s = %s%n", i, CalcNo(i));
		}
	}

	private static int CalcNo(int n) {
		if (n == 0)
			return 2;
		if (n == 1)
			return 1;
		if (n == 2)
			return 5;

		if (n % 2 == 0) {
			return 2 * CalcNo(n - 3) - CalcNo(n - 1);
		} else {
			return CalcNo(n - 1) + CalcNo(n - 2) + 3 * CalcNo(n - 3);
		}
	}

	/*
	 * For both even and uneven numbers previous 3 numbers must be remembered. 
	 * a = n-3
	 * b = n-2 
	 * c = n-1
	 */
//	private static int CalcNoIt(int n) {
//		if (n == 0)
//			return 2;
//		if (n == 1)
//			return 1;
//		if (n == 2)
//			return 5;
//
//		int a = 5;
//		int b = 1;
//		int c = 2;
//
//		for (int i = 3; i <= n; i++) {
//			int r;
//			if (i % 2 == 0) {
//				r = 2 * a - c;
//			} else {
//
//			}
//		}
//	}

}