package exSierpinskisTriangles;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class DrawTriangle extends Application {
	public int count;
	private Label lblOrder;
	private Canvas canvas;
	private GraphicsContext gc;

	/*
	 * The figure below shows a Sierpinski triangle of order 5.
		A Sierpinski triangle of order 0 is just a triangle with base 2*h and height h (where h is an
		integer number).
		A Sierpinski triangle of order 1 is a Sierpinski triangle of order 0 with triangles of height h/2
		added at the 3 corners. A Sierpinski triangle of order 2 is a Sierpinski triangle of order 1 with
		triangles of height h/4 added at the 3 corners of the 3 inner triangles. And so forth for
		Sierpinski triangles of higher orders.
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		count = 0;
		int dimension = 1000;

		Pane root = new Pane();
		primaryStage.setTitle("Sierpinski triangle");
		StackPane sp = new StackPane();
		canvas = new Canvas(dimension, dimension);
		sp.getChildren().add(canvas);
		root.getChildren().add(sp);
		sp.setStyle("-fx-background-color: grey");
		// TODO: move gc away from methods params
		gc = canvas.getGraphicsContext2D();

		// Buttons
		HBox hbButtons = new HBox();
		Button btnLess = new Button("<");
		Button btnMore = new Button(">");
		lblOrder = new Label(count + "");
		lblOrder.setStyle("-fx-background-color: white; " + "-fx-font-size: 20px;");
		hbButtons.getChildren().add(btnLess);
		hbButtons.getChildren().add(lblOrder);
		hbButtons.getChildren().add(btnMore);
		btnMore.setOnAction(e -> onClickMore());
		btnLess.setOnAction(e -> onClickLess());
		sp.getChildren().add(hbButtons);

		// Initial
		drawTriangle(gc, 50, 400, 350);

		Scene scene = new Scene(root, 600, 600);
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	private void onClickLess() {
		if (count > 0)
			count--;

		updateCanvas();
	}

	private void onClickMore() {
		count++;
		updateCanvas();
	}

	private void updateCanvas() {
		lblOrder.setText(count + "");
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		drawTriangle(gc, 50, 400, 350);
		drawInnerTriangles(gc, 50, 400, 350, count);
	}

	private void drawTriangle(GraphicsContext gc, double x, double y, double height) {
		gc.strokeLine(x, y, x + height, y);
		gc.strokeLine(x, y, x + height, y - height);
		gc.strokeLine(x + height, y, x + height * 2, y);
		gc.strokeLine(x + height, y - height, x + height * 2, y);
	}

	private void drawInnerTriangles(GraphicsContext gc, double x, double y, double h, int i) {
		double h1 = h / 2;

		drawTriangle(gc, x, y, h1);
		drawTriangle(gc, x + h * 2 - h1 * 2, y, h1);
		drawTriangle(gc, x + h - h1, y - h + h1, h1);

		if (i > 0) {
			drawInnerTriangles(gc, x, y, h1, i - 1);
			drawInnerTriangles(gc, x + h * 2 - h1 * 2, y, h1, i - 1);
			drawInnerTriangles(gc, x + h - h1, y - h + h1, h1, i - 1);
		}
	}

}
