package ex2;

public class Palindrome {

	public static void main(String[] args) {
		String s1 = "A nut for a jar of tuna.";
		String s2 = "Not a palindrome";
		String s3 = "Taco cat";
		String s4 = "taccat";

//		System.out.printf("Is \"%s\" a palindrome? Answer is %s%n", s1, isPalindrome(s1));
//		System.out.printf("Is \"%s\" a palindrome? Answer is %s%n", s2, isPalindrome(s2));
//		System.out.printf("Is \"%s\" a palindrome? Answer is %s%n", s3, isPalindrome(s3));
		System.out.printf("Is \"%s\" a palindrome? Answer is %s%n", s4, isPalindrome(s4));

	}

	public static boolean isPalindrome(String s) {
		return isPalindrome(s, 0, s.length() - 1);
	}

	/**
	 * 
	 * @param s
	 * @param li left index (pointer)
	 * @param ri right index (pointer)
	 * @return
	 */
	public static boolean isPalindrome(String s, int li, int ri) {
		boolean isPalindrome = false;

		if (li >= ri) {
			isPalindrome = true;
		} else {
			// left
			char left = s.toLowerCase().charAt(0 + li);
			if (!Character.isLetter(left)) {
				li++;
				isPalindrome = isPalindrome(s, li, ri);
			}
			// right
			char right = s.toLowerCase().charAt(ri);
			if (!Character.isLetter(right)) {
				ri--;
				isPalindrome = isPalindrome(s, li, ri);
			}

			if (left == right) {
				li++;
				ri--;
				isPalindrome = isPalindrome(s, li, ri);
			}
		}

		return isPalindrome;
	}

}
