package ex0;

import java.util.Arrays;

public class CountEvenNumbers {

	public static void main(String[] args) {
		int[] l1 = new int[10];
		for (int i = 0; i < l1.length; i++) {
			l1[i] = i;
		}
		System.out.printf("%s even counter = %d%n", Arrays.toString(l1), countEven(l1));
	}
	
	public static int countEven(int[] list) {
		return countEven(list, 0);
	}
	
	private static int countEven(int[] list, int counter){
		
		if (list.length > 0) {
			if(list[0] % 2 == 0) {
				counter++;
			}
			int[] newList = Arrays.copyOfRange(list, 1, list.length);
			return countEven(newList, counter);
		} else {
			return counter;
		}
		
	}

}
