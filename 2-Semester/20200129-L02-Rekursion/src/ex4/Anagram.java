package ex4;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Anagram {

	public static void main(String[] args) {
		String s1 = "cat";
		String s2 = "potato";
		String s3 = "aced";
		System.out.printf("%s anagrams:%n", s1);
		for (String word : removeDuplicates(anagram(s1))) {
			System.out.printf("%5s -%s is a real word? %s%n", " ", word, isRealWord(word));
		}
		System.out.printf("%s anagrams:%n", s2);
		for (String word : removeDuplicates(anagram(s2))) {
			System.out.printf("%5s -%s is a real word? %s%n", " ", word, isRealWord(word));
		}
		System.out.printf("%s anagrams:%n", s3);
		for (String word : removeDuplicates(anagram(s3))) {
			System.out.printf("%5s -%s is a real word? %s%n", " ", word, isRealWord(word));
		}
	}

	public static ArrayList<String> removeDuplicates(ArrayList<String> list) {
		ArrayList<String> cleanedList = new ArrayList<String>();
		for (String s : list) {

			if (!cleanedList.contains(s)) {
				cleanedList.add(s);
			}
		}
		return cleanedList;
	}

	public static boolean isRealWord(String s) {
		boolean found = false;

		File dictionary = new File("words.txt");
		try (Scanner scan = new Scanner(dictionary)) {
			while (scan.hasNext() && !found) {
				if (s.equals(scan.next())) {
					found = true;
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}

		return found;
	}

	public static ArrayList<String> anagram(String s) {
		ArrayList<String> anagrams = new ArrayList<>();

		if (s.length() == 1) {
			anagrams.add(s);
		} else {
			for (int i = 0; i < s.length(); i++) {
				String subS = s.substring(0, i) + s.substring(i + 1);
				ArrayList<String> variations = anagram(subS);
				for (String variation : variations) {
					anagrams.add(s.charAt(i) + variation);
				}
			}
		}
		return anagrams;
	}

//	private static ArrayList<String> anagrams(String s){
//		if (condition) {
//			
//		}
//	}

}
