package ex1;

public class Binomialkoefficient {

	public static void main(String[] args) {
		System.out.printf("   m  %5s %5s %5s %5s %5s %5s %5s %5s%n", 0,1,2,3,4,5,6,7);
		System.out.println("n ----------------------------------------------------");
		for (int m = 0; m < 8; m++) {
			System.out.printf("%s  | ", m);
			for (int n = 0; n < 8; n++) {
				if (m >= n) {
					System.out.printf(" %5s", binomial(m, n));
				}
			}
			System.out.println();
		}
	}
	
	private static int binomial(int n, int m) {
		
		if (m == 0 || n == m) 
			return 1;
		
		return binomial(n-1, m) + binomial(n-1, m-1);
	}
}
