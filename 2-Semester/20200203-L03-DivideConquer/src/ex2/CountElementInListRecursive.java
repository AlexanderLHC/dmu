package ex2;

import java.util.ArrayList;
import java.util.Arrays;

public class CountElementInListRecursive {

	/*
	 * Exercise 2
		Write a recursive method that counts the elements with value 0 in a List<Integer>. Use the
		template from Divide-Solve-Combine.
	 */
	public static void main(String[] args) {
		ArrayList<Integer> l1 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 2, 7, 8, 2));
		int t1 = 2;
		System.out.printf("List: %s, contains %s total %s%n", l1, t1, CountElementInList(l1, t1));
	}

	public static int CountElementInList(ArrayList<Integer> list, int target) {
		return CountElementInList(list, target, 0, list.size() - 1);
	}

	private static int CountElementInList(ArrayList<Integer> list, int target, int l, int h) {
		if (l == h) {
			return (list.get(l) == target) ? 1 : 0;
		} else {
			int m = (l + h) / 2;
			int part1 = CountElementInList(list, target, l, m);
			int part2 = CountElementInList(list, target, m + 1, h);
			return part1 + part2;
		}
	}
}
