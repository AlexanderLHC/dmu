package ex3;

import java.util.Arrays;

public class mergeSortRecursive {

	/*
	 * Exercise 3
	 * The paper on Divide-Solve-Combine contains most of the code necessary to do a mergesort.
		The implementation of the method merge() is missing.
		Write the merge() method.
		Test your mergesort on [8, 56, 45, 34, 15, 12, 34, 44].
	 */

	public static void main(String[] args) {
		int[] l1 = { 8, 56, 45, 34, 15, 12, 34, 44 };

		System.out.printf("%-15s %s%n", "List = ", Arrays.toString(l1));
		mergeSort(l1);
		System.out.printf("%-15s %s", "Merge sorted = ", Arrays.toString(l1));

	}

	public static void mergeSort(int[] list) {
		mergeSort(list, 0, list.length - 1);
	}

	private static void mergeSort(int[] list, int l, int h) {

		if (l < h) {
			int m = (l + h) / 2;
			mergeSort(list, l, m);
			mergeSort(list, m + 1, h);
			merge(list, l, m, h);
		}
	}

	public static void merge(int[] list, int l, int m, int h) {
		int lSizeLower = m - l + 1;
		int lSizeHigh = h - m;

		// Populate 2 lists divided by middle
		// TODO: should be mergeable
		int[] tmpListLower = new int[lSizeLower];
		int[] tmpListHigh = new int[lSizeHigh];

		for (int i = 0; i < lSizeLower; i++) {
			tmpListLower[i] = list[l + i];
		}
		for (int i = 0; i < lSizeHigh; i++) {
			tmpListHigh[i] = list[m + 1 + i];
		}

		// Merge
		int iLower = 0;
		int iHigh = 0;
		int i = l; // start from lower boundary

		while (iLower < lSizeLower && iHigh < lSizeHigh) {
			if (tmpListLower[iLower] <= tmpListHigh[iHigh]) {
				list[i] = tmpListLower[iLower];
				iLower++;
			} else {
				list[i] = tmpListHigh[iHigh];
				iHigh++;
			}
			i++;
		}

		while (iLower < lSizeLower) {
			list[i] = tmpListLower[iLower];
			i++;
			iLower++;
		}
		while (iHigh < lSizeHigh) {
			list[i] = tmpListHigh[iHigh];
			i++;
			iHigh++;
		}
	}

}
