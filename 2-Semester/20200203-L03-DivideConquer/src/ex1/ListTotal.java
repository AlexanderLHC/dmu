package ex1;

import java.util.ArrayList;
import java.util.Arrays;

public class ListTotal {

	/*
	 * Exercise 1
		Write a recursive method that totals all the elements in a List<Integer>. Use the template from
		Divide-Solve-Combine (Del, Løs og kombiner).
	 */

	public static void main(String[] args) {
		ArrayList<Integer> l1 = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
		System.out.println(sum(l1));
	}

	/*
	 * Metode: Løs(P)
	   Hvis p er simpelt, så returner L = løsning
	   Ellers
			Opdel P i P1 og P2
			L1 = Løs(P1)
			L2 = Løs(P2)
			L = Kombinér(L1,L2)
			Returner L;
	 */
	public static int sum(ArrayList<Integer> list) {
		return sum(list, 0, list.size() - 1);
	}

	private static int sum(ArrayList<Integer> list, int i1, int i2) {
		if (list.get(i1) == list.get(i2)) {
			return list.get(i1);
		} else {
			int middle = i1 + i2 / 2;
//			int p1 = sum(list, i1, middle);
//			int p2 = sum(list, middle + 1, i2);
//			return p1 + p2;
			return sum(list, i1, middle) + sum(list, middle + 1, i2);
		}
	}

}
