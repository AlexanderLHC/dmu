package ex1;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class SortedLinkedList {
    private Node first;
    private Node last;
    private int size;

    public SortedLinkedList() {
        first = null;
        last = null;
    }

    /**
     * Tilføjer et element til listen, så listen fortsat er sorteret i
     * henhold til den naturlige ordning på elementerne.
     *
     * @param element det der indsættes
     */
    public void addElement(String element) {
//        sortedLinkedListIterator().add(element);
        SortedLinkedListIterator iterator = iterator();

        // if found i = -1
//        while (i < size && i < 0) {
//            lookup = (Node) iterator.next();
//            i = (lookup.data.compareTo(element) < 0)? -1: i++;
//        }

        while (iterator.hasNext() && ((String) iterator.next()).compareTo(element) > 0){
            // wait until iterater reaches goal position
            // or end
        }
        if (iterator.hasPrevious())
            iterator.previous();

        iterator.add(element);

        size++;
    }

    /**
     * Fjerner et element fra listen.
     *
     * @param element det element der fjernes
     * @return true hvis elementet blev fjernet, men ellers false.
     */
    public boolean removeElement(String element) {
        size--;
        return false;
    }

    /**
     * Returnerer antallet af elementer i listen.
     */
    public int countElements() {
        return size;
    }

    @Override
    public String toString() {
        return "Unimplemented";
    }

    public SortedLinkedListIterator iterator() {
        return new SortedLinkedListIterator();
    }
    // --------------------
    public class Node {
        Node next;
        Node previous;
        String data;

        public Node(String data) {
            this.data = data;
            next = null;
            previous = null;
        }
    }
    // --------------------
    public class SortedLinkedListIterator implements ListIterator {
        private Node position;
        private boolean isAfterNext;
        private boolean isAfterPrevious;

        public SortedLinkedListIterator(){
            position = null;
            isAfterNext = false;
            isAfterPrevious = false;
        }

        @Override
        public boolean hasNext() {
            return (position == null) ? first != null: position.next != null;
//            return (position != null)? (position.next == null) : false;
        }

        @Override
        public Object next() {
            if (hasNext()){
                isAfterNext = true;
                isAfterPrevious = false;
                if (position == null)
                    position = first;
                else
                    position = position.next;

                return position.data;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public boolean hasPrevious() {
            return (position == null) ? first != null: position.previous != null;
        }

        @Override
        public Object previous() {
            if (hasPrevious()){
                isAfterNext = false;
                isAfterPrevious = true;

                position = position.previous;
                return position.data;

            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public int nextIndex() {
            throw new UnsupportedOperationException();
        }

        @Override
        public int previousIndex() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void set(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void add(Object elementToAdd) {
            Node nodeToAdd = new Node((String) elementToAdd);
            if (position == null) {
                first = nodeToAdd;
                position = first;
            } else {
                nodeToAdd.next = position.next;
                nodeToAdd.next.previous = nodeToAdd;
                System.out.println("Adding " + nodeToAdd.data + " .. Before " + position.next.data);
                position.next = nodeToAdd;
                position = nodeToAdd;
            }
            isAfterNext = false;
            isAfterPrevious = false;
        }

    }

}