package ex1try2;



public class Tester {

    public static void main(String[] args) {
        SortedLinkedList sllBooklist = new SortedLinkedList();

        sllBooklist.addElement("1984");
        sllBooklist.addElement("The Lion, the Witch, and the Wardrobe");
        sllBooklist.addElement("The Shining");
        sllBooklist.addElement("Dracula");
        sllBooklist.addElement("Lord of the Flies");
        System.out.println("Book list size: " + sllBooklist.size());

        SortedLinkedList.SortedLinkedListIterator iterator = sllBooklist.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
