DROP TABLE Prioritet;
DROP TABLE Løn;
DROP TABLE Transaktion;
DROP TABLE KontoKunde;
DROP TABLE Kunde;
DROP TABLE Konto;
DROP TABLE Medarbejder;
DROP TABLE Afdeling;
DROP TABLE Bank;

--??CONSTRAINT KundeKey PRIMARY KEY (KundeId),

CREATE TABLE Bank
(
    navn    CHAR(20) NOT NULL,
    adresse CHAR(20),
    postNr  INT,
    byNavn  CHAR(20),
    PRIMARY KEY (navn)
);

CREATE TABLE Afdeling
(
    regNr    INT      NOT NULL,
    navn     CHAR(20) NOT NULL,
    adresse  CHAR(30) NOT NULL,
    postNr   INT      NOT NULL,
    tlfNr    INT      NOT NULL,
    bankNavn CHAR(20) NOT NULL,
    PRIMARY KEY (regNr),
    FOREIGN KEY (bankNavn) REFERENCES Bank(navn),
);

CREATE TABLE Medarbejder
(
    cprNr    DECIMAL(12) NOT NULL,
    navn     CHAR(20)    NOT NULL,
    titel    CHAR(30)    NOT NULL,
    adresse  CHAR(20)    NOT NULL,
    postNr   INT         NOT NULL,
    byNavn   CHAR(20)    NOT NULL,
    afdeling INT         NOT NULL,
    FOREIGN KEY (afdeling) REFERENCES Afdeling(regNr),
    PRIMARY KEY (cprNr)
);

CREATE TABLE Kunde
(
    cprNr  INT       NOT NULL,
    navn   CHAR(100) NOT NULL,
    postNr INT       NOT NULL,
    byNavn CHAR(20)  NOT NULL,
    PRIMARY KEY (cprNr),
);

CREATE TABLE Konto
(
    regNr       INT       NOT NULL,
    ktoNr       INT       NOT NULL,
    tekst       CHAR(120),
    saldo       INT,
    renteUdlån  INT,
    renteINdlån INT,
    PRIMARY KEY (regNr, ktoNr),
    FOREIGN KEY (regNr) REFERENCES Afdeling(regNr)
);

CREATE TABLE KontoKunde
(
    regNr INT NOT NULL,
    ktoNr INT NOT NULL,
    cprNr INT NOT NULL,

    FOREIGN KEY (regNr, ktoNr) REFERENCES Konto(regNr, ktoNr),
    FOREIGN KEY (cprNr) REFERENCES Kunde(cprNr),
);

CREATE TABLE Transaktion
(
    regNr     INT      NOT NULL,
    ktoNr     INT      NOT NULL,
    tidspunkt DATETIME NOT NULL,
    tekst     CHAR(50),
    beløb     INT      NOT NULL,
    PRIMARY KEY (regNr, ktoNr, tidspunkt),
    FOREIGN KEY (regNr, ktoNr) REFERENCES Konto(regNr,ktoNr)
);

CREATE TABLE Løn
(
    regNr   INT NOT NULL,
    ktoNr   INT NOT NULL,
    låneret INT NOT NULL,
    PRIMARY KEY (regNr, ktoNr),
    FOREIGN KEY (regNr, ktoNr) REFERENCES Konto(regNr, ktoNr)
);

CREATE TABLE Prioritet
(

    regNr     INT NOT NULL,
    ktoNr     INT NOT NULL,
    hovedstol INT NOT NULL,
    PRIMARY KEY (regNr, ktoNr),
    FOREIGN KEY (regNr, ktoNr) REFERENCES Konto (regNr, ktoNr)
);