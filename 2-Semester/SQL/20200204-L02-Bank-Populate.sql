DELETE FROM KontoKunde;
DELETE FROM Kunde;
DELETE FROM Medarbejder;
DELETE FROM Konto;
DELETE FROM Afdeling;
DELETE FROM Bank;

-- INSERT
INSERT INTO Bank
    (navn, adresse, postNr, byNavn)
VALUES
    ('Bank Mig', 'Rabalderstræde 2', '8700', 'Horsens');



INSERT INTO Afdeling
    (regNr, navn, adresse, postNr, tlfNr, bankNavn)
VALUES
    (1234, 'Banke På', 'Rabalderstræde 3', 8700, 12345678, 'Bank Mig' );


INSERT INTO Medarbejder
    (cprNr, navn, titel, adresse, postNr, byNavn, afdeling)
VALUES
    ('326661111111', 'Alexander', 'Direktør', 'Fijiski', 010101, 'Saint', 1234 );

-- Kunde J.J Thomson
INSERT INTO Kunde
    (cprNr,navn, postNr, byNavn)
VALUES
    ('326661111', 'J. J. Thomson', 8220, 'Brabrand');

INSERT INTO Konto
    ( regNr, ktoNr,tekst,saldo,renteUdlån,renteINdlån)
VALUES
    (1234, 123456789, 'work work', 100, 1, 2);

INSERT INTO KontoKunde
    (regNr,ktoNr,cprNr)
VALUES
    (1234, 123456789, 326661111);

-- 
INSERT INTO Kunde
    (cprNr,navn, postNr, byNavn)
VALUES
    ('123123123', 'Percival Lowell', 4321, 'Universe');

INSERT INTO Konto
    ( regNr, ktoNr,tekst,saldo,renteUdlån,renteINdlån)
VALUES
    (1234, 123456999, 'work work', 100, 1, 2);

INSERT INTO KontoKunde
    (regNr,ktoNr,cprNr)
VALUES
    (1234, 123456999, 123123123);

--- Otto Guericke

INSERT INTO Kunde
    (cprNr,navn, postNr, byNavn)
VALUES
    ('1239812983', 'Otto Guericke', 1234, 'World');

INSERT INTO Konto
    ( regNr, ktoNr,tekst,saldo,renteUdlån,renteINdlån)
VALUES
    (1234, 123456799, 'home home', 12005, 1, 2);

INSERT INTO KontoKunde
    (regNr,ktoNr,cprNr)
VALUES
    (1234, 123456799, 1239812983);


INSERT INTO Konto
    ( regNr, ktoNr,tekst,saldo,renteUdlån,renteINdlån)
VALUES
    (1234, 123499799, 'home home', 12005, 1, 2);

INSERT INTO KontoKunde
    (regNr,ktoNr,cprNr)
VALUES
    (1234, 123499799, 1239812983);

INSERT INTO Prioritet
    (regNr, ktoNr, hovedstol)
VALUES
    (1234, 123499799, 100);
     

-- INSERT, DELETE UPDATE

-- opg6
-- * get duplicate db without delete "rights"
-- * boolean col
-- * inaktiv kunde tabel