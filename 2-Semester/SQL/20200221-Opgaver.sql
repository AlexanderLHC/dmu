-------------
-- Opgave 1
-- Opgave 1.1
CREATE VIEW kunder
AS
SELECT K.cprNr, K.navn, K.adresse, K.postNr
FROM Kunde K;
GO

-- Opgave 1.2
CREATE VIEW kunderMedBy
AS
SELECT K.cprNr, K.navn, K.adresse, K.postNr, P.bynavn
FROM Kunde K,
     PostDistrikt P
WHERE K.postNr = P.postNr;
GO

-- Opgave 1.3
CREATE VIEW kunderMedKontoSaldo
AS
SELECT K.cprNr, K.navn, K.adresse, K.postNr, P.bynavn, KO.ktoNr, KO.saldo
FROM Kunde K,
     PostDistrikt P,
     KundeHarKonto KK,
     Konto KO
WHERE K.postNr = P.postNr
  AND K.cprNr = KK.cprNr
  AND KK.ktonr = KO.ktoNr;
GO

-- Opgave 1.4
CREATE VIEW kundeDerErMedarbejder AS
SELECT K.navn, K.cprNr, K.adresse, K.postNr, M.titel
FROM Kunde K
         LEFT JOIN Medarbejder M ON K.cprNr = M.cprNr
UNION
SELECT M.navn, M.cprNr, M.adresse, M.postNr, M.titel
FROM Medarbejder M
GO

DROP VIEW kundeDerErMedarbejder;

-- SELECT * FROM kunder;
-- SELECT * FROM kunderMedBy;
-- SELECT * FROM kundeDerErMedarbejder;

-------------
-- Opgave 2
-- Opgave 2.1

GO
CREATE PROCEDURE alleKontoOplysninger
AS
SELECT K.ktoNr
     , K.saldo
     , A.regNr
     , A.navn
FROM Konto K,
     Afdeling A
WHERE k.regNr = A.regNr;
GO



exec alleKontoOplysninger;

-- Opgave 2.2
GO
CREATE PROCEDURE kontoOplysningerFraCPR @cpr char(10)
AS
BEGIN
    SELECT KU.navn, KO.ktoNr, KO.saldo
    FROM Kunde KU
             INNER JOIN KundeHarKonto KK ON KK.cprNr = KU.cprNr
             INNER JOIN Konto KO ON KO.ktoNr = KK.ktonr
    WHERE KU.cprNr = @cpr;
END
GO
DROP PROCEDURE kontoOplysningerFraCPR;

exec kontoOplysningerFraCPR '1209631223';

-- Opgave 2.3
GO
CREATE PROCEDURE indeHavendeFraCPR @cpr char(10),
                                   @indestående DECIMAL(20, 2) OUTPUT
AS
BEGIN
    SELECT @indestående = sum(K.saldo)
    FROM Konto K,
         KundeHarKonto KK
    WHERE K.ktoNr = KK.ktonr
      AND KK.cprNr = @cpr
END
GO

DROP PROCEDURE indeHavendeFraCPR;

GO
DECLARE @indestående DECIMAL(20, 2)
EXECUTE indeHavendeFraCPR '1209631223', @indestående OUTPUT;
SELECT @indestående AS "Indehavende";
GO

GO
CREATE PROCEDURE opretKonto @regNr INT,
                            @ktoNr INT,
                            @tekst CHAR(20),
                            @saldo REAL,
                            @renteIndlån REAL,
                            @renteUdlån REAL
AS
BEGIN
    INSERT INTO Konto (regNr, ktoNr, tekst, saldo, renteIndlån, renteUdlån)
    VALUES (@regNr, @ktoNr, @tekst, @saldo, @renteIndlån, @renteUdlån);
END
GO

GO
EXECUTE opretKonto '1234', '129191912', 'Wauw konto', '999', '9999', '0.1';
GO

-------------
-- Opgave 3
-- Opgave 3.1
CREATE TRIGGER maks3KontiPerKunde ON KundeHarKonto
    AFTER INSERT
    AS
    DECLARE @cprNr AS CHAR(10)
    SET @cprNr = (SELECT cprNr FROM Inserted);
    IF (SELECT count(cprNr) FROM KundeHarKonto WHERE cprNr= @cprNr) > 3
    BEGIN
        print 'Højst 3 konti er tilladt for en kunde'
        ROLLBACK TRANSACTION
    end
;
CREATE TRIGGER maks3KontiPerKunde ON KundeHarKonto
    AFTER INSERT
    AS
    IF (SELECT count(KK.cprNr) FROM KundeHarKonto KK, Inserted I WHERE KK.cprNr= I.cprNr GROUP BY KK.cprNr) > 3
    BEGIN
        print 'Højst 3 konti er tilladt for en kunde'
        ROLLBACK TRANSACTION
    end
;
-- Virker?
-- CREATE TRIGGER maks3KontiPerKunde ON KundeHarKonto
--     AFTER INSERT
--     AS
--     IF (SELECT count(cprNr) FROM Inserted WHERE cprNr= KundeHarKonto.cprNr) > 3
--     BEGIN
--         print 'Højst 3 konti er tilladt for en kunde'
--         ROLLBACK TRANSACTION
--     end
-- ;
--
DROP TRIGGER maks3KontiPerKunde;

INSERT INTO KundeHarKonto (cprNr, regNr, ktonr) VALUES (3112692132, 1234, 123434343);
INSERT INTO KundeHarKonto (cprNr, regNr, ktonr) VALUES (3112692132, 1234, 124434343);
