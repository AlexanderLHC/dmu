-- 2020-Feb-11
-- Opgaveseddel:


-- 1. Find gennemsnitssaldo pr. afdeling 
SELECT
    AVG(K.saldo) AS gennemsnitssaldo,
    k.regNr
FROM
    Konto K
GROUP BY K.regNr;

-- 2. Find gennemsnitssaldo pr. afdeling for kunder med en lønkonto 
SELECT
    AVG(K.saldo) AS gennemsnitssaldo,
    k.regNr
FROM
    Løn L
    INNER JOIN Konto K ON K.regNr = L.regNr AND K.ktoNr = L.ktoNr
GROUP BY k.regNr;

-- 3. Find antal kunder pr. afdeling 
SELECT
    COUNT(KK.cprNr) AS kundeAntal,
    KK.regNr
FROM
    KundeHarKonto KK
GROUP BY KK.regNr

-- 4. Find de kunder, der ikke har en lønkonto 
-- For alle lønkonti gå til kunde
-- Kunde except Løn
SELECT
    K.*
FROM
    Kunde K
    INNER JOIN KundeHarKonto KK ON KK.cprNr = K.cprNr
WHERE KK.ktonr NOT IN (SELECT Løn.ktonr FROM Løn);
--LEFT JOIN Løn L USING (KK.ktoNr, KK.regNr);

-- 5. Find de postnumre, hvor der hverken bor kunder eller medarbejdere 
    SELECT
        postNr
    FROM
        PostDistrikt
EXCEPT
    SELECT
        K.postNr
    FROM
        Kunde K
EXCEPT
    SELECT
        M.postNr
    FROM
        Medarbejder M


-- 6. Find navn, kontonummer transaktionstekst og transaktionsbeløb for en given kunde. 
-- (sorteret efter transaktionsdato) 

SELECT
    K.navn,
    KK.ktoNr,
    T.tekst,
    T.beløb
FROM
    Kunde K
    INNER JOIN KundeHarKonto KK ON K.cprNr = KK.cprNr
    INNER JOIN Transaktion T ON T.ktoNr = KK.ktonr AND T.regNr = T.regNr
WHERE k.cprNr = 3112692132
ORDER BY T.dato;

-- 7. FOR alle prioritetskonti ønskes forskellen på saldo og hovedstol beregnet. 
SELECT
    K.Navn,
    P.hovedstol-KO.saldo AS restGæld,
    KK.ktonr
FROM
    KundeHarKonto KK
    INNER JOIN Kunde K ON K.cprNr = KK.cprNr
    INNER JOIN Konto KO ON KO.regNr = KK.regNr AND KO.ktoNr = KK.ktonr
    INNER JOIN Prioritet P ON P.regNr = KK.regNr AND P.ktoNr = KK.ktonr;

-- 8. Find de konti, der ikke har transaktioner.
    SELECT
        regNr,
        ktoNr
    FROM
        Konto
EXCEPT
    SELECT
        regNr,
        ktoNr
    FROM
        Transaktion;
