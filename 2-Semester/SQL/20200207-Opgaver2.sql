-- Opgave 2.2 

SELECT
    *
FROM
    Kunde K
    FULL JOIN Ordre O ON K.KundeId = O.KundeId;

-- Opgave 2.3
SELECT
    K.Navn,
    V.VareNr,
    O.Antal
FROM
    Kunde K
    INNER JOIN Ordre O ON K.KundeId = O.KundeId
    INNER JOIN Vare V ON O.VareNr = V.VareNr
WHERE O.OrdreDato = '2009-02-01';

-- Opgave 2.4 

SELECT
    K.KundeId,
    P.PostNr,
    P.Bynavn,
    V.VareNr,
    V.Emne,
    O.Antal
FROM
    Kunde K
    INNER JOIN PostDistrikt P ON K.PostNr = P.PostNr
    INNER JOIN Ordre O ON K.KundeId = O.KundeId
    INNER JOIN Vare V ON O.VareNr = V.VareNr
WHERE O.OrdreDato = '2009-02-02';


-- Opg 2.5
-- SELECT
--     *
-- FROM
--     Kunde K
--     INNER JOIN Medarb M ON K.Navn = M.Navn AND K.Adresse = M.Adresse AND K.PostNr = M.PostNr;
    SELECT
        *
    FROM
        Kunde
INTERSECT
    SELECT
        *
    FROM
        Medarb;

-- opg 2.6 
    SELECT
        *
    FROM
        Kunde K
UNION
    SELECT
        *
    FROM
        Medarb;

-- opg 2.7
SELECT
    *
FROM     Kunde K
WHERE K.KundeId NOT IN (SELECT  MedarbId FROM    Medarb );

SELECT * FROM Kunde K 
EXCEPT
SELECT * FROM Medarb;