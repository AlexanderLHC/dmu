-- Opgave 1.2
-- 
SELECT
    K.navn
FROM
    Kunde K
    INNER JOIN KontoKunde KK ON K.cprNr=KK.cprNr
    INNER JOIN Konto KO ON KK.regNr=KO.regNr AND KK.ktoNr=KO.ktoNr
WHERE KO.saldo > 12000;

-- Opg 1.3
SELECT
    K.navn
FROM
    Kunde K
WHERE K.postNr = 4321;

-- Opgave 1.7Find navnene og
-- afdeling på alle kunder, der har en prioritetskonto.
