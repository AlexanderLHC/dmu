-- Opg 1.1
--\sigma_postNr=’8230’(Afdeling)

-- Opg 1.2
--\pi regNr, ktoNr(Konto)
SELECT regNr, ktoNr FROM Konto;
-- Opg 1.3
--\pi afdnavn, regNr, ktoNr, saldo(Afdeling \bowtie Konto)
SELECT A.navn, K.regNr,  K.ktoNr, K.saldo FROM Afdeling A INNER JOIN Konto K on A.regNr = K.regNr;
-- Opg 1.4
--\pi afdnavn, regNr, ktoNr, saldo(\sigma_saldo>10000(Afdeling \bowtie Konto))msdb
SELECT A.navn, K.regNr,  K.ktoNr, K.saldo FROM Afdeling A INNER JOIN Konto K on A.regNr = K.regNr
WHERE K.saldo > 10000;
-- Opg 1.5
\pi navn,adresse,postNr,bynavn (Kunde \bowtie Postdistrikt)
SELECT navn, adresse, postNr, bynavn FROM Kunde K INNER JOIN PostDistrikt PD on K.postNr = PD.postNr
