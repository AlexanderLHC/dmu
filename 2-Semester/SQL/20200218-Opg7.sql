CREATE TABLE TestTabel (
    letter CHAR(1)
    , firstname VARCHAR(30)
    , age INT
    , height DECIMAL
    , birthday DATETIME
    , isMale BIT
    );

INSERT INTO TestTabel (letter, firstname, age, height, birthday, isMale) VALUES ('A', 'Abe', 32, '200.1', '1945-11-11 13:37:32', 1);


INSERT INTO TestTabel (letter, firstname, age, height, birthday, isMale) VALUES ('A', 'Abe', 32, '200.1', '1945-11-11 13:37:32', 4);
INSERT INTO TestTabel (letter, firstname, age, height, birthday, isMale) VALUES ('A', 'Abe', 32, '200.1', '1945-11-11 13:37:32', 2);

SELECT * FROM TestTabel;

DELETE FROM TestTabel;
