package ex5color;

public interface ColorObserver {
    void update(String color);
}
