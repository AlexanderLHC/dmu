package ex6bookstore;

import java.util.LinkedHashMap;

public class Salesman implements BookObserver {
	private String name;

	public Salesman(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public void update(Book book) {
		LinkedHashMap<Book, Integer> similarSales = new LinkedHashMap<>(); // <Book, salesCount>
		for (Customer c : book.getCustomers()) {
			for (Book boughtBook : c.getPurchasedBooks()) {
				if (!boughtBook.equals(book)) {
					similarSales.put(boughtBook, similarSales.getOrDefault(boughtBook, 0) + 1);
				}
			}
		}
		System.out.println("Similar sales: ");
		similarSales.forEach((suggestedBook, salesCount) -> {
			System.out.println("- " + suggestedBook.getTitle() + " : bought by " + salesCount + " others who also "
					+ "liked this book.");
		});
	}
}
