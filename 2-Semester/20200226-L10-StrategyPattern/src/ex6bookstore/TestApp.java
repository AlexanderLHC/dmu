package ex6bookstore;

public class TestApp {

	public static void main(String[] args) {
		Purchaser birgitte = new Purchaser("Birgitte");
	    Salesman wrh = new Salesman("William Rowan Hamilton");


	    // Book 1
		Book b1 = new Book("Donald Duck");
		b1.addObserver(birgitte);
		b1.addObserver(wrh);
		birgitte.purchaseBook(b1, 6);
		System.out.println();

		// Book 2
		Book b2 = new Book("Java");
		b2.addObserver(birgitte);
		b2.addObserver(wrh);
		birgitte.purchaseBook(b2, 8);
		System.out.println();

		// Book 3
		Book b3 = new Book("Design Patterns");
		b3.addObserver(birgitte);
		b3.addObserver(wrh);
		birgitte.purchaseBook(b3, 10);

		System.out.println();

		Customer c1 = new Customer("Camilla");
		Customer c2 = new Customer("Julius Cesar");
		Customer c3 = new Customer("Jhon Dyslexia");

		//---------------------------------------------------------------------

		TestApp.makeSale(b1, c1);
		System.out.println();
		TestApp.makeSale(b1, c2);
		System.out.println();
		TestApp.makeSale(b1, c3);
		System.out.println();

		TestApp.makeSale(b2, c1);
		System.out.println();
		TestApp.makeSale(b2, c2);
		System.out.println();
		TestApp.makeSale(b2, c3);
		System.out.println();

		TestApp.makeSale(b3, c1);
		System.out.println();

		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c3);
	}

	public static void makeSale(Book b, Customer c) {
		System.out.println("Sale: " + b + " sold to " + c.getName());
		c.addBook(b);
		b.decCount(1);
	}

}
