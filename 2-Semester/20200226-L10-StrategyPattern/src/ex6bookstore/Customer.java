package ex6bookstore;

import java.util.LinkedList;
import java.util.Queue;

public class Customer {
	private String name;
	private Queue<Book> books;

	public Customer(String name) {
		this.name = name;
		books = new LinkedList<Book>();
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("--------- "+name+" ---------\n");
		for (Book book : books) {
			sb.append("- " + book.getTitle() +"\n");
		}
		sb.append("----------------------------\n");
		return sb.toString();
	}

	public void addBook(Book book){
		if (!books.contains(book)){
			books.add(book);
			book.addCustomer(this);
		}
	}

	public Queue<Book> getPurchasedBooks(){
		return books;
	}
}
