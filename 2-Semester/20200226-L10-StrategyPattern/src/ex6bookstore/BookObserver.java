package ex6bookstore;

public interface BookObserver {
    void update(Book book);
}
