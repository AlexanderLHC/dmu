package ex6bookstore;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class Book {
	private String title;
	private int count;
	private int purchaseStock;
	private Queue<Customer> customers;
	private Set<BookObserver> bookObservers;

	public Book(String title) {
		this.title = title;
		this.count = 0;
		bookObservers = new HashSet<BookObserver>();
		customers = new LinkedList<Customer>();
		purchaseStock = 10;
	}

	public int getPurchaseStock() {
		return purchaseStock;
	}

	public void setPurchaseStock(int purchaseStock) {
		this.purchaseStock = purchaseStock;
	}

	public String getTitle() {
		return title;
	}

	public int getCount() {
		return count;
	}

	public void incCount(int amount) {
		count += amount;
	}

	public void decCount(int amount) {
		count -= amount;
		updateObservers();
	}

	@Override
	public String toString() {
		return title;
	}

	public void addCustomer(Customer customer) {
		if (!customers.contains(customer)) {
			customers.add(customer);
			customer.addBook(this);
		}
	}

	public Queue<Customer> getCustomers() {
		return customers;
	}

	public void addObserver(BookObserver bookObserver) {
		bookObservers.add(bookObserver);
	}

	public void removeObserver(BookObserver bookObserver) {
		bookObservers.remove(bookObserver);
	}

	private void updateObservers() {
		bookObservers.forEach(bookObservers -> bookObservers.update(this));
	}
}
