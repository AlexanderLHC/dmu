package ex3student;

public class NumberEvaluator implements Evaluator {
	/**
	 * Returns true, if s contains valid integer or decimal number.
	 */
	@Override
	public boolean isValid(String s) {
		try{
		    Double.parseDouble(s);
			return true;
		} catch (NumberFormatException e){
			return false;
		}
//		return s.matches("((-)?(\\+)?)\\d*(\\.)?(\\d+)?$");
	}
}
