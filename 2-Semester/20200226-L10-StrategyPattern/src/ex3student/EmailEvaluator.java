package ex3student;

public class EmailEvaluator implements Evaluator {
	/**
	 * Returns true, if s contains a valid email address.
	 */
	@Override
	public boolean isValid(String s) {
		boolean valid = false;

		if (s.contains("@")) {
			String[] subwords = s.split("@");
			if (subwords.length == 2) {
				boolean localValid = (isValidWords(subwords[0])) ? true : false;
				boolean domainValid = (isValidWords(subwords[1])) ? true : false;
				valid = (localValid && domainValid) ? true : false;
			}
		}

		return valid;
	}

	/**
	 * Returns true, if s is an acceptable word. That is, s is not empty, and all
	 * letters are in 'A'..'Z', 'a'..'z' or '0'..'9'.
	 */
	public boolean isWord(String s) {
		return s.matches("[a-zA-Z0-9]+");
	}

	public boolean isValidWords(String words) {
		boolean invalidFound = (words.startsWith(".") || words.endsWith(".")) ? true : false;
		String[] wordList = words.split("\\.");
		int i = 0;
		while (i < wordList.length && !invalidFound) {
			if (!isWord(wordList[i]))
				invalidFound = true;
			i++;
		}
		return !invalidFound;
	}
}
