package ex2lambda;

public class CustomerTest {
    public static void main(String[] args) {

        Customer customer1 = new Customer("Hans Ole Henriksen", 50);
        Customer customer2 = new Customer("Michael", 99);

        System.out.printf("Customer 1 = %s%n", customer1);
        System.out.printf("Customer 2 = %s%n", customer2);

        Customer.setComparator((Customer c1, Customer c2) -> c1.getName().compareTo(c2.getName()) );
        System.out.printf("Compared by name (c1, c2)= %s%n", customer1.compareTo(customer2));

        Customer.setComparator((Customer c1, Customer c2) -> Integer.compare(c1.getAge(), c2.getAge()));
        System.out.printf("Compared by age (c1, c2)= %s%n", customer1.compareTo(customer2));

        Customer.setComparator((Customer c1, Customer c2) -> c1.getName().compareTo(c2.getName()) );
        System.out.printf("Compared by name (c1, c2)= %s%n", customer1.compareTo(customer2));

    }
}
