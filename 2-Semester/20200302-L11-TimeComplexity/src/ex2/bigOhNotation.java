package ex2;

public class bigOhNotation {

	/*
	 * Opgave 2
	What is the time complexity (in big-Oh notation) of the method shown here?
	*/

	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			System.out.println(method(i));
		}

	}

	public static int method(int n) {
		int result = 0;
		for (int i = 0; i < n; i++) { // n
			for (int j = 1; j < n; j = j * 2) { // n / 2
				result++;
			}
		}
		return result;
	} // n + n / 2

}
