package ex3;

import java.util.Arrays;

public class prefixAverage {
    public static void main(String[] args) {
        int[] a1 = {5, 10, 5, 6, 4, 9, 8};
        System.out.printf("%s%n%s%n%s%n%s%n", "For list", Arrays.toString(a1),
                "prefixAvg", Arrays.toString(prefixAverage(a1)));
    }

    public static double[] prefixAverage(int[] input) {
        int i = 0;
        int inputLength = input.length;
        double[] average = new double[inputLength];

        while (i < inputLength) {
            int prevI = (i-1 >= 0)? i-1 : 0;
            average[i] = (input[i] + (average[prevI] * i))/(i+1);
            i++;
        }
        return average;
    }
}
