package ex5FlagSort;

import java.util.Arrays;

public class sortGWR {
	public static void main(String[] args) {
		char[] letters = { 'W', 'R', 'G', 'W', 'W', 'R', 'G', 'R', 'W', 'W', 'R', 'G' };
		System.out.printf("list %s %n sort: %s%n", Arrays.toString(letters), Arrays.toString(sortLetters(letters)));
	}

	public static char[] sortLetters(char[] letters) {
		char[] lettersSorted = letters;
		int lower = 0;
		int upper = letters.length - 1;
		int i = 0;

		while (i < upper) {
			if (lettersSorted[i] == 'G') {
				lettersSorted[i] = lettersSorted[lower];
				lettersSorted[lower] = 'G';
				lower++;
				i++;
			} else if (lettersSorted[i] == 'R') {
				lettersSorted[i] = lettersSorted[upper];
				lettersSorted[upper] = 'R';
				upper--;
			} else {
				i++;
			}
		}
		return lettersSorted;
	}

}
