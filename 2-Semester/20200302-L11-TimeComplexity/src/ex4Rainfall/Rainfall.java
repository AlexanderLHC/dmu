package ex4Rainfall;

import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeMap;

public class Rainfall {
    private int[] weekly = {
            20, 10, 12, 12, 13, 14, 15, 10, 8, 7, 13, 15, 10,
            9, 6, 8, 12, 22, 14, 16, 16, 18, 23, 12, 0, 2,
            0, 0, 18, 0, 0, 0, 34, 12, 34, 23, 23, 12, 44,
            23, 12, 34, 22, 22, 22, 22, 18, 19, 21, 32, 24, 13};

    /**
     * Returns the week number of the week that starts
     * a period of 3 weeks with the least rainfall.
     */
    public int minRainfall3() {
        // n
        int weekMin = -1;         // week of least rainfall
        int weekMinRain = -1;     // accumulated rainfall for the 3 weeks starting from weekMin
        int preWeekRain = -1;         // 1 week before index
        int prepreWeekRain = -1;      // 2 weeks before index
        int i = 0;
        while (i < weekly.length) {
            int rainPrevious3Weeks = weekly[i] + preWeekRain + prepreWeekRain;
            if (rainPrevious3Weeks < weekMinRain || weekMinRain < 0) {
                weekMin = i - 1;
                weekMinRain = rainPrevious3Weeks;
            }
            prepreWeekRain = preWeekRain;
            preWeekRain = weekly[i];
            i++;
        }
        return weekMin;
    }

    /**
     * Returns the week number of the week that starts
     * a period of n weeks with the least rainfall.
     * Requires: 0 < n < 52.
     */
    public int minRainfallN(int n) {
        int currentWeekLookup = 0;
        int occurrences = 1; // starts at 1 since first week = 1
        int i = 0;

        TreeMap<Integer, Integer> weeksWithNthoccurence = new TreeMap<>(); // <rainfall, week>

        while (i < weekly.length) {
            if (weekly[currentWeekLookup] == weekly[i])
                occurrences++;
            else {
                if (occurrences >= n) { weeksWithNthoccurence.put(weekly[currentWeekLookup], currentWeekLookup+1); }
                currentWeekLookup = i;
                occurrences = 1;
            }
            i++;
        }

        return weeksWithNthoccurence.firstEntry().getValue();
    }

    /**
     * Returns the week number of the week that starts
     * the longest period, where the rainfall has been exactly the same.
     */
    public int sameRainfall() {
        // TODO
        return 0;
    }
}
