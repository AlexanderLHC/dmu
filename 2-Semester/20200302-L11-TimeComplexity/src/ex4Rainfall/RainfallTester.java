package ex4Rainfall;

public class RainfallTester {
    public static void main(String[] args) {
        Rainfall rainfall = new Rainfall();

        System.out.printf("Uge med mindst regn: %s%n", rainfall.minRainfall3());
        System.out.printf("For 2 uger med mindstregn: %s%n", rainfall.minRainfallN(2));
        System.out.printf("For 4 uger med mindstregn: %s%n", rainfall.minRainfallN(4));
    }
}
