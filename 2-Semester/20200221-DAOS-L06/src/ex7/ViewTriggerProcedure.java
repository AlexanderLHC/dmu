package ex7;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.*;

public class ViewTriggerProcedure {

    public static void main(String[] args) {
        Connection c = getConnection();
        // View
        System.out.println("--------------\nView");
        try (PreparedStatement ps = c.prepareStatement("SELECT  * FROM kunderMedBy")) {
            ResultSet res = ps.executeQuery();
            for (int i = 1; i <= res.getMetaData().getColumnCount(); i++) {
                System.out.printf("%-35s ", res.getMetaData().getColumnName(i));
            }
            System.out.printf("%n");
            while (res.next()) {
                System.out.printf("%-35s %-35s %-35s %-35s %-35s%n", res.getString("cprNr"), res.getString("navn"),
                        res.getString("adresse"), res.getString("postNr"), res.getString("bynavn"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        // Stored Procedure
        System.out.println("\n--------------\nStored Procedure");
        try (CallableStatement cs = c.prepareCall("{call indeHavendeFraCPR(?, ?)}");) {
            String cpr = "1209631223";
            cs.setString(1, cpr);
            cs.registerOutParameter(2, Types.DECIMAL);
            cs.execute();
            double balance = cs.getInt(2);
            System.out.println("CPR: " + cpr + ". Har: " + balance + "kr.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        // Trigger
        System.out.println("\n--------------\nTrigger");
        try (PreparedStatement ps = c.prepareStatement("INSERT INTO KundeHarKonto (cprNr, regNr, ktonr) VALUES (3112692132, 1234, 123434343);")) {
            ps.executeQuery();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    // --------------------- Database authentication
    private static String getUser() throws Exception {
        File file = new File("auth.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        return document.getElementsByTagName("user").item(0).getTextContent();
    }

    private static String getPass() throws Exception {
        File file = new File("auth.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        return document.getElementsByTagName("password").item(0).getTextContent();
    }

    private static String getAddr() throws Exception {
        File file = new File("auth.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        return document.getElementsByTagName("address").item(0).getTextContent();
    }

    private static Connection getConnection() {
        Connection c = null;
        try {
            c = DriverManager.getConnection("jdbc:sqlserver://" + getAddr() + ";databaseName=AndeByBank;user="
                    + getUser() + ";password=" + getPass() + ";");
        } catch (Exception e) {
            System.out.println("fejl:  " + e.getMessage());
        }
        return c;
    }

}
