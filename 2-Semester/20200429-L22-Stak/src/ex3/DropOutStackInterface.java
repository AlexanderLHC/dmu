package ex3;

public interface DropOutStackInterface {
    int stackCapacity();
    int size();

    void push(Object element);

    Object pop();

    Object peek();

    void clear();

    int top();

}
