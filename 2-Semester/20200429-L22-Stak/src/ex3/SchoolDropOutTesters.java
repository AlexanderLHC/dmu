package ex3;

import java.util.ArrayList;
import java.util.Arrays;

public class SchoolDropOutTesters {
    public static void main(String[] args) {
        DropOutStackArray dropOutStackArray = new DropOutStackArray(2);
        DropOutStackLinkedList dropOutStackLinkedList = new DropOutStackLinkedList(2);

        ArrayList<DropOutStackInterface> dropouts = new ArrayList<>(Arrays.asList(dropOutStackArray, dropOutStackLinkedList));

        for (DropOutStackInterface dropout : dropouts) {
            System.out.println("\n======= List");
            dropout.push("Alice Cooper");
            dropout.push("Stevie Nicks");
            System.out.println("Size = " + dropout.size());
            System.out.println("Peek = " + dropout.peek());
            System.out.println("Pop = " + dropout.pop());
            System.out.println("Size = " + dropout.size());
            System.out.println("Pop = " + dropout.pop());
            System.out.println("Size = " + dropout.size());
            System.out.println("Pushing = Angus Young");
            dropout.push("Angus Young");
            System.out.println("Size = " + dropout.size());
            System.out.println("Peek = " + dropout.peek());
        }

    }
}
