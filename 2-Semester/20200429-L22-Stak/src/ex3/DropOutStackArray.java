package ex3;

public class DropOutStackArray implements DropOutStackInterface {
    private Object[] stack;
    private int size;
    private int top;

    public DropOutStackArray(int stackCapacity){
        stack = new Object[stackCapacity];
        top = 0;
    }


    @Override
    public int stackCapacity() {
        return stack.length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void push(Object element) {
       stack[top] = element;
       newElementSetCounters();
    }

    @Override
    public Object pop() {
        // TODO: check for null
        Object result = stack[top-1];
        removeElementSetCounters();
        return result;
    }

    @Override
    public Object peek() {
        return stack[top-1];
    }

    @Override
    public void clear() {
        stack = new Object[stackCapacity()];
        top = 0; size = 0;
    }

    @Override
    public int top() {
        return top;
    }

    // ------ Helper

    // Could be part of Interface, but not necessarily used?
    // Resets top if capacity reached, also counts size up if capacity isn't reached
    private void newElementSetCounters(){
        if (top == stackCapacity())
            top = 0;
        else {
            size++; top++;
        }
    }

    private void removeElementSetCounters(){
        if (top == 0)
            throw new IndexOutOfBoundsException("Can't remove without elements");
        else {
            size--; top--;
        }
    }
}
