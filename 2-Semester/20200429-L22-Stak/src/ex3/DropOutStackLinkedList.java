package ex3;

import sun.awt.image.ImageWatched;

import java.util.LinkedList;

public class DropOutStackLinkedList implements DropOutStackInterface {
    private LinkedList<String> stack;
    private int size;
    private int top;
    private int stackCapacity;

    public DropOutStackLinkedList(int stackCapacity){
        stack = new LinkedList<String>();
        top = 0;
        this.stackCapacity = stackCapacity;
    }


    @Override
    public int stackCapacity() {
        return stackCapacity;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void push(Object element) {
        stack.push((String) element);
       newElementSetCounters();
    }

    @Override
    public Object pop() {
        // TODO: check for null
        String result = stack.pop();
        removeElementSetCounters();
        return result;
    }

    @Override
    public Object peek() {
        return stack.getLast();
    }

    @Override
    public void clear() {
        stack = new LinkedList<String>();
        top = 0; size = 0;
    }

    @Override
    public int top() {
        return top;
    }

    // ------ Helper

    // Could be part of Interface, but not necessarily used?
    // Resets top if capacity reached, also counts size up if capacity isn't reached
    private void newElementSetCounters(){
        if (top == stackCapacity())
            top = 0;
        else {
            size++; top++;
        }
    }

    private void removeElementSetCounters(){
        if (top == 0)
            throw new IndexOutOfBoundsException("Can't remove without elements");
        else {
            size--; top--;
        }
    }
}
