package ex1;

import java.util.Deque;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class ArrayStackTester {
    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack(10);
        Stack stack = new Stack<Integer>();

        for (int i = 0; i < 15; i++) {
            arrayStack.push(i);
            stack.push(i);
        }
        while (!arrayStack.isEmpty()){
            System.out.println("ArrayStack = " + arrayStack.pop() + " | Stack = " + stack.pop());
        }
    }
}
