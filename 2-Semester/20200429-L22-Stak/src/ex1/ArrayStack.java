package ex1;

import java.util.Arrays;
import java.util.EmptyStackException;

public class ArrayStack implements StackInterface {
    private Object[] stack;
    private int size; // elements amount in stack

    public ArrayStack(int initialCapacity){
        stack = new Object[initialCapacity];
        size = 0;
    }

    @Override
    public void push(Object element) {
        if (stack.length == size) {
            stack = Arrays.copyOf(stack,stack.length*2);
        }
        stack[size] = element;
        size++;
    }

    @Override
    public Object pop() {
        if (isEmpty()) { throw new EmptyStackException(); };
        Object removed = stack[size-1];
        stack[size-1] = null;
        size--;
        return removed;
    }

    @Override
    public Object peek() {
       if (isEmpty())
           throw new EmptyStackException();
       else
           return stack[size-1];
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }
}
