package ex2;

import ex1.ArrayStack;

import javax.swing.*;

public class ParentesesHelpMethod {

    public static void main(String[] args) {
        System.out.println(checkParenteses("(}){")); // false
        System.out.println(checkParenteses("()")); // true
        System.out.println(checkParenteses("({)}")); // false
        System.out.println(checkParenteses("()()(){}")); // true

        System.out.println(checkParenteses("(3+{5{99{*}}[23[{67}67]]})")); // true
    }

    public static boolean checkParenteses(String s){
       ArrayStack arrayStack = new ArrayStack(s.length());

       for (int i = 0; i < s.length(); i++){
           char lookup = s.charAt(i);
           if ("({[".indexOf(lookup) >= 0){ // if char is in String
              arrayStack.push(s.charAt(i));
           }  else if (!arrayStack.isEmpty()) {
               if (!isMatch((char) arrayStack.pop(), lookup)) {
                  return false;
               }
           }
       }
       return arrayStack.isEmpty();
    }

    private static boolean isMatch(char start, char end){
        if (start == '(' && end == ')'){
            return true;
        } else if (start == '{' && end == '}') {
            return true;
        } else if (start == '[' && end == ']') {
            return true;
        } else {
            return false;
        }
    }
}
